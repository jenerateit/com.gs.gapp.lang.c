package com.gs.gapp.converter.function.c;

import org.jenerateit.modelconverter.ModelConverterI;
import org.osgi.service.component.annotations.Component;

import com.gs.gapp.converter.basic.c.AbstractCConverterProvider;

/**
 *
 */
@Component
public class FunctionToCConverterProvider extends AbstractCConverterProvider {

	/**
	 *
	 */
	public FunctionToCConverterProvider() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.ModelConverterProviderI#getModelConverter()
	 */
	@Override
	public ModelConverterI getModelConverter() {
		return new FunctionToCConverter();
	}
}
