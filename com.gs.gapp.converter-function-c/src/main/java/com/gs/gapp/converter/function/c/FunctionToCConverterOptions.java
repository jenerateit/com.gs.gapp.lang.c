package com.gs.gapp.converter.function.c;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.basic.c.BasicToCConverterOptions;



/**
 * @author mmt
 *
 */
public class FunctionToCConverterOptions extends BasicToCConverterOptions {
	
	public FunctionToCConverterOptions(ModelConverterOptions options) {
		super(options);
	}

}
