package com.gs.gapp.converter.function.c;

import java.util.List;

import com.gs.gapp.converter.basic.c.BasicToCConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;



/**
 * @author mmt
 *
 */
public class FunctionToCConverter extends BasicToCConverter {

	private FunctionToCConverterOptions converterOptions;
	
	public FunctionToCConverter() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.persistence.jpa.PersistenceToJPAConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		this.converterOptions = new FunctionToCConverterOptions(getOptions());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters

		// --- slave element converters
	
		
		return result;
	}

	@Override
	public FunctionToCConverterOptions getConverterOptions() {
		return converterOptions;
	}
}
