package com.gs.gapp.lang.c;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;

import org.osgi.framework.Bundle;

import com.vd.jenerateit.server.test.util.AbstractGeneratorTest;
import com.vd.jenerateit.server.test.util.ZipTool;

public class LangCTest extends AbstractGeneratorTest {

	public LangCTest() {
		(new File("./target")).mkdirs(); 
	}

	@Override
	protected String getGensetBundleId() {
		return "com.gs.gapp.vd-c-groovy"; 
	}

	protected String getSymbolicNameOfTestModelBundle() {
		return "com.gs.gapp.ref-model-c";
	}

	@Override
	protected InputStream getZippedInputModel() {
		ZipTool zip = null;
		Bundle testModelBundle = AbstractGeneratorTest.getBundle(getSymbolicNameOfTestModelBundle());
		Enumeration<URL> modelFileEntries = testModelBundle.findEntries("/", "*.groovy", true);

		if (modelFileEntries != null) {
			ArrayList<URL> urls = new ArrayList<>();
			while (modelFileEntries.hasMoreElements()) {
				URL url = modelFileEntries.nextElement();
				urls.add(url);
			}
			zip = new ZipTool(urls.toArray(new URL[0]));
		}

		return zip == null ? null : zip.getInputStream();
	}

	@Override
	protected InputStream getZippedReferenceFiles() {
		return null;
	}

	@Override
	protected String specifyOutputDirectory() {
		return "/tmp/ctests/";
	}
	
	
}
