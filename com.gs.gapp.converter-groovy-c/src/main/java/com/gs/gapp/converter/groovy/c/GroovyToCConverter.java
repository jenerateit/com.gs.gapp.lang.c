package com.gs.gapp.converter.groovy.c;


import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.c.validation.ValidatorGlobalFunctionPrototypes;
import com.gs.gapp.metamodel.c.validation.ValidatorGlobalFunctions;
import com.gs.gapp.metamodel.c.validation.ValidatorGlobalVariables;
import com.gs.gapp.metamodel.c.validation.ValidatorLocalFunctionPrototypes;
import com.gs.gapp.metamodel.c.validation.ValidatorLocalFunctions;
import com.gs.gapp.metamodel.c.validation.ValidatorModuleVariables;
import com.gs.vd.converter.groovy.any.GroovyToAnyConverter;

public class GroovyToCConverter extends GroovyToAnyConverter {
	
	
	/**
	 * @param modelElementCache
	 */
	public GroovyToCConverter() {
		super(new ModelElementCache());
	}
	
	
//	/**
//	 * @param modelElements
//	 */
//	@Override
//	protected void onPerformValidationBeforeConversion(Set<Object> modelElements) {
//		super.onPerformValidationBeforeConversion(modelElements);
//	addMessages( new ValidatorLocalVariables().validate(result) );
//	}
//	
	
	
	@Override
	protected void onPerformModelValidation(Set<Object> result) {
		super.onPerformModelValidation(result);
		addMessages( new ValidatorModuleVariables().validate(result) );
		addMessages( new ValidatorLocalFunctions().validate(result) );
		addMessages( new ValidatorGlobalVariables().validate(result) );
		addMessages( new ValidatorGlobalFunctions().validate(result) );
		addMessages( new ValidatorLocalFunctionPrototypes().validate(result) );
		addMessages( new ValidatorGlobalFunctionPrototypes().validate(result) );



	}

}
