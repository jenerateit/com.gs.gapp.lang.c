package com.gs.gapp.converter.groovy.c;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;
import org.osgi.service.component.annotations.Component;


/**
 * @author mmt
 *
 */
@Component
public class GroovyToCConverterProvider implements ModelConverterProviderI {
	
	
	/**
	 * 
	 */
	public GroovyToCConverterProvider() {
		super();
	}
	
	@Override
	public ModelConverterI getModelConverter() {
    	
		return new GroovyToCConverter();
	}
}
