/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.AbstractCDataTypes;
import com.gs.gapp.metamodel.c.AbstractCType;
import com.gs.gapp.metamodel.c.CPointerType;
import com.gs.gapp.metamodel.c.CTypedef;

/**
 * @author dsn
 *
 */
public class CPointerTypeWriter extends CDerivedTypeWriter {
	@ModelElement
	private CPointerType cPointerType;

	private int starCounter = 0;

	@Override
	public void transform(TargetSection ts) throws WriterException {
		wType(ts);

	}

	private void wType(TargetSection ts) {
		if(cPointerType.getRealType() instanceof CPointerType) {
			AbstractCType<ModelElementI> tempPointer = handlePointerToPointer(cPointerType.getRealType());
			hanldeTheCore(tempPointer,ts);
			makePointer();
		} else if(cPointerType.getRealType() instanceof AbstractCDataTypes) {
			if( cPointerType.getRealType() instanceof CTypedef) {
				w(((CTypedef) cPointerType.getRealType()).getNewIdentifierName());
			} else {
				w(cPointerType.getRealType().getName());
				w(" ");
				w(((AbstractCDataTypes) cPointerType.getRealType()).getIdentifierName());
			}
			makePointer();
		} else {
			AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(cPointerType.getRealType());
			typeWriter.transform(ts);
			makePointer();
//			w(" ");
		}
	}

	public void makePointer() {
		if(starCounter >0) {
			w(" ");
			for (int i = 0; i < starCounter; i++) {
				w("*");
			}
		} else {
			w(" ");
			w("*");		
		}
	}



	private void hanldeTheCore(AbstractCType<ModelElementI> tempPointer,TargetSection ts) {
		if( tempPointer instanceof CTypedef) {
			w(((CTypedef) tempPointer).getNewIdentifierName());
		} else {
			if(tempPointer instanceof AbstractCDataTypes) {
				w(tempPointer.getName());
				w(" ");
				w(((AbstractCDataTypes) tempPointer).getIdentifierName());
			} else {
				AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(tempPointer);
				typeWriter.transform(ts);
			}
		}
	}


	private AbstractCType<ModelElementI> handlePointerToPointer(AbstractCType<ModelElementI> input) {
		starCounter++;
		if(input instanceof CPointerType) {
			return (handlePointerToPointer(((CPointerType) input).getRealType()));
		} else {
			return input;
		}
	}



}
