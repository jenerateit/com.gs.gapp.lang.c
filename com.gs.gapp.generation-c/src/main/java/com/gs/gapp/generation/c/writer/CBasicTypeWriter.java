/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CBasicType;

/**
 * @author dsn
 *
 */
public class CBasicTypeWriter extends AbstractCTypeWriter {
	
	@ModelElement
	private CBasicType basicType;

	@Override
	public void transform(TargetSection ts) throws WriterException {
		wType();		
	}
	
	
	protected void wType() {
		w(basicType.getName());
	}
	


}
