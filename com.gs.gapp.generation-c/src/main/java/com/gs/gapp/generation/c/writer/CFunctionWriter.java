/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import java.util.Iterator;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.c.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.AbstractCDataTypes;
import com.gs.gapp.metamodel.c.AbstractCType;
import com.gs.gapp.metamodel.c.CDerivedType;
import com.gs.gapp.metamodel.c.CFunction;
import com.gs.gapp.metamodel.c.CFunctionInputParameter;
import com.gs.gapp.metamodel.c.CModuleVariable;
import com.gs.gapp.metamodel.c.CPointerType;
import com.gs.gapp.metamodel.c.CTypedef;
import com.gs.gapp.metamodel.c.CVariable;
import com.gs.gapp.metamodel.c.CVoidType;

/**
 * @author dsn
 *
 */
public class CFunctionWriter extends CWriter {

	@ModelElement
	private CFunction cFunction;

	private int starCounter = 0;

	/**
	 * 
	 */

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wJavaDoc();

		wStorageSpecifier();
		wTypeQualifier();
		wReturnType(ts) ;
		w(" ");
		wName();
		wWriteInputParameters(ts);
		wNL();
		wNL("{");

		indent();

		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.FUNCTION_BODY)) {
			bDA(new StringBuffer(cFunction.getName()).append(".").append("functionBody").toString());
			wUsedVariables(ts);
			wLocalVariables(ts);
			wFunctionBody(ts);
			wReturn();
			eDA();

		} else {
			wLocalVariables(ts);
			wFunctionBody(ts);
			wReturn();
		}
		outdent();

		wNL("}");


	}


	private void wUsedVariables(TargetSection ts) {
		if(!cFunction.getUsedVariables().isEmpty()) {
			Iterator<CVariable> i = cFunction.getUsedVariables().iterator();  
			while(i.hasNext())  
			{  
				CVariable tempVar = i.next();
				w(tempVar.getName());
				if(tempVar.getValue()!=null || !tempVar.getValue().equals("")) {
					wNL(" = ", tempVar.getValue(),";");
				}
				else {
					wNL(";");
				}

			} 
		}

	}


	protected void wFunctionBody(TargetSection ts) {
		if(cFunction.getFunctionBody() != null) {
			if(cFunction.getFunctionBody().size() > 0) {
				for(String line : cFunction.getFunctionBody()) {
					wNL(line);
				}
			} else {
				wDefaultFunctionBody(ts);
			}
		} else {
			wDefaultFunctionBody(ts);

		}
	}

	private void wDefaultFunctionBody(TargetSection ts) {
		if (cFunction.getDefaultFunctionBody() != null && cFunction.getDefaultFunctionBody().size() > 0) {
			for (String defaultBodyLine : cFunction.getDefaultFunctionBody()) {
				wNL(defaultBodyLine);
			}
		}
	}

	private void wReturn() {
		if( !(cFunction.getReturnType() instanceof CVoidType)) {
			if(cFunction.getReturnVariable() != null) {
				if(cFunction.getReturnVariable().getType() == cFunction.getReturnType()) {
					w("return ");
					if(cFunction.getReturnVariable().getType() instanceof CPointerType) {
						if(((CDerivedType) cFunction.getReturnType()).getRealType() instanceof CPointerType) {
							handlePointerToPointer(((CDerivedType) cFunction.getReturnType()).getRealType());
							makePointer();
						}
					}
					w(cFunction.getReturnVariable().getName(), ";");
					wNL();
				} else {
					//if we need to cast return variable, it will be done here
				}
			}
		}
	}

	private void wReturnType(TargetSection ts) {
		if( cFunction.getReturnType() instanceof CTypedef) {
			wTypeDefedType();
		} else {
			if(cFunction.getReturnType() instanceof AbstractCDataTypes) {
				w(cFunction.getReturnType().getName());
				w(" ");
				w(((AbstractCDataTypes) cFunction.getReturnType()).getIdentifierName());
			} else if ((cFunction.getReturnType() instanceof CPointerType) ) {
				if((((CDerivedType) cFunction.getReturnType()).getRealType() instanceof AbstractCDataTypes )) {
					if( ((CDerivedType) cFunction.getReturnType()).getRealType() instanceof CTypedef ) {
						w(((CTypedef) ((CDerivedType) cFunction.getReturnType()).getRealType()).getNewIdentifierName());
						makePointer();
					} else {
						w(((CDerivedType) cFunction.getReturnType()).getRealType().getName());
						w(" ");
						w(((AbstractCDataTypes) ((CDerivedType) cFunction.getReturnType()).getRealType()).getIdentifierName());
						makePointer();
					}
				} else if( ((CDerivedType) cFunction.getReturnType()).getRealType() instanceof CTypedef ) {
					w(((CTypedef) ((CDerivedType) cFunction.getReturnType()).getRealType()).getNewIdentifierName());
					makePointer();
				} else if( ((CDerivedType) cFunction.getReturnType()).getRealType() instanceof CPointerType ) { 
					AbstractCType<ModelElementI> tempPointer = handlePointerToPointer(((CDerivedType) cFunction.getReturnType()).getRealType());
					hanldeTheCore(tempPointer,ts);
					makePointer();
				}
				else {
					wType(ts);
				}
			} else {
				wType(ts);
			}
		}
	}


	private void hanldeTheCore(AbstractCType<ModelElementI> tempPointer,TargetSection ts) {
		if( tempPointer instanceof CTypedef) {
			w(((CTypedef) tempPointer).getNewIdentifierName());
		} else {
			if(tempPointer instanceof AbstractCDataTypes) {
				w(tempPointer.getName());
				w(" ");
				w(((AbstractCDataTypes) tempPointer).getIdentifierName());
			} else {
				AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(tempPointer);
				typeWriter.transform(ts);
			}
		}
	}


	private AbstractCType<ModelElementI> handlePointerToPointer(AbstractCType<ModelElementI> input) {
		starCounter++;
		if(input instanceof CPointerType) {
			return (handlePointerToPointer(((CPointerType) input).getRealType()));
		} else {
			return input;
		}
	}

	private void makePointer() {
		if(starCounter >0) {
			w(" ");
			for (int i = 0; i<starCounter; i++) {
				w("*");
			}
			starCounter = 0;
		} else {
			if(cFunction.getReturnType() instanceof CPointerType) {
				w(" *");
			}		
		}
	}

	private void wTypeDefedType() {
		w(((CTypedef) cFunction.getReturnType()).getNewIdentifierName());		

	}

	private void wLocalVariables(TargetSection ts) {
		if(!cFunction.getLocalVariables().isEmpty()) {
			Iterator<CModuleVariable> i = cFunction.getLocalVariables().iterator();  
			while(i.hasNext())  
			{  
				AbstractCVariableWriter typeWriter = (AbstractCVariableWriter) getTransformationTarget().getWriterInstance(i.next());
				typeWriter.transform(ts);
			} 
		}
	}


	private void wWriteInputParameters(TargetSection ts) {
		w("( "); 
		if(cFunction.getInputParameters().isEmpty()) {
			w("void"); 
		} else {
			Iterator<CFunctionInputParameter> i = cFunction.getInputParameters().iterator();  
			while(i.hasNext())  
			{  
				AbstractCVariableWriter typeWriter = (AbstractCVariableWriter) getTransformationTarget().getWriterInstance(i.next());
				typeWriter.transform(ts);
				if(i.hasNext()) {	
					w(", ");				
				}
			}  
		}


		w(" )");
	}


	private void wTypeQualifier() {
		if(!cFunction.getTypeQualifier().getName().equals("")) {
			w(" ");
			w(cFunction.getTypeQualifier().getName());
		}
	}


	private void wStorageSpecifier() {
		if(!cFunction.getStorageSpecifier().getName().equals("")) {
			w(cFunction.getStorageSpecifier().getName());
			w(" ");
		}

	}


	protected void wType(TargetSection ts) {
		AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(cFunction.getReturnType());
		typeWriter.transform(ts);

	}

	protected void wName() {
		w(cFunction.getName());
	}
}
