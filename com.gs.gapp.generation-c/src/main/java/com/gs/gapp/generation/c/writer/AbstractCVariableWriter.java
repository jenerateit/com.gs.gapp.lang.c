/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

/**
 * @author dsn
 *
 */
public abstract class AbstractCVariableWriter extends CWriter {
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wJavaDoc();
	}
}
