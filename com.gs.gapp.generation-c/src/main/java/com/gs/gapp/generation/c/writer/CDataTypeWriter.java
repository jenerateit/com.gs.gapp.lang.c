/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.AbstractCDataTypes;
import com.gs.gapp.metamodel.c.CDataMember;
import com.gs.gapp.metamodel.c.CDataTypes;
import com.gs.gapp.metamodel.c.CModelElement;

/**
 * @author dsn
 *
 */
public abstract class CDataTypeWriter extends AbstractCDataTypeWriter {

	@ModelElement
	CDataTypes cDataTypes;


	/*
	 * 
	 * struct and union curly braces etc must be written here
	 */

	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		w("{");
		wMemberDefinitions(ts);
		w("}");
		wDataTypeVariables();
	}





	@SuppressWarnings("unchecked")
	private void wMemberDefinitions(TargetSection ts) {
		if(!cDataTypes.getMemberDefinitions().isEmpty()) {
			indent();
			wNL();
			for(CDataMember memberDefinition : cDataTypes.getMemberDefinitions()) {
				CDataMemberWriter cDataMemberWriter =  (CDataMemberWriter) getTransformationTarget().getWriterInstance(memberDefinition);
				if(memberDefinition.getType() instanceof AbstractCDataTypes) {
					if(((CModelElement<ModelElementI>) memberDefinition.getType()).getOwner() instanceof CDataTypes) {
						cDataMemberWriter.handleNestedDataType(ts);
					} else {
						cDataMemberWriter.transform(ts);
					}
				}
				else {
					cDataMemberWriter.transform(ts);
				}
			}
			outdent();
		}
	}

}
