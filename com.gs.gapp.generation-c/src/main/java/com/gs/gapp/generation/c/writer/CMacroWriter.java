/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.c.target.CTargetDocument;
import com.gs.gapp.metamodel.c.CMacro;

/**
 * @author dsn
 * Special macros and directives are not covered please refer to wiki page https://en.wikipedia.org/wiki/C_preprocessor
 */
public abstract class CMacroWriter extends CWriter {
	
	@ModelElement
	private CMacro cMacro;

	public void transform(TargetSection ts) throws WriterException {
		if (ts == CTargetDocument.MACROS) {
			wJavaDoc();
			wDefine();
		}

	}

	public void wDefine() {
		w("#define ");
	}
	
	public void wReplacementTokens () {
		for(String replacementToken : cMacro.getReplacementTokenList()) {
			w(" ");
			w(replacementToken);
		}
	}

}
