/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import java.util.Iterator;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.AbstractCDataTypes;
import com.gs.gapp.metamodel.c.AbstractCType;
import com.gs.gapp.metamodel.c.CDerivedType;
import com.gs.gapp.metamodel.c.CFunctionInputParameter;
import com.gs.gapp.metamodel.c.CFunctionPrototype;
import com.gs.gapp.metamodel.c.CPointerType;
import com.gs.gapp.metamodel.c.CTypedef;

/**
 * @author dsn
 *
 */
public class CFunctionPrototypeWriter extends CWriter{

	@ModelElement
	CFunctionPrototype cFunctionPrototype;

	int starCounter = 0;

	/**
	 * 
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wJavaDoc();
		wStorageSpecifier();
		wTypeQualifier();
		wReturnType(ts);
		w(" ");
		wName();
		wWriteInputParameters(ts);
		wNL(";");
	}

	private void wWriteInputParameters(TargetSection ts) {
		w("( "); 
		if(cFunctionPrototype.getAssociatedFunction().getInputParameters().isEmpty()) {
			w("void"); 
		} else {
			Iterator<CFunctionInputParameter> i = cFunctionPrototype.getAssociatedFunction().getInputParameters().iterator();  
			while(i.hasNext())  
			{  
				AbstractCVariableWriter typeWriter = (AbstractCVariableWriter) getTransformationTarget().getWriterInstance(i.next());
				typeWriter.transform(ts);
				if(i.hasNext()) {	
					w(", ");
				}
			}  
		}


		w(" )");
	}


	private void wTypeQualifier() {
		if(!cFunctionPrototype.getAssociatedFunction().getTypeQualifier().getName().equals("")) {
			//			if(!cFunctionPrototype.getAssociatedFunction().getStorageSpecifier().getName().equals("")) {

			//			}
			w(cFunctionPrototype.getAssociatedFunction().getTypeQualifier().getName());
			w(" ");
		}
	}


	private void wStorageSpecifier() {
		if(!cFunctionPrototype.getAssociatedFunction().getStorageSpecifier().getName().equals("")) {
			w(cFunctionPrototype.getAssociatedFunction().getStorageSpecifier().getName());
			w(" ");
		}

	}


	private void wReturnType(TargetSection ts) {
		if( cFunctionPrototype.getAssociatedFunction().getReturnType() instanceof CTypedef) {
			wTypeDefedType();
		} else {
			if(cFunctionPrototype.getAssociatedFunction().getReturnType() instanceof AbstractCDataTypes) {
				w(cFunctionPrototype.getAssociatedFunction().getReturnType().getName());
				w(" ");
				w(((AbstractCDataTypes) cFunctionPrototype.getAssociatedFunction().getReturnType()).getIdentifierName());
			} else if ((cFunctionPrototype.getAssociatedFunction().getReturnType() instanceof CPointerType) ) {
				if((((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType() instanceof AbstractCDataTypes )) {
					if( ((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType() instanceof CTypedef ) {
						w(((CTypedef) ((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType()).getNewIdentifierName());
						makePointer();
					} else {
						w(((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType().getName());
						w(" ");
						w(((AbstractCDataTypes) ((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType()).getIdentifierName());
						makePointer();
					}
				} else if( ((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType() instanceof CTypedef ) {
					w(((CTypedef) ((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType()).getNewIdentifierName());
					makePointer();
				} else if( ((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType() instanceof CPointerType ) { 
					AbstractCType<ModelElementI> tempPointer = handlePointerToPointer(((CDerivedType) cFunctionPrototype.getAssociatedFunction().getReturnType()).getRealType());
					hanldeTheCore(tempPointer,ts);
					makePointer();
				}
				else {
					wType(ts);
				}
			} else {
				wType(ts);
			}
		}
	}

	protected void wType(TargetSection ts) {
		AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(cFunctionPrototype.getAssociatedFunction().getReturnType());
		typeWriter.transform(ts);
	}

	private void wTypeDefedType() {
		w(((CTypedef) cFunctionPrototype.getAssociatedFunction().getReturnType()).getNewIdentifierName());		

	}

	protected void wName() {
		w(cFunctionPrototype.getName());
	}

	private void hanldeTheCore(AbstractCType<ModelElementI> tempPointer,TargetSection ts) {
		if( tempPointer instanceof CTypedef) {
			w(((CTypedef) tempPointer).getNewIdentifierName());
		} else {
			if(tempPointer instanceof AbstractCDataTypes) {
				w(tempPointer.getName());
				w(" ");
				w(((AbstractCDataTypes) tempPointer).getIdentifierName());
			} else {
				AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(tempPointer);
				typeWriter.transform(ts);
			}
		}
	}


	private AbstractCType<ModelElementI> handlePointerToPointer(AbstractCType<ModelElementI> input) {
		starCounter++;
		if(input instanceof CPointerType) {
			return (handlePointerToPointer(((CPointerType) input).getRealType()));
		} else {
			return input;
		}
	}

	private void makePointer() {
		if(starCounter >0) {
			w(" ");
			for (int i = 0; i<starCounter; i++) {
				w("*");
			}
		} else {
			if(cFunctionPrototype.getAssociatedFunction().getReturnType() instanceof CPointerType) {
				w(" *");
			}		
		}
	}
}
