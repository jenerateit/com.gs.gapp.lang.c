/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CArrayType;
import com.gs.gapp.metamodel.c.CDerivedType;
import com.gs.gapp.metamodel.c.CPointerType;
import com.gs.gapp.metamodel.c.CTypedef;
import com.gs.gapp.metamodel.c.CVariable;

/**
 * @author dsn
 *
 */
public abstract class CVariableWriter extends AbstractCVariableWriter {

	@ModelElement
	private CVariable cVariable;

	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		wStorageSpecifier();
		wTypeQualifier();

		if( cVariable.getType() instanceof CTypedef) {
			wTypeDefedType();
		} else {
			wType(ts);
		}
		if(!(cVariable.getType() instanceof CPointerType)) {
			w(" ");
		}
		wName();
		if(cVariable.getType() instanceof CDerivedType) {
			if(cVariable.getType() instanceof CArrayType) {
				MakeVariableArray(ts);
			}
		}
		//assigning value to extern is meaningless. Compile would warn
		//		if(!cVariable.getStorageSpecifierAsString().equals("extern")) { 
		wAssignValue();
		//		} 
		//TODO : Discuss about the disabling initializing the extern variable
	}


	protected void wTypeDefedType() {
		w(((CTypedef) cVariable.getType()).getNewIdentifierName());		
	}

	private void MakeVariableArray(TargetSection ts) {
		CArrayTypeWriter arrayWriter = (CArrayTypeWriter) getTransformationTarget().getWriterInstance(cVariable.getType());
		arrayWriter.makeArray(ts);		
	}


	protected void wType(TargetSection ts) {
		AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(cVariable.getType());
		typeWriter.transform(ts);
	}

	protected void wName() {
		w(cVariable.getName());
	}

	protected void wAssignValue() {
		if(!cVariable.getValue().isEmpty()) {
			w(" = ", cVariable.getValue()) ;
		}
	}

	protected void wStorageSpecifier() {
		w(cVariable.getStorageSpecifierAsString());
		if(!cVariable.getStorageSpecifierAsString().equals("")) {
			w(" ");
		}
	}

	protected void wTypeQualifier() {
		w(cVariable.getTypeQualifierAsString());
		if(!cVariable.getTypeQualifierAsString().equals("")) {
			w(" ");
		}
	}
}
