/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package com.gs.gapp.generation.c.target;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.AbstractTextTargetDocument;
import org.jenerateit.target.DeveloperAreaInfoI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;

/**
 * @author dsn
 *
 */
public class CTargetDocument extends AbstractTextTargetDocument {


	private static final long serialVersionUID = -1222683702652410945L;

	@SuppressWarnings("unused")
	private static final int TABSIZE = 4;
	
	/** target section describing the Comments in C ... */
	public static final TargetSection COMMENTS = new TargetSection("comments",10);
	/** target section describing the include libraries in C ... */
	public static final TargetSection INCLUDES = new TargetSection("includes", 20);
	/** target section describing the macros ... */
	public static final TargetSection MACROS = new TargetSection("macros", 30);  
	/** target section describing the typedefs ... */
	public static final TargetSection TYPEDEFS = new TargetSection("typedefs", 40);
	/** target section describing the global variables ... */
	public static final TargetSection GLOBALVARIABLES = new TargetSection("globalvariables", 50);
	/** target section describing the module variables ... */
	public static final TargetSection MODULEVARIABLES = new TargetSection("modulevariables", 60);
	/** target section describing the global variables ... */
	public static final TargetSection GLOBALCOMPOSITEDATATYPEDECLARATIONS = new TargetSection("globalcompositedatatypedeclarations", 70);
	/** target section describing the global function prototypes ... */
	public static final TargetSection GLOBALFUNCTIONPROTOTYPES = new TargetSection("globalfunctionprototypes", 80);
	/** target section describing the function prototypes ... */
	public static final TargetSection LOCALFUNCTIONPROTOTYPES = new TargetSection("localfunctionprototypes", 90);
	/** target section describing the main function ... */
	public static final TargetSection LOCALFUNCTIONS = new TargetSection("localfunctions", 100);
	/** target section describing the functions ... */
	public static final TargetSection GLOBALFUNCTIONS = new TargetSection("globalfunctions", 110);
	/** target section describing the functions ... */
	public static final TargetSection ENDIFFORHEADER = new TargetSection("endifforheader", 120);

	


	
	private static final SortedSet<TargetSection> SECTIONS = 
		new TreeSet<>(Arrays.asList(new TargetSection[] {
				COMMENTS,
				INCLUDES,
				MACROS,
				TYPEDEFS,
				GLOBALVARIABLES,
				MODULEVARIABLES,
				GLOBALCOMPOSITEDATATYPEDECLARATIONS,
				GLOBALFUNCTIONPROTOTYPES,
				LOCALFUNCTIONPROTOTYPES,
				LOCALFUNCTIONS,
				GLOBALFUNCTIONS,
				ENDIFFORHEADER
		})); 
	
	protected static final Pattern INCLUDES_PATTERN = 
			Pattern.compile("^\\s*include\\s+(" + 
					DeveloperAreaInfoI.ID_PATTERN + "(\\.\\*)?)\\s*;\\s*$");
	
	protected static final Pattern MACRO_PATTERN = 
			Pattern.compile("^\\s*define\\s+(" + 
					DeveloperAreaInfoI.ID_PATTERN + "(\\.\\*)?)\\s*;\\s*$");
	/**
	 * Return an empty string.
	 * 
	 * @return the comment end sequence
	 * @see org.jenerateit.target.TextTargetDocumentI#getCommentEnd()
	 */
	@Override
	public final String getCommentEnd() {
		return StringTools.EMPTY_STRING;
	}

	/**
	 * Returns the '//' sequence.
	 * 
	 * @return the comment start sequence
	 * @see org.jenerateit.target.TextTargetDocumentI#getCommentStart()
	 */
	@Override
	public final String getCommentStart() {
		return "//";
	}

	/**
	 * Returns the variable {@link #SECTIONS} from this class.
	 * 
	 * @return a set of sections describing a C file
	 * @see org.jenerateit.target.TextTargetDocumentI#getTargetSections()
	 */
	@Override
	public final SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}


	/**
	 * Getter for the indent character.
	 * 
	 * @return the indent character
	 */
	@Override
	public char getPrefixChar() {
		return ' ';
	}
	
	
	public Set<String> getSectionIncludes() {
		Set<String> imports = new LinkedHashSet<>();
		
		LineNumberReader lnr = new LineNumberReader(new StringReader(getIncludesSection()));
		String line = null;
		Matcher m = null;
		try {
			while ((line = lnr.readLine()) != null) {
				if (StringTools.isText(line)) {
				    if (line.indexOf(" static ") < 0 && (m = CTargetDocument.INCLUDES_PATTERN.matcher(line)) != null && m.matches()) {
					    imports.add(m.group(1));
				    }
				}
			}
		} catch (IOException e) {
			throw new JenerateITException("Can not read the current includes");
		}
		return imports;
	}
	
	
	public Set<String> getSectionMacros() {
		Set<String> imports = new LinkedHashSet<>();
		
		LineNumberReader lnr = new LineNumberReader(new StringReader(getIncludesMacros()));
		String line = null;
		Matcher m = null;
		try {
			while ((line = lnr.readLine()) != null) {
				if (StringTools.isText(line)) {
				    if (line.indexOf(" static ") < 0 && (m = CTargetDocument.MACRO_PATTERN.matcher(line)) != null && m.matches()) {
					    imports.add(m.group(1));
				    }
				}
			}
		} catch (IOException e) {
			throw new JenerateITException("Can not read the current includes");
		}
		return imports;
	}
	
	/**
	 * 
	 * @return INCLUDES as a string
	 */
	public String getIncludesSection() {
		return new String(toByte(INCLUDES));
	}
	
	/**
	 * 
	 * @return INCLUDES as a string
	 */
	public String getIncludesMacros() {
		return new String(toByte(MACROS));
	}

}
