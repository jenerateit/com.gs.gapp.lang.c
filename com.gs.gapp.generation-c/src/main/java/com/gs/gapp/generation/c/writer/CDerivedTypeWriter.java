/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.c.CDerivedType;

/**
 * @author dsn
 *
 */
public abstract class CDerivedTypeWriter extends AbstractCTypeWriter {

	@ModelElement
	private CDerivedType cDerivedType;
	
}
