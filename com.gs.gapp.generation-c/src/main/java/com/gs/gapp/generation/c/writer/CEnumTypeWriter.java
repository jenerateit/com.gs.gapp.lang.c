/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import java.util.Iterator;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CEnumType;
import com.gs.gapp.metamodel.c.CEnumType.CEnumConstant;

/**
 * @author dsn
 *
 */
public class CEnumTypeWriter extends AbstractCDataTypeWriter {

	
	@ModelElement
	private CEnumType cEnumType;
	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wJavaDoc();
		wType();
		super.transform(ts);
		w("{");
		wEnumConstants(ts);
		w("}");
		wDataTypeVariables();
	}
	
	private void wEnumConstants(TargetSection ts) {
		Iterator<CEnumConstant> i = cEnumType.getConstants().iterator();  
		indent();
		while(i.hasNext())  
		{  
			CEnumConstant constToBeProccessed = i.next();
			wNL(); 
			w(constToBeProccessed.getName());
			if(constToBeProccessed.hasValue()) {
				w(" = ", constToBeProccessed.getValue());
			}
			if(i.hasNext()) {	
				w(",");
			}
		}
		wONL();
	}

	protected void wType() {
		w(cEnumType.getName());
	}

}
