/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CArrayType;

/**
 * @author dsn
 *
 */
public class CArrayTypeWriter extends CDerivedTypeWriter {

	@ModelElement
	private CArrayType cArrayType;
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wType(ts);
	}
	
	private void wDimensionsWithSizes() {
		for(Integer arrayDimensionSize : cArrayType.getDimensionSizes()) {
			w("[",arrayDimensionSize.toString(),"]");
		}
	}

	private void wDimension() {
			w("[");w("]");
	}
	
	public void makeArray(TargetSection ts) {
		if(cArrayType.getDimensionSizes().isEmpty()) {
			if(cArrayType.getDimensions() > 1) {
				throw new WriterException("Multi dimensional arrays must have size");
			}
			else {
				wDimension();
			}
		}
		else {
			wDimensionsWithSizes();
		}
	}
//
//	private void wName() {
//		w(cArrayType.getName());
//		
//	}
//
	protected void wType(TargetSection ts) {
		AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(cArrayType.getRealType());
		typeWriter.transform(ts);
	}
	

}
