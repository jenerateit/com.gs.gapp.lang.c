package com.gs.gapp.generation.c;


/**
 * 
 */
public enum DeveloperAreaTypeEnum {

	/**
	 * Allows for manual modifications within a function-body
	 */
	FUNCTION_BODY,
	/**
	 * Allows for manual modifications on includes section
	 */
	INCLUDES,
	/**
	 * Allows for manual modifications on macro definitions section
	 */
	MACRO_DEFINITIONS,
	/**
	 * Allows for manual modifications on typedefs section
	 */
	TYPEDEFS,
	/**
	 * Allows for manual modifications on global variables section
	 */
	GLOBAL_VARIABLES,
	/**
	 * Allows for manual modifications on module variables section
	 */
	MODULE_VARIABLES,
	/**
	 * Allows for manual modifications on global composite data types section
	 */
	DATA_TYPES,	
	/**
	 * Allows for manual additions of enum entries in an enumeration
	 */
	ENUM_CONSTANTS,
	/**
	 * Allows for manual modifications on local function prototypes
	 */
	LOCAL_FUNCTION_PROTOTYPES,
	/**
	 * Allows for manual modifications on global function prototypes
	 */
	GLOBAL_FUNCTION_PROTOTYPES,
	/**
	 * Allows for manual modifications on local functions
	 */
	LOCAL_FUNCTIONS,
	/**
	 * Allows for manual modifications on global functions
	 */
	GLOBAL_FUNCTIONS,
	;
}
