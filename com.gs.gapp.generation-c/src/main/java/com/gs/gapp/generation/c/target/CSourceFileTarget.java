/**
 * 
 */
package com.gs.gapp.generation.c.target;

import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.c.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.c.CFunction;
import com.gs.gapp.metamodel.c.CFunctionPrototype;
import com.gs.gapp.metamodel.c.CSourceFile;
import com.gs.gapp.metamodel.c.CVariable;

/**
 * Target for CSourceFile
 * 
 * @author dsn
 *
 */
public class CSourceFileTarget extends CFileTarget {

	@org.jenerateit.annotation.ModelElement
	private CSourceFile cSourceFile;


	/**
	 *
	 */
	public CSourceFileTarget() {
		super();
	}

	/**
	 * Source file specific sections are written by this class
	 */
	public static class CSourceFileWriter extends CFileWriter {

		@org.jenerateit.annotation.ModelElement
		private CSourceFile cSourceFile;

		@Override
		public void transform(TargetSection ts) throws WriterException {
			super.transform(ts);
			if (ts == CTargetDocument.MODULEVARIABLES) {
				wNL("/*==============================================================================\r\n" + 
						"*  MODULE VARIABLES\r\n" + 
						"*/");
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.MODULE_VARIABLES)) {
					wComment("manually written module variables should be located in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cSourceFile.getName()).append(".").append("moduleVariables").toString());
					for(CVariable variable : cSourceFile.getModuleVariables()) {
						WriterI localVariableWriter = getTransformationTarget().getWriterInstance(variable);
						if(localVariableWriter != null) {
							localVariableWriter.transform(ts);
						}
					}
					eDA();
				}
			} else if (ts == CTargetDocument.LOCALFUNCTIONPROTOTYPES) {
				wNL("/*==============================================================================\r\n" + 
						"*  LOCAL FUNCTION PROTOTYPES\r\n" + 
						"*/");
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.LOCAL_FUNCTION_PROTOTYPES)) {
					wComment("manually written local function prototypes should be located in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cSourceFile.getName()).append(".").append("localFunctionPrototypes").toString());

					for(CFunctionPrototype functionPrototype : cSourceFile.getLocalFunctionPrototypes()) {
						if(functionPrototype.getAssociatedFunction().getStorageSpecifier().getName().equalsIgnoreCase("static")) { 
							//TODO: I have to be sure if local functions are only static ones! For now i assume they are static
							WriterI localFunctionPrototypeWriter = getTransformationTarget().getWriterInstance(functionPrototype);
							if(localFunctionPrototypeWriter != null) {
								localFunctionPrototypeWriter.transform(ts);
							}
						}

					}
					eDA();
				}
			} else if (ts == CTargetDocument.LOCALFUNCTIONS) {
				wNL("/*==============================================================================\r\n" + 
						"*  LOCAL FUNCTIONS\r\n" + 
						"*/");
				for(CFunction cFunc : cSourceFile.getLocalFunctions()) {
					WriterI functionWriter = getTransformationTarget().getWriterInstance(cFunc);
					if(functionWriter != null) {
						functionWriter.transform(ts);
					}
				}
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.LOCAL_FUNCTIONS)) {
					wComment("manually written global functions should be located in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cSourceFile.getName()).append(".").append("localFunctions").toString());
					eDA();
				}
			} else if (ts == CTargetDocument.GLOBALFUNCTIONS) {
				wNL("/*==============================================================================\r\n" + 
						"*  GLOBAL FUNCTIONS\r\n" + 
						"*/");
				for(CFunction cFunc : cSourceFile.getGlobalFunctions()) {
					WriterI functionWriter = getTransformationTarget().getWriterInstance(cFunc);
					if(functionWriter != null) {
						functionWriter.transform(ts);
					}
				}
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.GLOBAL_FUNCTIONS)) {
					wComment("manually written global functions should be located in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cSourceFile.getName()).append(".").append("globalFunctions").toString());
					eDA();
				}
			}
		}

	}
}
