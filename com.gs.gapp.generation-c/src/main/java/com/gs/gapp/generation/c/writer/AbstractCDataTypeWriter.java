/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import java.util.Iterator;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.AbstractCDataTypes;

/**
 * @author dsn
 *
 */
public abstract class AbstractCDataTypeWriter extends AbstractCTypeWriter {

	@ModelElement
	AbstractCDataTypes abstractCDataType;


	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		w(" ");
		wIdentifierName();
		w(" ");
	}
	
	
	protected void wDataTypeVariables() {

		Iterator<String> i = abstractCDataType.getDataTypeVariables().iterator();  

		while(i.hasNext())  
		{  
			w (" "); w(i.next()); 
			if(i.hasNext()) {	
				w(",");
			}
		}  
		wNL(";");
	}
	
	protected void wIdentifierName() {
		w(abstractCDataType.getIdentifierName());		
	}
	
	

}
