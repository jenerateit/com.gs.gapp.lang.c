/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.c.target.CTargetDocument;
import com.gs.gapp.metamodel.c.CObjectLikeMacro;

/**
 * @author dsn
 *
 */
public class CObjectLikeMacroWriter extends CMacroWriter {
	
	@ModelElement
	private CObjectLikeMacro cObjectLikeMacro;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		if (ts == CTargetDocument.MACROS) {
			super.transform(ts);
		    w(cObjectLikeMacro.getName());
		    if(!cObjectLikeMacro.getReplacementTokenList().isEmpty()) {
		    	wReplacementTokens();
		    }
		    wNL();
		}

	}
	

}