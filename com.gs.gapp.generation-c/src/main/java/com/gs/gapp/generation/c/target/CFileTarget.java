/**
 * 
 */
package com.gs.gapp.generation.c.target;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.generation.c.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.c.GenerationGroupCOptions;
import com.gs.gapp.generation.c.writer.CWriter;
import com.gs.gapp.metamodel.c.AbstractCDataTypes;
import com.gs.gapp.metamodel.c.CDataTypes;
import com.gs.gapp.metamodel.c.CEnumType;
import com.gs.gapp.metamodel.c.CFile;
import com.gs.gapp.metamodel.c.CFunctionLikeMacro;
import com.gs.gapp.metamodel.c.CHeaderFile;
import com.gs.gapp.metamodel.c.CMacro;
import com.gs.gapp.metamodel.c.CMetaModelUtil;
import com.gs.gapp.metamodel.c.CObjectLikeMacro;
import com.gs.gapp.metamodel.c.CSourceFile;
import com.gs.gapp.metamodel.c.CTypedef;
import com.gs.gapp.metamodel.c.CVariable;

/**
 * This target file handles the extensions of the file. .c for source and .h for header
 * In addition include sections are also handled here. 
 *
 *@author dsn
 *
 */
public abstract class CFileTarget extends BasicTextTarget<CTargetDocument> {

	@org.jenerateit.annotation.ModelElement
	private CFile cFile;

	/**
	 * included headers
	 */
	private final Set<String> includes = new LinkedHashSet<>();

	/**
	 * included system headers
	 * note that for now we include by taking inputs as string
	 * it might be useful to use real system headers like stdio, stdlib etc. in the future
	 */
	private final Set<String> includeSystemHeaders = new LinkedHashSet<>();
	/**
	 *
	 */
	public CFileTarget() {
		super();
	}

	/**
	 * @return
	 */
	public Set<DeveloperAreaTypeEnum> getActiveDeveloperAreaTypes() {
		return EnumSet.of(DeveloperAreaTypeEnum.DATA_TYPES,
				DeveloperAreaTypeEnum.ENUM_CONSTANTS,
				DeveloperAreaTypeEnum.FUNCTION_BODY,
				DeveloperAreaTypeEnum.GLOBAL_FUNCTION_PROTOTYPES,
				DeveloperAreaTypeEnum.GLOBAL_FUNCTIONS,
				DeveloperAreaTypeEnum.GLOBAL_VARIABLES,
				DeveloperAreaTypeEnum.INCLUDES,
				DeveloperAreaTypeEnum.LOCAL_FUNCTION_PROTOTYPES,
				DeveloperAreaTypeEnum.LOCAL_FUNCTIONS,
				DeveloperAreaTypeEnum.MODULE_VARIABLES,
				DeveloperAreaTypeEnum.MACRO_DEFINITIONS,
				DeveloperAreaTypeEnum.TYPEDEFS);
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetI#getTargetURI()
	 */
	@Override
	public URI getTargetURI() {
		StringBuilder sb = new StringBuilder();
		String targetRoot = getTargetRoot();
		if (targetRoot.endsWith("/")) targetRoot.substring(0, targetRoot.lastIndexOf("/"));

		sb.append(targetRoot).append("/").append(getTargetPrefix()).append("/");

		if(cFile instanceof CSourceFile) {
			sb.append(CMetaModelUtil.getFilePath(this.cFile)).append(".c");
		} else if (cFile instanceof CHeaderFile) {
			sb.append(CMetaModelUtil.getFilePath(this.cFile)).append(".h");
		}

		try {
			return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}

	/**
	 * 
	 * @param library
	 * @return true if library added succesfully
	 */
	public boolean addInclude(String library) {
		if(!includes.contains(library)) {
			includes.add(library);
			return true;
		}
		return false;	
	}


	/**
	 * 
	 * @param systemHeaderName
	 * @return true if system header added succesfully
	 */
	public boolean addIncludeSystemHeader(String systemHeaderName) {
		if(!includeSystemHeaders.contains(systemHeaderName)) {
			includeSystemHeaders.add(systemHeaderName);
			return true;
		}
		return false;		
	}






	/**
	 * The section of a c file written in this class. 
	 * The sections are written in this class which are common sections of
	 * CSource file and CHeader file. 
	 * 
	 *@author dsn
	 *
	 */
	public static class CFileWriter extends CWriter {


		@org.jenerateit.annotation.ModelElement
		private CFile cFile;

		@SuppressWarnings("unused")
		private GenerationGroupCOptions generationGroupOptions;

		@Override
		public void transform(TargetSection ts) throws WriterException {
			if (ts == CTargetDocument.COMMENTS) {
				wCommentsHeader(cFile.getCommentHeader());
			}
			else if (ts == CTargetDocument.INCLUDES) {


				wNL("/*==============================================================================\r\n" + 
						"*  INCLUDES\r\n" + 
						"*/");
				cFile.getIncludesSystemHeaders();
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.INCLUDES)) {
					wComment("manually included libraries should be placed in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cFile.getName()).append(".").append("includes").toString());
					wIncludeSystemHeaders(cFile.getIncludesSystemHeaders());
					wInclude(cFile.getIncludes());
					eDA();
				}
			}
			else if (ts == CTargetDocument.MACROS) {
				wNL("/*==============================================================================\r\n" + 
						"*  MACRO DEFINITIONS\r\n" + 
						"*/");
				Set<CMacro> allMacros = cFile.getAllMacros();
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.MACRO_DEFINITIONS)) {
					wComment("manually written macro definitions should be placed in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cFile.getName()).append(".").append("definitions").toString());

					if(allMacros != null) {
						for (CObjectLikeMacro aOLikeMacro : cFile.getObjectLikeMacros()) {
							WriterI objectLikeMacroWriter = getTransformationTarget().getWriterInstance(aOLikeMacro);
							if (objectLikeMacroWriter != null) {
								objectLikeMacroWriter.transform(ts);
							}
						}
						for(CFunctionLikeMacro aFlikeMacro : cFile.getFunctionLikeMacros()) {
							WriterI functionLikeMacroWriter = getTransformationTarget().getWriterInstance(aFlikeMacro);
							if (functionLikeMacroWriter != null) {
								functionLikeMacroWriter.transform(ts);
							}
						}
					}

					//to enable manually written code from other generators
					wMacroDefinitionHardCoded();
					eDA();
				}

			} else if (ts == CTargetDocument.GLOBALVARIABLES) {
				wNL("/*==============================================================================\r\n" + 
						"*  GLOBAL VARIABLES\r\n" + 
						"*/");
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.GLOBAL_VARIABLES)) {
					wComment("manually written global variables should be located in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cFile.getName()).append(".").append("globalVariables").toString());
					for(CVariable variable : cFile.getGlobalVariables()) {
						WriterI globalVariableWriter = getTransformationTarget().getWriterInstance(variable);
						if(globalVariableWriter != null) {
							globalVariableWriter.transform(ts);
						}
					}
					eDA();
				}
			} else if (ts == CTargetDocument.GLOBALCOMPOSITEDATATYPEDECLARATIONS) {
				wNL("/*==============================================================================\r\n" + 
						"*  GLOBAL COMPOSITE DATA TYPE DECLARATIONS\r\n" + 
						"*/");
				Queue<AbstractCDataTypes> queue = new LinkedList<>();
				Set<AbstractCDataTypes> asSet = cFile.getGlobalDataTypeDeclarations();
				for(AbstractCDataTypes globalDataType : new LinkedHashSet<>(asSet)) {
					if(globalDataType instanceof CEnumType) { //First we add enums because they cannot have nested structure
						queue.add(globalDataType);
						asSet.remove(globalDataType);
					}

				}
				for(AbstractCDataTypes withoutNestedDataType : new LinkedHashSet<>(asSet)) {
					if(withoutNestedDataType != null) {
						if(withoutNestedDataType instanceof CDataTypes) {
							if(((CDataTypes) withoutNestedDataType).hasNestedDataType() == false) { //Then we add the ones without nested structure
								queue.add(withoutNestedDataType);
								asSet.remove(withoutNestedDataType);
							}
						}
					}
				}

				for(AbstractCDataTypes otherDataTypes : new LinkedHashSet<>(asSet)) {  // we handle the rest
					if(otherDataTypes != null) {
						queue.add(otherDataTypes);
						asSet.remove(otherDataTypes);
					}
				}
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.DATA_TYPES)) {
					wComment("manually written global composite datatypes should be located in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cFile.getName()).append(".").append("datatypes").toString());
				if(asSet.isEmpty()) {// we check if we sort all of the members already
					for(AbstractCDataTypes memberOfQueue : queue) {
						WriterI cDataTypeWriter = getTransformationTarget().getWriterInstance(memberOfQueue);
						if (cDataTypeWriter != null) {
							cDataTypeWriter.transform(ts);
						}
					}	
				}

					eDA();
				}
			} else if (ts == CTargetDocument.TYPEDEFS) {
				wNL("/*==============================================================================\r\n" + 
						"*  TYPEDEFS\r\n" + 
						"*/");
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.TYPEDEFS)) {
					wComment("manually written typedefs should be located in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cFile.getName()).append(".").append("typedefs").toString());
				for(CTypedef typedef : cFile.getTypeDefs()) {
					WriterI typeDefWriter = getTransformationTarget().getWriterInstance(typedef);
					if(typeDefWriter != null) {
						typeDefWriter.transform(ts);
					}
				}
					eDA();
				}
			}


		}

		public CFileWriter wCommentsHeader(List<String> commentHeader) {
			checkTarget();

			for (String i : commentHeader) {
				wNL(CTargetDocument.COMMENTS, i);
			}

			return this;
		}

		/**
		 * 
		 * @param set
		 * @return
		 */
		public CFileWriter wInclude(Set<String> set) {
			checkTarget();

			for (String i : set) {
				if (StringTools.isText(i)) {
					if (!getTransformationTarget().addInclude(i)) {
						// import must not be added
					} else {
						Set<String> currentIncludes = CFileTarget.class.cast(getTextTransformationTarget()).getNewTargetDocument().getSectionIncludes();
						if (!currentIncludes.contains(i)) {
							currentIncludes.add(i);
							wNL(CTargetDocument.INCLUDES, "#include \"", i, "\"");
						}
					}
				}
			}
			return this;
		}


		/**
		 * 
		 * @param set
		 * @return
		 */
		public CFileWriter wIncludeSystemHeaders(Set<String> set) {
			checkTarget();
			
			for (String i : set) {
				if (StringTools.isText(i)) {
					if (getTransformationTarget().addIncludeSystemHeader(i)) {
						Set<String> currentIncludes = CFileTarget.class.cast(getTextTransformationTarget()).getNewTargetDocument().getSectionIncludes();
						if (!currentIncludes.contains(i)) {
							currentIncludes.add(i);
							wNL(CTargetDocument.INCLUDES, "#include <", i, ">");
						} 
					}
				}
			}
			return this;
		}

		/**
		 *  This method lets user to add manually written to macro definitions section.
		 *  It works as wFunctionBody for functions. 
		 */
		protected void wMacroDefinitionHardCoded() {
		}

		/**
		 * 
		 */
		private void checkTarget() {
			if (!CFileTarget.class.isInstance(getTransformationTarget())) {
				throw new WriterException("The target '" + getTransformationTarget().getClass().getName() +
						"' in the TransformationContext is not of type '" +
						CFileTarget.class.getName() + "'", getTransformationTarget(), this);
			}
		}


		@Override
		public CFileTarget getTransformationTarget() {
			return (CFileTarget) super.getTransformationTarget();
		}

	}

}
