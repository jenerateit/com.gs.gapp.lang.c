/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CIntegerType;

/**
 * @author dsn
 *
 */
public class CIntegerTypeWriter extends AbstractCTypeWriter {
	
	@ModelElement
	private CIntegerType cIntegerType;

	@Override
	public void transform(TargetSection ts) throws WriterException {
		wType();		
	}
	
	
	protected void wType() {
		w(cIntegerType.getName());
	}
	


}
