/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.AbstractCDataTypes;
import com.gs.gapp.metamodel.c.CBasicType;
import com.gs.gapp.metamodel.c.CTypedef;
import com.gs.gapp.metamodel.c.CVoidType;

/**
 * @author dsn
 *
 */
public class CTypedefWriter extends AbstractCTypeWriter {

	@ModelElement
	private CTypedef cTypedef;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wJavaDoc();
		wType();
		w(" ");
		wExistingType(ts);
		if(cTypedef.getExistingCType() instanceof AbstractCDataTypes) {
			if(cTypedef.getExistingCType().getOwner() != null) {
				if(!cTypedef.getExistingCType().getOwner().equals(cTypedef)) {
					w(" ");
					wDefinedDataTypeName();
					w(" ");
					wNewIdentifierName();
					wNL(";");
				}
			}
		} else if ((cTypedef.getExistingCType() instanceof CBasicType) || (cTypedef.getExistingCType() instanceof CVoidType)) {
			w(" ");
			wNewIdentifierName();
			wNL(";");
		}

	}

	private void wDefinedDataTypeName() {
		AbstractCDataTypes abstarctCDataType = (AbstractCDataTypes) cTypedef.getExistingCType();
		w(abstarctCDataType.getIdentifierName());
	}

	public void wNewIdentifierName() {
		w(cTypedef.getNewIdentifierName());
	}

	//TODO: I need to find a way to handle Derived types. Arrays are a bit tricky!

	private void wExistingType(TargetSection ts) {
		if(cTypedef.getExistingCType().getOwner() != null) {
			if(cTypedef.getExistingCType().getOwner().equals(cTypedef)) { 
				AbstractCDataTypes existingType = (AbstractCDataTypes) cTypedef.getExistingCType();
				existingType.addDataTypeVariable(cTypedef.getNewIdentifierName());
				AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(cTypedef.getExistingCType()); 
				typeWriter.transform(ts);
			}
			else {
				w(cTypedef.getExistingCType().getName());
			}
		}
		else {
			w(cTypedef.getExistingCType().getName());
		}
	}

	protected void wType() {
		w(cTypedef.getName());
	}

}
