/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CStructType;

/**
 * @author dsn
 *
 */
public class CStructTypeWriter extends CDataTypeWriter {

	@ModelElement
	private CStructType cStructType;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wJavaDoc();
		wType();
		super.transform(ts);
	}
	
	protected void wType() {
		w(cStructType.getName());
	}
	

}
