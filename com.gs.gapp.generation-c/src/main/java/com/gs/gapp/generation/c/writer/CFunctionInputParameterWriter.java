/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.AbstractCDataTypes;
import com.gs.gapp.metamodel.c.AbstractCType;
import com.gs.gapp.metamodel.c.CDerivedType;
import com.gs.gapp.metamodel.c.CFunctionInputParameter;
import com.gs.gapp.metamodel.c.CPointerType;
import com.gs.gapp.metamodel.c.CTypedef;

/**
 * @author dsn
 *
 */
public class CFunctionInputParameterWriter extends CVariableWriter {

	@ModelElement
	private CFunctionInputParameter cInParam;

	private int starCounter = 0;
	/**
	 * 
	 */
	public CFunctionInputParameterWriter() {
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.c.writer.CVariableWriter#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {

		wStorageSpecifier();		
		wTypeQualifier();

		if(cInParam.getType() instanceof AbstractCDataTypes) {
			if((!(cInParam.getType() instanceof CTypedef))) {
				wCDataType(ts);
			} else {
				wTypeDefedType();
			}
			w(" ");
			w(cInParam.getName());
		} else if (cInParam.getType() instanceof CPointerType) {
			if( ((CDerivedType) cInParam.getType()).getRealType() instanceof AbstractCDataTypes) {
				if( ((CDerivedType) cInParam.getType()).getRealType() instanceof CTypedef) {
					w(((CTypedef) ((CDerivedType) cInParam.getType()).getRealType()).getNewIdentifierName());		
				} else {
					w(((CDerivedType) cInParam.getType()).getRealType().getName());
					w(" ");
					w(((AbstractCDataTypes) ((CDerivedType) cInParam.getType()).getRealType()).getIdentifierName());
				}
				makePointer();
				w(cInParam.getName());
			} else if (((CDerivedType) cInParam.getType()).getRealType() instanceof CPointerType) {
				AbstractCType<ModelElementI> tempPointer = handlePointerToPointer(((CDerivedType) cInParam.getType()).getRealType());
				hanldeTheCore(tempPointer,ts);
				makePointer();
				w(cInParam.getName());

			} else {
				if( ((CDerivedType) cInParam.getType()).getRealType() instanceof AbstractCDataTypes) {
					w(((CTypedef) ((CDerivedType) cInParam.getType()).getRealType()).getNewIdentifierName());	
					makePointer();
					w(cInParam.getName());
				} else if (((CDerivedType) cInParam.getType()).getRealType() instanceof CTypedef) {
					w(((CTypedef) ((CDerivedType) cInParam.getType()).getRealType()).getNewIdentifierName());
					makePointer();
					w(cInParam.getName());
				} else {
					super.transform(ts);
				}
			}
		} else {
			super.transform(ts);
		}
	}

	private void hanldeTheCore(AbstractCType<ModelElementI> tempPointer,TargetSection ts) {
		if( tempPointer instanceof CTypedef) {
			w(((CTypedef) tempPointer).getNewIdentifierName());
		} else {
			if(tempPointer instanceof AbstractCDataTypes) {
				w(tempPointer.getName());
				w(" ");
				w(((AbstractCDataTypes) tempPointer).getIdentifierName());
			} else {
				AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(tempPointer);
				typeWriter.transform(ts);
			}
		}
	}

	private AbstractCType<ModelElementI> handlePointerToPointer(AbstractCType<ModelElementI> input) {
		starCounter++;
		if(input instanceof CPointerType) {
			return (handlePointerToPointer(((CPointerType) input).getRealType()));
		} else {
			return input;
		}
	}	

	private void makePointer() {
		if(starCounter > 0 ) {
			w(" ");
			for (int i = 0; i<starCounter; i++) {
				w("*");
			}
		} else {
			if(cInParam.getType() instanceof CPointerType) {
				w(" *");
			}		
		}
	}

	protected void wCDataType(TargetSection ts) {
		w(cInParam.getType().getName());
		w(" ");
		w(((AbstractCDataTypes) cInParam.getType()).getIdentifierName());
	}

	protected void wTypeQualifier() {
		if(!cInParam.getTypeQualifier().getName().equals("")) {
			w(cInParam.getTypeQualifier().getName());
			w(" ");
		}
	}

	protected void wStorageSpecifier() {
		if(!cInParam.getStorageSpecifier().getName().equals("")) {
			w(cInParam.getStorageSpecifier().getName());
			w(" ");
		}

	}

}
