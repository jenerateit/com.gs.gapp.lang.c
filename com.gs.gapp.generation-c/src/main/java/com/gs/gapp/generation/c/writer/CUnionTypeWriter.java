/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CUnionType;

/**
 * @author dsn
 *
 */
public class CUnionTypeWriter extends CDataTypeWriter {

	@ModelElement
	private CUnionType unionType;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wJavaDoc();
		wType();
		super.transform(ts);
	}
	
	protected void wType() {
		w(unionType.getName());
	}
	

}
