package com.gs.gapp.generation.c;

import java.util.ArrayList;
import java.util.List;

public enum DeveloperAreaRule {
	
	ALL,
	NONE,
	DEFAULT;

	/**
	 * @return
	 */
	public static List<String> getNames() {
		List<String> result = new ArrayList<>();
		
		for (DeveloperAreaRule rule : values()) {
			result.add(rule.name());
		}
		
		return result;
	}
}
