/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CModuleVariable;

/**
 * @author dsn
 *
 */
public class CModuleVariableWriter extends CVariableWriter {
	
	@ModelElement
	private CModuleVariable cLocalVariable;
	
	
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		wNL(";");
	}
}
