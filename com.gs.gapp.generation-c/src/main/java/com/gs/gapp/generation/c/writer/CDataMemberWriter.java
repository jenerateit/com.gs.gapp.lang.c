/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CDataMember;

/**
 * @author dsn
 *
 */
public class CDataMemberWriter extends AbstractCVariableWriter {

	@ModelElement
	CDataMember cDataMember;

	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		wType(ts);
		w(" ");
		wName();
		wNL(";");
	}


	protected void wType(TargetSection ts) {
		AbstractCTypeWriter typeWriter = (AbstractCTypeWriter) getTransformationTarget().getWriterInstance(cDataMember.getType());
		if(typeWriter instanceof CBasicTypeWriter || typeWriter instanceof CIntegerTypeWriter) {
			typeWriter.transform(ts);
		}
		else if (typeWriter instanceof AbstractCDataTypeWriter) {
			w(cDataMember.getType().getName()); 
			//This line is here because when "struct" is not enough to declare a struct as member we also need name of the struct
			//nested structs handled in DataTypeWriter.
			//defined enums can also be used as variable so that we check if the typewriter is instance of abstractCDataTypeWriter 
		} else if (typeWriter instanceof CTypedefWriter) {
			// we don't want a typedef to be defined in a CDataType member, that's why this else if does nothing
		} else {
			typeWriter.transform(ts);
		}

	}

	protected void wName() {
		w(cDataMember.getName());
	}

	public void handleNestedDataType(TargetSection ts) {
		AbstractCDataTypeWriter typeWriter = (AbstractCDataTypeWriter) getTransformationTarget().getWriterInstance(cDataMember.getType());
		typeWriter.transform(ts);
	}



}
