/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CVoidType;

/**
 * @author dsn
 *
 */
public class CVoidTypeWriter extends AbstractCTypeWriter {
	
	@ModelElement
	private CVoidType voidType;
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		wType();				
	}

	private void wType() {
		w(voidType.getName());		
	}
 
}
