package com.gs.gapp.generation.c;

import java.util.Set;

import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;

import com.gs.gapp.generation.basic.AbstractWriterLocator;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.generation.c.target.CFileTarget;
import com.gs.gapp.generation.c.target.CHeaderFileTarget;
import com.gs.gapp.generation.c.target.CHeaderFileTarget.CHeaderFileWriter;
import com.gs.gapp.generation.c.target.CSourceFileTarget;
import com.gs.gapp.generation.c.target.CSourceFileTarget.CSourceFileWriter;
import com.gs.gapp.generation.c.writer.CArrayTypeWriter;
import com.gs.gapp.generation.c.writer.CBasicTypeWriter;
import com.gs.gapp.generation.c.writer.CDataMemberWriter;
import com.gs.gapp.generation.c.writer.CDataTypeWriter;
import com.gs.gapp.generation.c.writer.CEnumTypeWriter;
import com.gs.gapp.generation.c.writer.CFunctionInputParameterWriter;
import com.gs.gapp.generation.c.writer.CFunctionLikeMacroWriter;
import com.gs.gapp.generation.c.writer.CFunctionPrototypeWriter;
import com.gs.gapp.generation.c.writer.CFunctionWriter;
import com.gs.gapp.generation.c.writer.CGlobalVariableWriter;
import com.gs.gapp.generation.c.writer.CIntegerTypeWriter;
import com.gs.gapp.generation.c.writer.CModuleVariableWriter;
import com.gs.gapp.generation.c.writer.CObjectLikeMacroWriter;
import com.gs.gapp.generation.c.writer.CPointerTypeWriter;
import com.gs.gapp.generation.c.writer.CStructTypeWriter;
import com.gs.gapp.generation.c.writer.CTypedefWriter;
import com.gs.gapp.generation.c.writer.CUnionTypeWriter;
import com.gs.gapp.generation.c.writer.CVoidTypeWriter;
import com.gs.gapp.metamodel.c.CArrayType;
import com.gs.gapp.metamodel.c.CBasicType;
import com.gs.gapp.metamodel.c.CDataMember;
import com.gs.gapp.metamodel.c.CDataTypes;
import com.gs.gapp.metamodel.c.CEnumType;
import com.gs.gapp.metamodel.c.CFunction;
import com.gs.gapp.metamodel.c.CFunctionInputParameter;
import com.gs.gapp.metamodel.c.CFunctionLikeMacro;
import com.gs.gapp.metamodel.c.CFunctionPrototype;
import com.gs.gapp.metamodel.c.CGlobalVariable;
import com.gs.gapp.metamodel.c.CHeaderFile;
import com.gs.gapp.metamodel.c.CIntegerType;
import com.gs.gapp.metamodel.c.CModuleVariable;
import com.gs.gapp.metamodel.c.CObjectLikeMacro;
import com.gs.gapp.metamodel.c.CPointerType;
import com.gs.gapp.metamodel.c.CSourceFile;
import com.gs.gapp.metamodel.c.CStructType;
import com.gs.gapp.metamodel.c.CTypedef;
import com.gs.gapp.metamodel.c.CUnionType;
import com.gs.gapp.metamodel.c.CVoidType;


/**
 * @author dsn
 *
 */
public class WriterLocatorC extends AbstractWriterLocator implements WriterLocatorI {

	public WriterLocatorC(Set<Class<? extends TargetI<?>>> targetClasses) {
		super(targetClasses);
		// --- mappers for generation decision
		addWriterMapperForGenerationDecision(new WriterMapper(CHeaderFile.class, CHeaderFileTarget.class, CHeaderFileWriter.class));
		addWriterMapperForGenerationDecision(new WriterMapper(CSourceFile.class, CSourceFileTarget.class, CSourceFileWriter.class));

	    // --- mappers for writer delegation
		addWriterMapperForWriterDelegation(new WriterMapper(CObjectLikeMacro.class, CFileTarget.class, CObjectLikeMacroWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CFunctionLikeMacro.class, CFileTarget.class, CFunctionLikeMacroWriter.class));

		addWriterMapperForWriterDelegation(new WriterMapper(CGlobalVariable.class, CFileTarget.class, CGlobalVariableWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CModuleVariable.class, CFileTarget.class, CModuleVariableWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CFunctionInputParameter.class, CFileTarget.class, CFunctionInputParameterWriter.class));

		
		
		addWriterMapperForWriterDelegation(new WriterMapper(CBasicType.class, CFileTarget.class, CBasicTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CIntegerType.class, CFileTarget.class, CIntegerTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CVoidType.class, CFileTarget.class, CVoidTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CEnumType.class, CFileTarget.class, CEnumTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CTypedef.class, CFileTarget.class, CTypedefWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CArrayType.class, CFileTarget.class, CArrayTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CPointerType.class, CFileTarget.class, CPointerTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CStructType.class, CFileTarget.class, CStructTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CUnionType.class, CFileTarget.class, CUnionTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CDataTypes.class, CFileTarget.class, CDataTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CDataMember.class, CFileTarget.class, CDataMemberWriter.class));
		
		addWriterMapperForWriterDelegation(new WriterMapper(CFunction.class, CFileTarget.class, CFunctionWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(CFunctionPrototype.class, CFileTarget.class, CFunctionPrototypeWriter.class));


	}
}
