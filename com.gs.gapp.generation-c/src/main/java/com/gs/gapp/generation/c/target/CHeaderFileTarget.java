/**
 * 
 */
package com.gs.gapp.generation.c.target;

import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.c.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.c.CFunctionPrototype;
import com.gs.gapp.metamodel.c.CHeaderFile;

/**
 * Target for CHeaderFile
 * 
 * @author dsn
 *
 */
public class CHeaderFileTarget extends CFileTarget {

	@org.jenerateit.annotation.ModelElement
	private CHeaderFile cHeaderFile;


	/**
	 *
	 */
	public CHeaderFileTarget() {
		super();
	}

	/**
	 * Header file specific sections are written by this class
	 */
	public static class CHeaderFileWriter extends CFileWriter {
		@org.jenerateit.annotation.ModelElement
		private CHeaderFile cHeaderFile;

		boolean includeGuardsWritten = false;
		boolean endifWritten = false;

		@Override
		public void transform(TargetSection ts) throws WriterException {
			if(!includeGuardsWritten && ts != CTargetDocument.COMMENTS) {
				writeIncludeGuards();
				includeGuardsWritten = true;
			}
			super.transform(ts);

			if (ts == CTargetDocument.GLOBALFUNCTIONPROTOTYPES) {
				wNL("/*==============================================================================\r\n" + 
						"*  GLOBAL FUNCTION PROTOTYPES\r\n" + 
						"*/");
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.GLOBAL_FUNCTION_PROTOTYPES)) {
					wComment("manually written global function prototypes should be located in between DA-START and DA-ELSE");
					bDA(new StringBuffer(cHeaderFile.getName()).append(".").append("globalFunctionPrototypes").toString());
				for(CFunctionPrototype cFuncPrototype : cHeaderFile.getGlobalFunctionPrototypes()) {
					WriterI functionPrototypeWriter = getTransformationTarget().getWriterInstance(cFuncPrototype);
					if(functionPrototypeWriter != null) {
						functionPrototypeWriter.transform(ts);
					}
				}
					eDA();
				}
			} else if (ts == CTargetDocument.ENDIFFORHEADER) {
				if(!endifWritten) {
					writeEndif();
					endifWritten = true;
				}
			}

		}
		/**
		 * end of the guard
		 */
		private void writeEndif() {
			wNL("#endif");			
		}
		
		/**
		 * Guards are important for header files because if a header included more than once
		 * it would give a compiler error
		 */
		private void writeIncludeGuards() {
			wNL("#ifndef ", cHeaderFile.getName().toUpperCase(),"_H");
			wNL("#define ", cHeaderFile.getName().toUpperCase(),"_H");
		}
	}
}
