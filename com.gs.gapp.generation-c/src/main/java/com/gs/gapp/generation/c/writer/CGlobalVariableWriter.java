/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.c.CBasicType;
import com.gs.gapp.metamodel.c.CGlobalVariable;
import com.gs.gapp.metamodel.c.CVoidType;

/**
 * @author dsn
 *
 */
public class CGlobalVariableWriter extends CVariableWriter {

	@ModelElement
	private CGlobalVariable cGlobalVariable;

	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		
		if(cGlobalVariable.getType() instanceof CBasicType) {
			if(cGlobalVariable.getStorageSpecifierAsString().equals("auto") || cGlobalVariable.getStorageSpecifierAsString().equals("register")) {
				throw new WriterException("StorageSpecifier "+ cGlobalVariable.getStorageSpecifierAsString() +" cannot be used with global variables");
			}
		}
		if(cGlobalVariable.getType() instanceof CVoidType) {
			throw new WriterException("Storage size of variable: " + cGlobalVariable.getName() + " is not known therefore it cannot be a global variable");
		}
		
		wNL(";");
		
		
	}

}
