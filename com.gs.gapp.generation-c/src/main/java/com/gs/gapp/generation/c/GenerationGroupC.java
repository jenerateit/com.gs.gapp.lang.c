package com.gs.gapp.generation.c;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.WriterLocatorI;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.generation.c.target.CHeaderFileTarget;
import com.gs.gapp.generation.c.target.CSourceFileTarget;
import com.gs.gapp.metamodel.basic.ModelElementCache;


/**
 *
 */
public class GenerationGroupC extends AbstractGenerationGroup implements GenerationGroupConfigI {
	
	private final WriterLocatorI writerLocator;


	/**
	 *
	 */
	public GenerationGroupC() {
		super(new ModelElementCache());

		addTargetClass(CSourceFileTarget.class);
		addTargetClass(CHeaderFileTarget.class);

		writerLocator = new WriterLocatorC(getAllTargets());
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.util.DetailsI#getDescription()
	 */
	@Override
	public String getDescription() {
        return getName();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.util.DetailsI#getName()
	 */
	@Override
	public String getName() {
        return getClass().getSimpleName();
	}

	/**
	 * @return the writerLocator
	 */
	@Override
	public WriterLocatorI getWriterLocator() {
		return writerLocator;
	}
}

