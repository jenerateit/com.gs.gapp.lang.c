/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.c.target.CTargetDocument;
import com.gs.gapp.metamodel.c.CFunctionLikeMacro;

/**
 * @author dsn
 *
 */
public class CFunctionLikeMacroWriter extends CMacroWriter {

	@ModelElement
	private CFunctionLikeMacro cFunctionLikeMacro;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		if (ts == CTargetDocument.MACROS) {
			super.transform(ts);
		    w(cFunctionLikeMacro.getName());
		    wFunctionParameters();
		    if(!cFunctionLikeMacro.getReplacementTokenList().isEmpty()) {
		    	wReplacementTokens();
		    }
		    wNL();
		}
	}
	
	protected void wFunctionParameters() {
		w("(",cFunctionLikeMacro.getParameters(),")");
	}
}
