/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.basic.writer.ModelElementWriter;
import com.gs.gapp.generation.c.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.c.GenerationGroupCOptions;
import com.gs.gapp.generation.c.target.CFileTarget;
import com.gs.gapp.metamodel.c.CModelElement;

/**
 * @author dsn
 *
 */
public abstract class CWriter extends ModelElementWriter {
	
	@ModelElement
	private CModelElement<?> cModelElement;
	
	private GenerationGroupCOptions generationGroupOptions;
	
	/**
	 * Method to write block comments
	 *
	 * @param comment the comment to write
	 * @param ts the {@link TargetSection} to write to
	 * @param java if true start the comment with "//**"  otherwise start with "//*"
	 * @return an instance of this writer object
	 */
	private CWriter wBlockComment(TargetSection ts, boolean java, CharSequence... comment) {
		if (comment == null || comment.length == 0) {
			// found an empty comment -> do nothing

		} else {
			if (ts != null) {
				wNL(ts, java ? "/**" : "/*");
			} else {
				wNL(java ? "/**" : "/*");
			}
			for (CharSequence c : comment) {
				for (CharSequence s : getLines(c)) {
					if (ts != null) {
						if (s.length() == 0) {
							wNL(ts, " *");
						} else {
						    wNL(ts, " * ", s.toString().replace("/*", "??").replace("*/", "??").trim());
						}
					} else {
						if (s.length() == 0) {
							wNL(" *");
						} else {
						    wNL(" * ", s.toString().replace("/*", "??").replace("*/", "??").trim());
						}
					}
				}
			}
			if (ts != null) {
				wNL(ts, " */");
			} else {
				wNL(" */");
			}
		}
		return this;
	}
	
	
	/**
	 * Writes an invisible Java block comment to the current working target section
	 *
	 * @param comment the block comment to write
	 * @see #wBlockComment(TargetSection, String...)
	 * @return an instance of this writer object
	 */
	public CWriter wBlockComment(String... comment) {
		return wBlockComment(null, comment);
	}

	/**
	 * Write invisible Java block comment(s) to the current working {@link TargetSection} ts.
	 * If ts is null the current working target section is used to write to.
	 * The java code
	 * <pre>
	 * <code>
	 * wBlockComment(JavaTargetDocument.OPERATION, "This is an example");
	 * </code>
	 * </pre>
	 * Will be written in the Java target document as
	 * <pre>
	 * <code>
	 * &#x2F;*
	 *  * the comment
	 *  *&#x2F;
	 * </code>
	 * </pre>
	 *
	 * @param comments the block comment to write
	 * @param ts the target section to write to
	 * @return an instance of this writer object
	 */
	public CWriter wBlockComment(TargetSection ts, String... comments) {
		return wBlockComment(ts, false, comments);
	}
	
	/**
	 * Helper method to parse text segments for line breaks.
	 * The text segment will be split into separate lines.
	 * Leading and trailing empty lines will be discarded.
	 *
	 * @see BufferedReader#readLine()
	 * @param text the text segment to parse
	 * @return an array of lines
	 */
	protected CharSequence[] getLines(CharSequence text) {
		// TODO this is content that could be moved into the StringTools
		if (StringTools.isNotEmpty(text.toString())) {
			List<String> lines = new ArrayList<>();
			BufferedReader br = new BufferedReader(new StringReader(text.toString()));
			String s = null;
			try {
				while ((s = br.readLine()) != null) {
					lines.add(s);
				}

			} catch (IOException e) {
				throw new WriterException("Error while parse a text segment for line breaks",
						e, getTransformationTarget(), this);
			}
			
			if (lines.size() > 1) {
				String firstLine = lines.get(0);
				String lastLine = lines.get(lines.size()-1);
				if (lastLine == null || lastLine.trim().length() == 0) {
				    lines.remove(lines.size()-1);
				}
				if (firstLine == null || firstLine.trim().length() == 0) {
				    lines.remove(0);
				}
			}

			return lines.toArray(new String[lines.size()]);
		}
		return new String[] {};
	}
	
	protected boolean isDeveloperAreaTypeActive(DeveloperAreaTypeEnum devAreaType) {
		if (devAreaType == null) throw new NullPointerException("parameter 'devArea' must not be null");
		
		boolean result = false;
		
		switch (this.getGenerationGroupOptions().getDeveloperAreaRule()) {
		case ALL:
			result = true;
			break;
		case DEFAULT:
			if (getTransformationTarget() instanceof CFileTarget) {
				CFileTarget cTarget = getTransformationTarget();
				result = cTarget.getActiveDeveloperAreaTypes().contains(devAreaType);
			}
			break;
		case NONE:
			result = false;
			break;
		default:
			throw new WriterException("unhandled developer area rule '" + this.getGenerationGroupOptions().getDeveloperAreaRule() + "' found", getTransformationTarget(), this);
		}
		
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.writer.ModelElementWriter#getGenerationGroupOptions()
	 */
	public GenerationGroupCOptions getGenerationGroupOptions() {
		if (this.generationGroupOptions == null) {
			this.generationGroupOptions = new GenerationGroupCOptions(this);
		}
		
		return this.generationGroupOptions;
	}
	
	@Override
	public CFileTarget getTransformationTarget() {
		return (CFileTarget) super.getTransformationTarget();
	}
	

	/**
	 * @param modelElement
	 * @return
	 */
	public static String getJavaDoc(CModelElement<?> modelElement) {
		String result = "";
		if (StringTools.isText(modelElement.getBody())) {
			result = modelElement.getBody() == null ? "" : modelElement.getBody();
		}

//		if (modelElement instanceof CTypeI) {
//			// --- add information about the generator and the model, to help users understand how things are related to each other
//			StringBuilder generatorInfo = GeneratorInfo.formatGeneratorInfo(modelElement);
//			if (generatorInfo != null && generatorInfo.length() > 0) {
//				if (result.length() > 0) {
//					result = new StringBuilder(result).append(System.lineSeparator()).append(generatorInfo).toString();
//				} else {
//					result = generatorInfo.toString();
//				}
//			}
//		}
//		
		return result;
	}

	/**
	 * @return
	 */
	public String getJavaDoc() {
		return getJavaDoc(this.cModelElement);
	}

	/**
	 * Writes a Java bock comment for the related element.
	 *
	 * @return
	 */
	protected ModelElementWriter wJavaDoc() {
		String javaDoc = getJavaDoc();
		if (javaDoc != null && javaDoc.trim().length() > 0) {
		    wJavaBlockComment(javaDoc);
		}
		return this;
	}
	
	/**
	 * Write visible Java block comment(s) to the current working target section
	 *
	 * @param comments the block comment to write
	 * @see #wJavaBlockComment(TargetSection, String...)
	 * @return an instance of this writer object
	 */
	public CWriter wJavaBlockComment(String... comments) {
		return wJavaBlockComment(null, comments);
	}
	
	/**
	 * Write visible Java block comment(s) to the current working {@link TargetSection} ts.
	 * If ts is null the current working target section is used to write to.
	 * The java code
	 * <pre>
	 * <code>
	 * wJavaBlockComment(JavaTargetDocument.OPERATION, "This is an example");
	 * </code>
	 * </pre>
	 * Will be written in the Java target document as
	 * <pre>
	 * <code>
	 * &#x2F;**
	 *  * the comment
	 *  *&#x2F;
	 * </code>
	 * </pre>
	 *
	 * @param comments the comment(s) to write into a block
	 * @param ts the target section to write to
	 * @return an instance of this writer object
	 */
	public CWriter wJavaBlockComment(TargetSection ts, String... comments) {
		return wBlockComment(ts, true, comments);
	}



}
