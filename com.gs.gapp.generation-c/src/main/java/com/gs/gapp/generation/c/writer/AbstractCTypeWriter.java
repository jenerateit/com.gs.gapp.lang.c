/**
 * 
 */
package com.gs.gapp.generation.c.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.AbstractCType;

/**
 * @author dsn
 *
 */
public abstract class AbstractCTypeWriter extends CWriter {

	@ModelElement
	private AbstractCType<ModelElementI> cType;

	@Override
	public void transform(TargetSection ts) throws WriterException {
	}
}
