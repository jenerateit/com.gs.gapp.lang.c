package com.gs.gapp.generation.c;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.log.LogService;



/**
 * @author hrr
 *
 */
@Component
public class GenerationGroupCProvider implements GenerationGroupProviderI {
	
	private LogService log;

	/**
	 * 
	 */
	public GenerationGroupCProvider() {}

    public void addLogService(LogService log) {
    	this.log = log;
    }
    
    public void removeLogService(LogService log) {
    	this.log = null;
    }
	
    
    public void startup(ComponentContext context) {
    	this.log.log(LogService.LOG_INFO, "startup of " + getClass().getSimpleName());
    }
    
    public void shutdown(ComponentContext context) {
    	this.log.log(LogService.LOG_INFO, "shutdown of " + getClass().getSimpleName());
    }

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupC();
	}
}
