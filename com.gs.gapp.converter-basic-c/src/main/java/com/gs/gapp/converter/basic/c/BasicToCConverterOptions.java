package com.gs.gapp.converter.basic.c;

import java.io.Serializable;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.analytics.AbstractAnalyticsConverterOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;

public class BasicToCConverterOptions extends AbstractAnalyticsConverterOptions {
	
	public BasicToCConverterOptions(ModelConverterOptions options) {
		super(options);
	}

	@Override
	public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
		Set<OptionDefinition<? extends Serializable>> optionDefinitions = super.getOptionDefinitions();
		return optionDefinitions;
	}
}
