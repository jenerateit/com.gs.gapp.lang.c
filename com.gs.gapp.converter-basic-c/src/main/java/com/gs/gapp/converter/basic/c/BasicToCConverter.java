package com.gs.gapp.converter.basic.c;

import java.util.List;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;


/**
 * @author mmt
 *
 */
public class BasicToCConverter extends AbstractAnalyticsConverter {
	
	
	private BasicToCConverterOptions converterOptions;

	/**
	 * @param javaToElementMapper
	 */
	public BasicToCConverter() {
		super(new ModelElementCache());
	}
	
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		this.converterOptions = new BasicToCConverterOptions(getOptions());
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters
		
		// --- slave element converters

		return result;
	}

	/**
	 * @return
	 */
	@Override
	public BasicToCConverterOptions getConverterOptions() {
		return converterOptions;
	}
}
