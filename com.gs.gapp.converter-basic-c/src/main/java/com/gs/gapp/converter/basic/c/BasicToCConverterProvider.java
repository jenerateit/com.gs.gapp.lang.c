package com.gs.gapp.converter.basic.c;

import org.jenerateit.modelconverter.ModelConverterI;
import org.osgi.service.component.annotations.Component;

/**
 * @author hrr
 *
 */
@Component
public class BasicToCConverterProvider extends AbstractCConverterProvider {

	/**
	 * 
	 */
	public BasicToCConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new BasicToCConverter();
	}
}
