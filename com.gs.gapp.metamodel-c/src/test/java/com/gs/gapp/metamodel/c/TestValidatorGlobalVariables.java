/**
 * 
 */
package com.gs.gapp.metamodel.c;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.junit.Assert;
import org.junit.Test;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.validation.ValidatorGlobalVariables;

/**
 * @author dsn
 *
 */
public class TestValidatorGlobalVariables {


	/**
	 * 
	 */
	@Test
	public void testGlobalVariablesHaveUniqueNames() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CSourceFile cSourceFile = new CSourceFile("examplefile");
		sourceFiles.add(cSourceFile);
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		
		
		
		CHeaderFile cHeaderFile = new CHeaderFile("examplefileheader");
		sourceFiles.add(cHeaderFile);
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummytwo", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummytwo", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummytwo", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummytwo", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummytwo", cSourceFile, "1", CBasicType.BasicType.INT.getType()));
	
		ValidatorGlobalVariables validatorToBeTested = new ValidatorGlobalVariables();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 8", result.size() == 8);
	}
	
	/**
	 * 
	 */
	@Test
	public void testGlobalVariablesCannotBeRegister() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CSourceFile cSourceFile = new CSourceFile("examplefile");
		sourceFiles.add(cSourceFile);
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummytwo", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummythree", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyfour", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyfive", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		
		
		
		CHeaderFile cHeaderFile = new CHeaderFile("examplefileheader");
		sourceFiles.add(cHeaderFile);
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummyone", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummytwo", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummythree", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummyfour", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummyfive", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
	
		ValidatorGlobalVariables validatorToBeTested = new ValidatorGlobalVariables();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 10", result.size() == 10);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void testGlobalVariablesCannotBeAuto() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CSourceFile cSourceFile = new CSourceFile("examplefile");
		sourceFiles.add(cSourceFile);
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummytwo", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummythree", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyfour", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cSourceFile.addGlobalVariable(new CGlobalVariable("dummyfive", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		
		
		
		CHeaderFile cHeaderFile = new CHeaderFile("examplefileheader");
		sourceFiles.add(cHeaderFile);
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummyone", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummytwo", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummythree", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummyfour", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cHeaderFile.addGlobalVariable(new CGlobalVariable("dummyfive", cHeaderFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
	
		ValidatorGlobalVariables validatorToBeTested = new ValidatorGlobalVariables();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 10", result.size() == 10);
	}


}
