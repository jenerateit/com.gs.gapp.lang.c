/**
 * 
 */
package com.gs.gapp.metamodel.c;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.junit.Assert;
import org.junit.Test;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.validation.ValidatorModuleVariables;

/**
 * @author dsn
 *
 */
public class TestValidatorLocalVariables {

	/**
	 * 
	 */
	@Test
	public void testLocalVariablesMustBeStatic() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CSourceFile cSourceFile = new CSourceFile("examplefile");
		sourceFiles.add(cSourceFile);
		cSourceFile.addModuleVariable(new CModuleVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cSourceFile.addModuleVariable(new CModuleVariable("dummytwo", cSourceFile, "2", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.EXTERN));
		cSourceFile.addModuleVariable(new CModuleVariable("dummythree", cSourceFile, "3", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.NONE));
		cSourceFile.addModuleVariable(new CModuleVariable("dummyfour", cSourceFile, "4", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cSourceFile.addModuleVariable(new CModuleVariable("dummyfive", cSourceFile, "5", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
	
		ValidatorModuleVariables validatorToBeTested = new ValidatorModuleVariables();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 4", result.size() == 4);
	}
	

	/**
	 * 
	 */
	@Test
	public void testLocalVariablesHaveUniqueNames() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CSourceFile cSourceFile = new CSourceFile("examplefile");
		sourceFiles.add(cSourceFile);
		cSourceFile.addModuleVariable(new CModuleVariable("dummyone", cSourceFile, "1", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addModuleVariable(new CModuleVariable("dummyone", cSourceFile, "2", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addModuleVariable(new CModuleVariable("dummyone", cSourceFile, "3", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addModuleVariable(new CModuleVariable("dummyone", cSourceFile, "4", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addModuleVariable(new CModuleVariable("dummyone", cSourceFile, "5", CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
	
		ValidatorModuleVariables validatorToBeTested = new ValidatorModuleVariables();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 4", result.size() == 4);
	}


}
