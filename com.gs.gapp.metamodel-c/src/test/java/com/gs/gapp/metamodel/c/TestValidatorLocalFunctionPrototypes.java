/**
 * 
 */
package com.gs.gapp.metamodel.c;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.junit.Assert;
import org.junit.Test;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.validation.ValidatorLocalFunctionPrototypes;

/**
 * @author dsn
 *
 */
public class TestValidatorLocalFunctionPrototypes {

	/**
	 * 
	 */
	@Test
	public void testLocalFunctionPrototypesMustBeStatic() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CSourceFile cSourceFile = new CSourceFile("examplefile");
		sourceFiles.add(cSourceFile);
		cSourceFile.addLocalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.NONE));
		cSourceFile.addLocalFunction(new CFunction("dummytwo", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.EXTERN));
		cSourceFile.addLocalFunction(new CFunction("dummythree", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cSourceFile.addLocalFunction(new CFunction("dummyfour", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cSourceFile.addLocalFunction(new CFunction("dummysix", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
	
		ValidatorLocalFunctionPrototypes validatorToBeTested = new ValidatorLocalFunctionPrototypes();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 4", result.size() == 4);
	}
	

	
	/**
	 * 
	 */
	@Test
	public void testLocalFunctionsHaveUniqueNames() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CSourceFile cSourceFile = new CSourceFile("examplefile");
		sourceFiles.add(cSourceFile);
		cSourceFile.addLocalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addLocalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addLocalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addLocalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addLocalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
	
		ValidatorLocalFunctionPrototypes validatorToBeTested = new ValidatorLocalFunctionPrototypes();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 4", result.size() == 4);
	}


}
