/**
 * 
 */
package com.gs.gapp.metamodel.c;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.junit.Assert;
import org.junit.Test;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.validation.ValidatorGlobalFunctions;

/**
 * @author dsn
 *
 */
public class TestValidatorGlobalFunctions {


	
	/**
	 * 
	 */
	@Test
	public void testGlobalFunctionsHaveUniqueNames() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CSourceFile cSourceFile = new CSourceFile("examplefile");
		sourceFiles.add(cSourceFile);
		cSourceFile.addGlobalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addGlobalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addGlobalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addGlobalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
		cSourceFile.addGlobalFunction(new CFunction("dummyone", cSourceFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC));
	
		ValidatorGlobalFunctions validatorToBeTested = new ValidatorGlobalFunctions();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 4", result.size() == 4);
	}


}
