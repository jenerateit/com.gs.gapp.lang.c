/**
 * 
 */
package com.gs.gapp.metamodel.c;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.junit.Assert;
import org.junit.Test;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.validation.ValidatorGlobalFunctionPrototypes;

/**
 * @author dsn
 *
 */
public class TestValidatorGlobalFunctionPrototypes {


	
	/**
	 * 
	 */
	@Test
	public void testLocalFunctionsHaveUniqueNames() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();
		CHeaderFile cHeaderFile = new CHeaderFile("examplefileheader");
		sourceFiles.add(cHeaderFile);
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyone", cHeaderFile,CBasicType.BasicType.INT.getType()));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyone", cHeaderFile,CBasicType.BasicType.INT.getType()));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyone", cHeaderFile,CBasicType.BasicType.INT.getType()));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyone", cHeaderFile,CBasicType.BasicType.INT.getType()));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyone", cHeaderFile,CBasicType.BasicType.INT.getType()));
	
		ValidatorGlobalFunctionPrototypes validatorToBeTested = new ValidatorGlobalFunctionPrototypes();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 4", result.size() == 4);
	}

	/**
	 * 
	 */
	@Test
	public void testGlobalFunctionPrototypesCannotBeRegister() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();

		CHeaderFile cHeaderFile = new CHeaderFile("examplefileheader");
		sourceFiles.add(cHeaderFile);
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyone", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummytwo", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummythree", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyfour", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyfive", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.REGISTER));
	
		ValidatorGlobalFunctionPrototypes validatorToBeTested = new ValidatorGlobalFunctionPrototypes();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 5", result.size() == 5);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void testGlobalFunctionPrototypesCannotBeAuto() {
		Collection<Object> sourceFiles = new LinkedHashSet<>();

		CHeaderFile cHeaderFile = new CHeaderFile("examplefileheader");
		sourceFiles.add(cHeaderFile);
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyone", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummytwo", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummythree", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyfour", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
		cHeaderFile.addGlobalFunctionPrototypes(new CFunction("dummyfive", cHeaderFile,CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.AUTO));
	
		ValidatorGlobalFunctionPrototypes validatorToBeTested = new ValidatorGlobalFunctionPrototypes();
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		result = (LinkedHashSet<Message>) validatorToBeTested.validate(sourceFiles);

		Assert.assertTrue("collection of model elements is not of size 5", result.size() == 5);
	}

}
