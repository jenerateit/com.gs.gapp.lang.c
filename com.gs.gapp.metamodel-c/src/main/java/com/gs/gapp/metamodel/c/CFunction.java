package com.gs.gapp.metamodel.c;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum;

/**
 * 
 * Metamodel of a CFunction
 * 
 * @author dsn
 *
 */
public class CFunction extends AbstractCType<ModelElementI> {  //FunctionType is also a CType

	private static final long serialVersionUID = 7825851629578681939L;

	/**
	 * return type of the function
	 */
	private final AbstractCType<ModelElementI> returnType;

	/**
	 * storage specifier {none, auto, register, static, extern}
	 */
	private StorageClassSpecifiersEnum storageSpecifier = StorageClassSpecifiersEnum.NONE;

	/**
	 * type qualifier {none, const, volatile}
	 */
	private TypeQualifiersEnum typeQualifiersEnum = TypeQualifiersEnum.NONE;

	/**
	 * set of input parameters
	 */
	private Set<CFunctionInputParameter> inputParameters = new LinkedHashSet<>();

	/**
	 * function body
	 */
	private List<String> functionBody = new ArrayList<>(); //string for now

	/**
	 * local variables
	 */
	private Set<CModuleVariable> localVariables = new LinkedHashSet<>();
	
	/**
	 * variables that owned by the cSourcefile which contains this function
	 */
	private Set<CVariable> usedVariables = new LinkedHashSet<>();

	/**
	 * return variable
	 */
	private CModuleVariable returnVariable;

	/**
	 * default function body
	 */
	private final List<String> defaultFunctionBody = new ArrayList<>();


	/**
	 * @param name
	 * @param owner
	 * @param returnType
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 */
	public CFunction(String name, ModelElementI owner, AbstractCType<ModelElementI> returnType,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner);
		this.returnType = returnType;
		this.storageSpecifier = storageSpecifier;
		this.typeQualifiersEnum = typeQualifiersEnum;
	}

	/**
	 * @param name
	 * @param owner
	 * @param returnType
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 * @param inputParameters
	 * @param functionBody
	 * @param localVariables
	 */
	public CFunction(String name, ModelElementI owner, AbstractCType<ModelElementI> returnType,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum,
			Set<CFunctionInputParameter> inputParameters, List<String> functionBody,
			Set<CModuleVariable> localVariables) {
		super(name, owner);
		this.returnType = returnType;
		this.storageSpecifier = storageSpecifier;
		this.typeQualifiersEnum = typeQualifiersEnum;
		this.inputParameters = inputParameters;
		this.functionBody = functionBody;
		this.localVariables = localVariables;
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param returnType
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 * @param inputParameters
	 * @param functionBody
	 */
	public CFunction(String name, ModelElementI owner, AbstractCType<ModelElementI> returnType,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum,
			Set<CFunctionInputParameter> inputParameters, List<String> functionBody) {
		super(name, owner);
		this.returnType = returnType;
		this.storageSpecifier = storageSpecifier;
		this.typeQualifiersEnum = typeQualifiersEnum;
		this.inputParameters = inputParameters;
		this.functionBody = functionBody;
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param returnType
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 * @param inputParameters
	 */
	public CFunction(String name, ModelElementI owner, AbstractCType<ModelElementI> returnType,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum,
			Set<CFunctionInputParameter> inputParameters) {
		super(name, owner);
		this.returnType = returnType;
		this.storageSpecifier = storageSpecifier;
		this.typeQualifiersEnum = typeQualifiersEnum;
		this.inputParameters = inputParameters;
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param returnType
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 * @param functionBody
	 */
	public CFunction(String name, ModelElementI owner, AbstractCType<ModelElementI> returnType,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum,
			List<String> functionBody) {
		super(name, owner);
		this.returnType = returnType;
		this.storageSpecifier = storageSpecifier;
		this.typeQualifiersEnum = typeQualifiersEnum;
		this.functionBody = functionBody;
	}

	/**
	 * 
	 * @param name
	 * @param returnType
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 * @param functionBody
	 */
	public CFunction(String name, AbstractCType<ModelElementI> returnType, StorageClassSpecifiersEnum storageSpecifier,
			TypeQualifiersEnum typeQualifiersEnum, List<String> functionBody) {
		super(name);
		this.returnType = returnType;
		this.storageSpecifier = storageSpecifier;
		this.typeQualifiersEnum = typeQualifiersEnum;
		this.functionBody = functionBody;
	}

	/**
	 * 
	 * @param name
	 * @param returnType
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 * @param inputParameters
	 * @param functionBody
	 */
	public CFunction(String name, AbstractCType<ModelElementI> returnType, StorageClassSpecifiersEnum storageSpecifier,
			TypeQualifiersEnum typeQualifiersEnum, Set<CFunctionInputParameter> inputParameters, List<String> functionBody) {
		super(name);
		this.returnType = returnType;
		this.storageSpecifier = storageSpecifier;
		this.typeQualifiersEnum = typeQualifiersEnum;
		this.inputParameters = inputParameters;
		this.functionBody = functionBody;
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param returnType
	 * @param storageSpecifier
	 */
	public CFunction(String name, ModelElementI owner, AbstractCType<ModelElementI> returnType, StorageClassSpecifiersEnum storageSpecifier) {
		super(name, owner);
		this.returnType = returnType;
		this.storageSpecifier = storageSpecifier;
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param returnType
	 */
	public CFunction(String name, ModelElementI owner, AbstractCType<ModelElementI> returnType) {
		super(name, owner);
		this.returnType = returnType;
	}

	/**
	 * 
	 * @param name
	 * @param returnType
	 */
	public CFunction(String name, AbstractCType<ModelElementI> returnType) {
		super(name);
		this.returnType = returnType;
	}


	/**
	 * storage specifier {none, auto, register, static, extern}
	 * 
	 * @return storageSpecifier
	 */
	public StorageClassSpecifiersEnum getStorageSpecifier() {
		return storageSpecifier;
	}

	/**
	 * 
	 * @param storageSpecifier
	 */
	public void setStorageSpecifier(StorageClassSpecifiersEnum storageSpecifier) {
		this.storageSpecifier = storageSpecifier;
	}

	/**
	 * get all input parameters. An InputParameter is also a variable.
	 * 
	 * @return inputParameters
	 */
	public Set<CFunctionInputParameter> getInputParameters() {
		return inputParameters;
	}

	/**
	 * 
	 * @param inputParameters
	 */
	public void setInputParameters(Set<CFunctionInputParameter> inputParameters) {
		this.inputParameters = inputParameters;
	}

	/**
	 * 
	 * @param inputParameter
	 */
	public void addInputParameter(CFunctionInputParameter inputParameter) {
		this.inputParameters.add(inputParameter);
	}

	/**
	 * get the return type
	 * 
	 * @return returnType
	 */
	public AbstractCType<ModelElementI> getReturnType() {
		return returnType;
	}

	/**
	 * type qualifier {none, const, volatile}
	 * 
	 * @return typeQualifiersEnum
	 */
	public TypeQualifiersEnum getTypeQualifier() {
		return typeQualifiersEnum;
	}

	/**
	 * @param typeQualifiersEnum the typeQualifiersEnum to set
	 */
	public void setTypeQualifier(TypeQualifiersEnum typeQualifiersEnum) {
		this.typeQualifiersEnum = typeQualifiersEnum;
	}

	/**
	 * get the body of the function
	 * 
	 * @return the functionBody
	 */
	public List<String> getFunctionBody() {
		return functionBody;
	}

	/**
	 * @param functionBody the functionBody to set
	 */
	public void setFunctionBody(List<String> functionBody) {
		this.functionBody = functionBody;
	}
	
	/**
	 * @param functionBody the functionBody to set
	 */
	public void addFunctionBody(String functionBody) {
		this.functionBody.add(functionBody);
	}

	/**
	 * @return the localVariables
	 */
	public Set<CModuleVariable> getLocalVariables() {
		return localVariables;
	}

	/**
	 * @param localVariables the localVariables to set
	 */
	public void setLocalVariables(Set<CModuleVariable> localVariables) {
		this.localVariables = localVariables;
	}

	public void addLocalVariable(CModuleVariable localVar) {
		this.localVariables.add(localVar);
	}

	/**
	 * get the return variable
	 * 
	 * @return the returnVariable
	 */
	public CModuleVariable getReturnVariable() {
		return returnVariable;
	}

	/**
	 * @param returnVariable the returnVariable to set
	 */
	public void setReturnVariable(CModuleVariable returnVariable) {
		this.returnVariable = returnVariable;
	}

	/**
	 * if the function body is empty then this default function body is written.
	 * if the default function body is also empty then nothing is written
	 * 
	 * @return the defaultFunctionBody
	 */
	public List<String> getDefaultFunctionBody() {
		return defaultFunctionBody;
	}

	/**
	 * @return the usedVariables
	 */
	public Set<CVariable> getUsedVariables() {
		return usedVariables;
	}

	
	/**
	 * @param cVar to add usedVariables
	 */
	public CVariable addUsedVariableAndSetAValue(CVariable cVar, String value) {
		CGlobalVariable tempVar = new CGlobalVariable(cVar.getName(), cVar.getOwner(), value, cVar.getType(), cVar.getStorageSpecifier(), cVar.getTypeQualifier()) ;
		this.usedVariables.add(tempVar);
		return tempVar;
	}

}
