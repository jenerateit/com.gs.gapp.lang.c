package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * 
 * CPointerType is also a CDerivedType. It is used to define pointers.
 * 
 * @author dsn
 *
 */
public class CPointerType extends CDerivedType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1574511906789578526L;

	/**
	 * @param name
	 * @param realType
	 */
	public CPointerType(String name, AbstractCType<ModelElementI> realType) {
		super(name, realType);
	}

	/**
	 * @param name
	 * @param owner
	 * @param realType
	 */
	public CPointerType(String name, ModelElementI owner, AbstractCType<ModelElementI> realType) {
		super(name, owner, realType);
	}


}
