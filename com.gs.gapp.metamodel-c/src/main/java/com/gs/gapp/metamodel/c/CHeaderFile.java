package com.gs.gapp.metamodel.c;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 
 * CHeader files are the files with .h extension. This class inherits from CFile.
 * 
 * @author dsn
 *
 */
public class CHeaderFile extends CFile {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1879774947880225801L;

	/**
	 * global function prototypes used within a header file.
	 */
	private Set<CFunctionPrototype> globalFunctionPrototypes = new LinkedHashSet<>();


	/**
	 * 
	 * @param name
	 */
	public CHeaderFile(String name) {
		super(name);
	}

	/**
	 * get all the global function prototypes of the header
	 * 
	 * @return globalFunctionPrototypes
	 */
	public Set<CFunctionPrototype> getGlobalFunctionPrototypes() {
		return globalFunctionPrototypes;
	}

	/**
	 * 
	 * @param globalFunctionPrototypes
	 */
	public void setGlobalFunctionPrototypes(Set<CFunctionPrototype> globalFunctionPrototypes) {
		this.globalFunctionPrototypes = globalFunctionPrototypes;
	}

	/**
	 * 
	 * @param add globalFunctionPrototype to globalFunctionPrototypes
	 */
	public void addGlobalFunctionPrototypes(CFunctionPrototype globalFunctionPrototype) {
		this.globalFunctionPrototypes.add(globalFunctionPrototype);
	}


	/**
	 * We can add functions to globalFunctionPrototypes
	 * @param function
	 */
	public void addGlobalFunctionPrototypes(CFunction function) {
		CFunctionPrototype cFunctionPrototype = new CFunctionPrototype(function);
		if(function.getBody() != null) {
			cFunctionPrototype.setBody(function.getBody());
		}
		addGlobalFunctionPrototypes(cFunctionPrototype);
	}

}
