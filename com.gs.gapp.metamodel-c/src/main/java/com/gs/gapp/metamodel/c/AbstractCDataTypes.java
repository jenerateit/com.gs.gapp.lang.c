/**
 * 
 */
package com.gs.gapp.metamodel.c;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * This class is the parent class of CDataTypes contains the information of 
 * identifierName and dataTypeVariables
 * 
 * @author dsn
 *
 */
public abstract class AbstractCDataTypes extends AbstractCType<ModelElementI> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1295360308935409287L;

	/**
	 * IdentifierName corresponds the name of the data type
	 * e.g consider "struct myStruct {...}" identifierName is myStruct
	 */
	protected String identifierName;

	/**
	 * DataTypeVariables correspond to the variables of the data types
	 * e.g 
	 * struct AStruct {
	 *     char a;
	 *     long double b;
	 * } structVar, AnotherStructVar;
	 * structVar and AnotherStructVar are dataTypeVariables
	 */
	protected Set<String> dataTypeVariables = new LinkedHashSet<>();


	/**
	 * @param name
	 * @param identifierName
	 * 
	 */
	public AbstractCDataTypes(String name, String identifierName) {
		super(name);
		this.identifierName = identifierName;
	}

	/**
	 * @param name
	 * @param identifierName
	 * @param dataTypeVariables
	 */
	public AbstractCDataTypes(String name, String identifierName, Set<String> dataTypeVariables) {
		super(name);
		this.identifierName = identifierName;
		this.dataTypeVariables = dataTypeVariables;
	}


	/**
	 * 
	 * @param name
	 * @param identifierName
	 * @param owner
	 */
	public AbstractCDataTypes(String name, String identifierName, ModelElementI owner) {
		super(name, owner);
		this.identifierName = identifierName;
	}


	/**
	 * 
	 * @param name
	 * @param identifierName
	 * @param owner
	 * @param dataTypeVariables
	 */
	public AbstractCDataTypes(String name, String identifierName, ModelElementI owner, Set<String> dataTypeVariables) {
		super(name, owner);
		this.identifierName = identifierName;
		this.dataTypeVariables = dataTypeVariables;
	}



	/**
	 * @param name
	 * @param owner
	 */
	public AbstractCDataTypes(String name, ModelElementI owner) {
		super(name, owner);
	}

	/**
	 * @param name
	 */
	public AbstractCDataTypes(String name) {
		super(name);
	}

	/**
	 * DataTypeVariables correspond to the variables of the data types
	 * e.g 
	 * struct AStruct {
	 *     char a;
	 *     long double b;
	 * } structVar, AnotherStructVar;
	 * structVar and AnotherStructVar are dataTypeVariables
	 * 
	 * @return the structureVariables
	 */
	public Set<String> getDataTypeVariables() {
		return dataTypeVariables;
	}

	/**
	 * @param structureVariables the structureVariables to set
	 */
	public void setDataTypeVariables(Set<String> dataTypeVariables) {
		this.dataTypeVariables = dataTypeVariables;
	}


	/**
	 * @param add structureVar to structureVariables
	 */
	public void addDataTypeVariable (String dataTypeVar) {
		this.dataTypeVariables.add(dataTypeVar);
	}

	/**
	 * IdentifierName corresponds the name of the data type
	 * e.g consider "struct myStruct {...}" identifierName is myStruct
	 * 
	 * @return the identifierName
	 */
	public String getIdentifierName() {
		return identifierName;
	}

	/**
	 * @param identifierName the identifierName to set
	 */
	public void setIdentifierName(String identifierName) {
		this.identifierName = identifierName;
	}


}
