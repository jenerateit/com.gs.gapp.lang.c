package com.gs.gapp.metamodel.c.enums;

import java.util.HashMap;
import java.util.Map;

public enum TypeQualifiersEnum {
	
	NONE (""),
	CONST ("const"),
	VOLATILE ("volatile"),
	;

	private static Map<String, TypeQualifiersEnum> nameToEnumMap = new HashMap<>();

	static {
		for (TypeQualifiersEnum e : values()) {
			nameToEnumMap.put(e.getName(), e);
		}
	}

	private final String name;

	/**
	 * @param name
	 */
	private TypeQualifiersEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static TypeQualifiersEnum getFromName(String name) {
		return nameToEnumMap.get(name);
	}
}
