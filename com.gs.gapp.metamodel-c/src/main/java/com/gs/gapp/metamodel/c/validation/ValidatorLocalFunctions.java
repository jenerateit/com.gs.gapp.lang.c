/**
 * 
 */
package com.gs.gapp.metamodel.c.validation;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.c.CFunction;
import com.gs.gapp.metamodel.c.CFunctionPrototype;
import com.gs.gapp.metamodel.c.CSourceFile;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;

/**
 * @author dsn
 *
 */
public class ValidatorLocalFunctions implements ModelValidatorI {

	/**
	 * 
	 */
	public ValidatorLocalFunctions() {	}

	/**
	 * 
	 */
	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(localFunctionsAreStatic(modelElements));
		result.addAll(localFunctionPrototypesWereAlsoCreated(modelElements));
		result.addAll(localFunctionsHaveUniqueNames(modelElements));

		return result;
	}


	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> localFunctionsAreStatic(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CSourceFile) {
				CSourceFile cSourceFile =  (CSourceFile) element;
				Set<CFunction> allLocalVariables = cSourceFile.getLocalFunctions();
				for(CFunction cLocalFunctions : allLocalVariables) {
					if (cLocalFunctions.getStorageSpecifier() != StorageClassSpecifiersEnum.STATIC) {
						StringBuilder sb = new StringBuilder("Function: ");
						sb.append(cLocalFunctions.getName()).append(" in ").append(((CSourceFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is located in "
								+ "local functions section but its storage specifiers is not static.");
						result.add(message);
					}
				}
			}
		}

		return result;
	}


	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> localFunctionPrototypesWereAlsoCreated(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CSourceFile) {
				CSourceFile cSourceFile =  (CSourceFile) element;
				Set<CFunction> allLocalFunctions = cSourceFile.getLocalFunctions();
				Set<CFunctionPrototype> allLocalFunctionPrototypes = cSourceFile.getLocalFunctionPrototypes();
				Set<String> allLocalFunctionPrototypesNames = new LinkedHashSet<>();
				for(CFunctionPrototype cFuncProto : allLocalFunctionPrototypes) {
					allLocalFunctionPrototypesNames.add(cFuncProto.getName());
				}
				for(CFunction function : allLocalFunctions) {
					if(!allLocalFunctionPrototypesNames.contains(function.getName())) {
						StringBuilder sb = new StringBuilder("Function: ");
						sb.append(function.getName()).append(" in ").append(((CSourceFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is located in local functionc but prototype doesn't exist");
						result.add(message);
					}
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> localFunctionsHaveUniqueNames(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CSourceFile) {
				CSourceFile cSourceFile =  (CSourceFile) element;
				Set<CFunction> allLocalFunctions = cSourceFile.getLocalFunctions();
				Set<String> allLocalFunctionNames = new LinkedHashSet<>();
				for(CFunction localVar : allLocalFunctions) {
					if (!allLocalFunctionNames.add(localVar.getName())) {
						StringBuilder sb = new StringBuilder("Function: ");
						sb.append(localVar.getName()).append(" in ").append(((CSourceFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is defined twice."
								+ " Each Local Function must have a unique name");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}

}
