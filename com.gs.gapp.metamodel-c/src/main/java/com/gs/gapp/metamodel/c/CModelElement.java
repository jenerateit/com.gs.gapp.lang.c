package com.gs.gapp.metamodel.c;

import java.util.Objects;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * @author dsn
 *
 */
public class CModelElement <T extends ModelElementI> extends ModelElement implements CTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 362471629929948734L;
	
	private T owner;

	
	public CModelElement(String name, T owner) {
		super(name);
		if (owner != null) {
			setOwner(owner);
		}
	}
	
	public CModelElement(String name) {
		super(name);
	}
	
	public void setOwner(T owner) {
		if (owner == null) {
			throw new NullPointerException("The owner may not be null");
		} else if (this.owner == owner) {
			// reset
		} else if (this.owner == null) {
			this.owner = owner;
		} else {
			throw new RuntimeException("The owner is already set to '" + this.owner + "'");
		}
	}
	
	public T getOwner() {
		return owner;
	}
	
	@Override
	public int hashCode() {
		//TODO remove
		return Objects.hash(super.hashCode(), owner);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		return false;
	}
	
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);
		if (comp == 0) {
			if (CModelElement.class.isInstance(element)) {
				CModelElement<?> c = CModelElement.class.cast(element);
				
				if (comp == 0) {
					if (owner == null) {
						if (c.owner != null) {
							comp = 1;
						}
					} else {
						comp = owner.compareTo(c.owner);
					}
				}
			}
		}
		return comp;
	}

}
