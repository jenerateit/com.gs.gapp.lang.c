package com.gs.gapp.metamodel.c;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * CArrayTypes are CDerivedTypes 
 * An array can have minimum one dimension
 * It is mandatory for multidimensional arrays to have dimension sizes.
 * If the size of dimensionsize set is larger than dimension, then dimension = max(dimensionSizes.size, dimension)
 * 
 * @author dsn
 *
 */
public class CArrayType extends CDerivedType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5733376240857802447L;

	/**
	 * dimension is one by default but increases at dimensionsize set increase
	 */
	private Integer dimensions = 1;

	/**
	 * A set holds the size of the dimensions in a multi dimensional array.
	 */
	private Set<Integer> dimensionSizes = new LinkedHashSet<>();



	/**
	 * 
	 * @param name
	 * @param dimensions
	 * @param owner
	 * @param realType
	 */
	public CArrayType(String name, int dimensions, ModelElementI owner,  AbstractCType<ModelElementI> realType) {
		super(name,owner,realType);
		this.dimensions = dimensions;
	}


	/**
	 * @param name
	 * @param dimensions
	 * @param owner
	 * @param type
	 */
	public CArrayType(String name, Set<Integer> dimensionSizes, ModelElementI owner, AbstractCType<ModelElementI> realType) {
		super(name,owner,realType);
		this.dimensionSizes = dimensionSizes;
	}

	/**
	 * @param name
	 * @param owner
	 * @param realType
	 */
	public CArrayType(String name, ModelElementI owner, AbstractCType<ModelElementI> realType) {
		super(name,owner,realType);
	}


	/**
	 * dimension is one by default but increases at dimensionsize set increase
	 * 
	 * @return dimensions
	 */
	public int getDimensions() {
		return dimensions;
	}



	/**
	 * A set holds the size of the dimensions in a multi dimensional array.
	 * 
	 * @return the dimensionSizes
	 */
	public Set<Integer> getDimensionSizes() {
		return dimensionSizes;
	}


	/**
	 * @param dimensionSizes the dimensionSizes to set
	 */
	public void setDimensionSizes(Set<Integer> dimensionSizes) {
		this.dimensionSizes = dimensionSizes;
	}

	/**
	 * @param add dimensionSize to dimensionSizes set
	 */
	public void addDimensionSize(int dimensionSize) {
		this.dimensionSizes.add(dimensionSize);
		this.dimensions = Math.max(this.dimensionSizes.size(), this.dimensions);
	}




}
