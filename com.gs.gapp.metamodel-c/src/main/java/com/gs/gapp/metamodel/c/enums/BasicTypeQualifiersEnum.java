package com.gs.gapp.metamodel.c.enums;

import java.util.HashMap;
import java.util.Map;


//used for self aware tests
public enum BasicTypeQualifiersEnum {
	CHAR ("char"),
	SIGNED_CHAR ("signed char"),
	UNSIGEND_CHAR ("unsigned char"),
	SHORT ("short"),
	SHORT_INT ("short int"),
	SIGNED_SHORT ("signed short"),
	SIGNED_SHORT_INT ("signed short int"),
	UNSIGNED_SHORT ("unsigned short"),
	INT ("int"),
	SIGNED ("signed"),
	SIGNED_INT ("signed int"),
	UNSIGNED ("unsigned"),
	UNSIGNED_INT ("unsigned int"),
	LONG ("long"),
	LONG_INT ("long int"),
	SIGNED_LONG ("signed_long"),
	SIGNED_LONG_INT ("signed long int"),
	UNSIGNED_LONG ("unsigned long"),
	UNSIGNED_LONG_INT ("unsigned long int"),
	LONG_LONG ("long long"),
	LONG_LONG_INT ("long long int"),
	SIGNED_LONG_LONG ("signed long long"),
	SIGNED_LONG_LONG_INT ("signed long long int"),
	UNSIGNED_LONG_LONG ("unsigned long long"),
	UNSIGNED_LONG_LONG_INT ("unsigned long long int"),
	FLOAT ("float"),
	DOUBLE ("double"),
	LONG_DOUBLE ("long double"),
	;

	private static Map<String, BasicTypeQualifiersEnum> nameToEnumMap = new HashMap<>();

	static {
		for (BasicTypeQualifiersEnum e : values()) {
			nameToEnumMap.put(e.getName(), e);
		}
	}

	private final String name;

	/**
	 * @param name
	 */
	private BasicTypeQualifiersEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static BasicTypeQualifiersEnum getFromName(String name) {
		return nameToEnumMap.get(name);
	}
}
