/**
 * 
 */
package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * This class is the parent of CVariable and CDataMembers
 * thus it allows us to cast.
 * 
 * @author dsn
 *
 */
public abstract class AbstractCVariable extends CModelElement<ModelElementI> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1903383323853728913L;

	/**
	 * @param name
	 * @param owner
	 */
	public AbstractCVariable(String name, ModelElementI owner) {
		super(name, owner);
	}

	/**
	 * @param name
	 */
	public AbstractCVariable(String name) {
		super(name);
	}

}
