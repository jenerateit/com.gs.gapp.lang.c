package com.gs.gapp.metamodel.c;

import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * CStructType structure as follows
 * e.g 
 * struct identifiername {
 * 	   data_type member1;
 *     data_type member2;
 *     ...
 * } structure_variable1, structure_variable2, structure_variable[3] ;
 *
 * @author dsn
 *
 */
public class CStructType extends CDataTypes {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9002859206512702785L;



	/**
	 * @param identifierName
	 * @param owner
	 * @param memberDefinitions
	 * @param dataTypeVariables
	 * 
	 */
	public CStructType(String identifierName, ModelElementI owner, Set<CDataMember> memberDefinitions, Set<String> dataTypeVariables) {
		super("struct", identifierName, owner, memberDefinitions, dataTypeVariables);
	}

	/**
	 * 
	 * @param identifierName
	 * @param owner
	 */
	public CStructType(String identifierName, ModelElementI owner) {
		super("struct", identifierName, owner);
	}

	/**
	 *
	 * @param identifierName
	 */
	public CStructType(String identifierName) {
		super("struct", identifierName);
	}
	
	/**
	 *
	 * 
	 */
	public CStructType() {
		super("struct");
	}

	
}
