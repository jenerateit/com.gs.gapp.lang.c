package com.gs.gapp.metamodel.c;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * 
 * CFile is the structure of a CFile with sections, those sections are the ones shared by 
 * the CSourceFile and the CHeader file.
 * 
 * @author dsn
 *
 */
public class CFile extends CModelElement<ModelElementI> {

	private static final long serialVersionUID = -5157707822050001053L;

	/**
	 *  this section is not decided yet
	 */
	private List<String> commentHeader = new LinkedList<>(); //it is not decided to have comment template for now so that it is just a string

	/**
	 * include headers which are not system headers
	 */
	private Set<String> includes = new LinkedHashSet<>(); //For now inlcudes are just strings. maybe we will need to model something more advanced in the future

	/**
	 * include system headers
	 */
	private Set<String> includesSystemHeaders = new LinkedHashSet<>(); //For now inlcudes are just strings. maybe we will need to model something more advanced in the future

	/**
	 * object like macros
	 */
	private Set<CObjectLikeMacro> objectLikeMacros = new LinkedHashSet<>(); 

	/**
	 * function like macros
	 * maybe there's a better way to merge macros into one class (not urgent)
	 */
	private Set<CFunctionLikeMacro> functionLikeMacros = new LinkedHashSet<>();  

	/**
	 * global variables
	 */
	private Set<CVariable> globalVariables = new LinkedHashSet<>();

	/**
	 * global data type declarations
	 */
	private Set<AbstractCDataTypes> globalDataTypeDeclarations = new LinkedHashSet<>(); // could be stuct union or enum

	/**
	 * typedefs
	 */
	private Set<CTypedef> typeDefs = new LinkedHashSet<>();

	/**
	 * object like macros
	 * e.g #define ABC 3
	 * 
	 * @return the objectLikeMacros
	 */
	public Set<CObjectLikeMacro> getObjectLikeMacros() {
		return objectLikeMacros;
	}


	/**
	 * @param objectLikeMacros the objectLikeMacros to set
	 */
	public void setObjectLikeMacros(Set<CObjectLikeMacro> objectLikeMacros) {
		this.objectLikeMacros = objectLikeMacros;
	}

	/**
	 * 
	 * @param objectLikeMacro
	 */
	public void addObjectLikeMacro(CObjectLikeMacro objectLikeMacro) {
		this.objectLikeMacros.add(objectLikeMacro);
	}

	/**
	 * function like macros
	 * e.g #define min(X, Y)  ((X) < (Y) ? (X) : (Y))
	 * 
	 * @return functionLikeMacros
	 */
	public Set<CFunctionLikeMacro> getFunctionLikeMacros() {
		return functionLikeMacros;
	}

	/**
	 * 
	 * @param functionLikeMacros
	 */
	public void setFunctionLikeMacros(Set<CFunctionLikeMacro> functionLikeMacros) {
		this.functionLikeMacros = functionLikeMacros;
	}

	/**
	 * 
	 * @param functionLikeMacro
	 */
	public void addFunctionLikeMacro(CFunctionLikeMacro functionLikeMacro) {
		this.functionLikeMacros.add(functionLikeMacro);
	}

	/**
	 * 
	 * @param name
	 */
	public CFile(String name) {
		super(name);
	}

	/**
	 * include headers which are not system headers
	 * 
	 * @return includes
	 */
	public Set<String> getIncludes() {
		return includes;
	}

	/**
	 * 
	 * @param includes
	 */
	public void setIncludes(Set<String> includes) {
		this.includes = includes;
	}

	/**
	 * 
	 * @param include
	 */
	public void addInclude(String include) {
		this.includes.add(include);		
	}

	/**
	 * 
	 * @param include
	 */
	public void addInclude(CFile include) {
		String nameOfTheInclude = include.getName();
		if(include instanceof CSourceFile) {
			if (!nameOfTheInclude.endsWith(".c")) {
				nameOfTheInclude = nameOfTheInclude + ".c";
			}
		} else if(include instanceof CHeaderFile) {
			if (!nameOfTheInclude.endsWith(".h")) {
				nameOfTheInclude = nameOfTheInclude + ".h";
			}
		}
		this.includes.add(nameOfTheInclude);		

	}


	/**
	 * 
	 * @return allMacros
	 */
	public Set<CMacro> getAllMacros() {
		Set<CMacro> allMacros = new LinkedHashSet<>();
		allMacros.addAll(objectLikeMacros);
		allMacros.addAll(functionLikeMacros);
		return allMacros;
	}

	/**
	 * get all the global variables in the file
	 * 
	 * @return globalVariables
	 */
	public Set<CVariable> getGlobalVariables() {
		return globalVariables;
	}

	/**
	 * 
	 * @param globalVariables
	 */
	public void setGlobalVariables(Set<CVariable> globalVariables) {
		this.globalVariables = globalVariables;
	}

	/**
	 * 
	 * @param globalVar
	 */
	public void addGlobalVariable(CVariable globalVar) {
		this.globalVariables.add(globalVar);
	}

	/**
	 * include system headers
	 * 
	 * @return includesSystemHeaders
	 */
	public Set<String> getIncludesSystemHeaders() {
		return includesSystemHeaders;
	}


	/**
	 * 
	 * @param includesSystemHeaders
	 */
	public void setIncludesSystemHeaders(Set<String> includesSystemHeaders) {
		this.includesSystemHeaders = includesSystemHeaders;
	}

	/**
	 * 
	 * @param include
	 */
	public void addIncludesSystemHeader(String include) {
		this.includesSystemHeaders.add(include);
	}


	/**
	 * get user defined data types such as struct unions and enums
	 * 
	 * @return the globalCompositeDataTypeDeclarations
	 */
	public Set<AbstractCDataTypes> getGlobalDataTypeDeclarations() {
		return globalDataTypeDeclarations;
	}


	/**
	 * @param globalCompositeDataTypeDeclarations the globalCompositeDataTypeDeclarations to set
	 */
	public void setGlobalDataTypeDeclarations(Set<AbstractCDataTypes> globalCompositeDataTypeDeclarations) {
		this.globalDataTypeDeclarations = globalCompositeDataTypeDeclarations;
	}

	/**
	 * 
	 * @param globalCompositeDataTypeDeclaration to add
	 */
	public void addGlobalDataTypeDeclaration(AbstractCDataTypes globalCompositeDataTypeDeclaration) {
		this.globalDataTypeDeclarations.add(globalCompositeDataTypeDeclaration);
	}


	/**
	 * get all typedefs
	 * 
	 * @return the typeDefs
	 */
	public Set<CTypedef> getTypeDefs() {
		return typeDefs;
	}


	/**
	 * @param typeDefs the typeDefs to set
	 */
	public void setTypeDefs(Set<CTypedef> typeDefs) {
		this.typeDefs = typeDefs;
	}

	/**
	 * @param typeDef the typeDefs to add
	 */
	public void addTypeDefs(CTypedef typeDef) {
		this.typeDefs.add(typeDef);
	}


	/**
	 * @return the commentHeader
	 */
	public List<String> getCommentHeader() {
		return commentHeader;
	}


	/**
	 * @param commentHeader the commentHeader to set
	 */
	public void setCommentHeader(List<String> commentHeader) {
		this.commentHeader = commentHeader;
	}
}
