package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * Derived types are arrays and pointers
 * (CArrayType and CPointerType)
 * 
 * @author dsn
 *
 */
public abstract class CDerivedType extends AbstractCType<ModelElementI>  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6001623869806209804L;

	/**
	 * realType is the actual type of the derived type. 
	 * the actual type of a pointer is type of the element pointer points to.
	 * the actual type of an array is the type of the element which array holds.
	 */
	private final AbstractCType<ModelElementI> realType;

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param realType
	 */
	public CDerivedType(String name, ModelElementI owner, AbstractCType<ModelElementI> realType) {
		super(name, owner);
		this.realType = realType;
	}

	/**
	 * 
	 * @param name
	 * @param realType
	 */
	public CDerivedType(String name, AbstractCType<ModelElementI> realType) {
		super(name);
		this.realType = realType;
	}
	
	/**
	 * realType is the actual type of the derived type. 
	 * the actual type of a pointer is type of the element pointer points to.
	 * the actual type of an array is the type of the element which array holds.
	 * 
	 * @return realType
	 */
	public AbstractCType<ModelElementI> getRealType() {
		return realType;
	}



}
