/**
 * 
 */
package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * @author dsn
 *
 * CDataMember is used to define members of user defined data types
 * such as unions and structs.
 * 
 */
public class CDataMember extends AbstractCVariable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5231711937461895579L;

	/**
	 * type of the member
	 */
	private CTypeI type;

	/**
	 * @param name
	 * @param owner
	 * @param type
	 */
	public CDataMember(String name, ModelElementI owner, AbstractCType<ModelElementI> type) {
		super(name, owner);
		this.setType(type);
	}



	/**
	 * 
	 * @param name
	 * @param owner
	 * @param dataType
	 */
	public CDataMember(String name, ModelElementI owner, AbstractCDataTypes dataType) {
		super(dataType.getIdentifierName() + " " + name, owner);
		this.setType(dataType);
	}
	
	/**
	 * 
	 * @param name
	 * @param owner
	 * @param dataType
	 */
	public CDataMember(String name, ModelElementI owner, CTypedef dataType) {
		super(dataType.getNewIdentifierName() + " " + name, owner);
		this.setType(dataType);
	}




	/**
	 * type of the member
	 * 
	 * @return type
	 */
	public CTypeI getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(CTypeI type) {
		this.type = type;
	}

}
