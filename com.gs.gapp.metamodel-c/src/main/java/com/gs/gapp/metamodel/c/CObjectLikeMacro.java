package com.gs.gapp.metamodel.c;

/**
 * 
 * Macros that are like objects.
 * e.g
 * #define BUFFER_SIZE 1024
 * 
 * @author dsn
 *
 */
public class CObjectLikeMacro extends CMacro {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6896097683626508175L;
	

	/**
	 * @param name
	 * @param owner
	 * @param replacementTokens
	 */
	public CObjectLikeMacro(String name, CFile owner, String... replacementTokens) {
		super(name, owner, replacementTokens);
		owner.addObjectLikeMacro(this);
	}
	
	/**
	 * @param name
	 * @param owner
	 */
	public CObjectLikeMacro(String name, CFile owner) {
		super(name, owner);
		owner.addObjectLikeMacro(this);
	}

}
