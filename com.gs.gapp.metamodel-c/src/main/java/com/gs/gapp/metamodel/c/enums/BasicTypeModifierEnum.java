package com.gs.gapp.metamodel.c.enums;

import java.util.HashMap;
import java.util.Map;
//not necessary for now
public enum BasicTypeModifierEnum {
	NONE (""),
	SIGNED ("signed"),
	UNSIGNED ("unsigned"),
	SHORT ("short"),
	LONG ("long"),
	;
	
	private static Map<String, BasicTypeModifierEnum> nameToEnumMap = new HashMap<>();

	static {
		for (BasicTypeModifierEnum e : values()) {
			nameToEnumMap.put(e.getName(), e);
		}
	}

	private final String name;

	/**
	 * @param name
	 */
	private BasicTypeModifierEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static BasicTypeModifierEnum getFromName(String name) {
		return nameToEnumMap.get(name);
	}
}
