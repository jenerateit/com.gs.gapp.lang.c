/**
 * 
 */
package com.gs.gapp.metamodel.c.validation;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.c.CFunctionPrototype;
import com.gs.gapp.metamodel.c.CSourceFile;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;

/**
 * @author dsn
 *
 */
public class ValidatorLocalFunctionPrototypes implements ModelValidatorI {

	/**
	 * 
	 */
	public ValidatorLocalFunctionPrototypes() {	}

	/**
	 * 
	 */
	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(localFunctionPrototypesAreStatic(modelElements));
		result.addAll(localFunctionPrototypesHaveUniqueNames(modelElements));

		return result;
	}


	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> localFunctionPrototypesAreStatic(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CSourceFile) {
				CSourceFile cSourceFile =  (CSourceFile) element;
				Set<CFunctionPrototype> allLocalVariables = cSourceFile.getLocalFunctionPrototypes();
				for(CFunctionPrototype cLocalFunctions : allLocalVariables) {
					if (cLocalFunctions.getAssociatedFunction().getStorageSpecifier() != StorageClassSpecifiersEnum.STATIC) {
						StringBuilder sb = new StringBuilder("Function Prototype: ");
						sb.append(cLocalFunctions.getName()).append(" in ").append(((CSourceFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is located in "
								+ "local functions section but its storage specifiers is not static.");
						result.add(message);
					}
				}
			}
		}

		return result;
	}


	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> localFunctionPrototypesHaveUniqueNames(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CSourceFile) {
				CSourceFile cSourceFile =  (CSourceFile) element;
				Set<CFunctionPrototype> allLocalFunctions = cSourceFile.getLocalFunctionPrototypes();
				Set<String> allLocalFunctionNames = new LinkedHashSet<>();
				for(CFunctionPrototype localVar : allLocalFunctions) {
					if (!allLocalFunctionNames.add(localVar.getName())) {
						StringBuilder sb = new StringBuilder("Function Prototype: ");
						sb.append(localVar.getName()).append(" in ").append(((CSourceFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is defined twice."
								+ " Each Local Function Prototype must have a unique name");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}

}
