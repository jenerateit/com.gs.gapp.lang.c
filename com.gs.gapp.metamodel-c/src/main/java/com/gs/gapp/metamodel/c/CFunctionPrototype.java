package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * 
 * CFunctionPrototype is always associated with a function. it is done within the constructor.
 * 
 * @author dsn
 *
 */
public class CFunctionPrototype extends AbstractCType<ModelElementI> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4556485048167043469L;

	/**
	 * the actual function
	 */
	private CFunction associatedFunction;

	/**
	 * 
	 * @param name
	 * @param owner
	 */
	public CFunctionPrototype(String name, ModelElementI owner) {
		super(name, owner);
	}

	/**
	 * 
	 * @param name
	 */
	public CFunctionPrototype(String name) {
		super(name);
	}

	/**
	 *
	 * @param actualFunction
	 */
	public CFunctionPrototype(CFunction actualFunction) {
		super(actualFunction.getName());
		this.setOwner(actualFunction.getOwner());
		this.associatedFunction = actualFunction;
	}

	/**
	 * get the actual function of the prototype
	 * 
	 * @return associatedFunction
	 */
	public CFunction getAssociatedFunction() {
		return associatedFunction;
	}

	/**
	 * 
	 * @param associatedFunction
	 */
	public void setAssociatedFunction(CFunction associatedFunction) {
		this.associatedFunction = associatedFunction;
	}

}
