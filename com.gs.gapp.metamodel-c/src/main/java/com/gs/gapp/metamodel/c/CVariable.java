package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum;

/**
 * 
 * CVariable is the parent class of CLocalVariable, CGlobalVariable and CFunctionInputParameter
 * Value and the type of the variable is held with this class.
 * In addition, storage specifier and the type qualifier of the variable are
 * held with this class.
 * 
 * @author dsn
 *
 */
public abstract class CVariable extends AbstractCVariable {

	private static final long serialVersionUID = -3866980145901267181L;

	/**
	 * value of the variable
	 */
	private String value;

	/**
	 * type of the variable
	 */
	private final AbstractCType<ModelElementI> type;

	/**
	 * can be none, auto, register, static or extern
	 */
	StorageClassSpecifiersEnum storageSpecifier = StorageClassSpecifiersEnum.NONE;

	/**
	 * can be none, const or volatile
	 */
	TypeQualifiersEnum typeQualifier = TypeQualifiersEnum.NONE;

	/**
	 * 
	 * @param abstractCDataTypes
	 * @param owner
	 * @param value
	 */
	public CVariable(AbstractCDataTypes abstractCDataTypes, ModelElementI owner, String value) {
		super(abstractCDataTypes.getName(), owner);
		this.value = value;
		this.type = abstractCDataTypes;
	}

	/**
	 * 
	 * @param derivedType
	 * @param owner
	 * @param value
	 */
	public CVariable(CDerivedType derivedType, ModelElementI owner, String value) {
		super(derivedType.getName(), owner);
		this.value = value;
		this.type = derivedType;
	}

	/**
	 * 
	 * @param cTypedef
	 * @param owner
	 * @param value
	 */
	public CVariable(CTypedef cTypedef, ModelElementI owner, String value) {
		super(cTypedef.getNewIdentifierName(), owner);
		this.value = value;
		this.type = cTypedef;
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 */
	public CVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type) {
		super(name, owner);
		this.value = value;
		this.type = type;
	}


	/**
	 * 
	 * @param name
	 * @param owner
	 * @param type
	 */
	public CVariable(String name, ModelElementI owner, AbstractCType<ModelElementI> type) {
		super(name, owner);
		this.type = type;
		this.value = "";
	}




	/**
	 * 
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 */
	public CVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner);
		this.value = value;
		this.type = type;
		this.storageSpecifier = storageSpecifier;
		this.typeQualifier = typeQualifiersEnum;
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param storageSpecifier
	 */
	public CVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type,
			StorageClassSpecifiersEnum storageSpecifier) {
		super(name, owner);
		this.value = value;
		this.type = type;
		this.storageSpecifier = storageSpecifier;
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param typeQualifiersEnum
	 */
	public CVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type, TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner);
		this.value = value;
		this.type = type;
		this.typeQualifier = typeQualifiersEnum;
	}

	/**
	 * 
	 * @return type
	 */
	public AbstractCType<ModelElementI> getType() {
		return type;
	}
	/**
	 * 
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * 
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * 
	 * @return true if it is only a declaration
	 * if value is null then the variable is a 
	 * declaration.
	 */
	public boolean isDeclaration() {
		return value.isEmpty();
	}

	/**
	 * @return the storageSpecifier
	 */
	public StorageClassSpecifiersEnum getStorageSpecifier() {
		return storageSpecifier;
	}

	/**
	 * @param storageSpecifier the storageSpecifier to set
	 */
	public void setStorageSpecifier(StorageClassSpecifiersEnum storageSpecifier) {
		this.storageSpecifier = storageSpecifier;
	}

	/**
	 * @return the typeQualifiersEnum
	 */
	public TypeQualifiersEnum getTypeQualifier() {
		return typeQualifier;
	}

	/**
	 * @param typeQualifiersEnum the typeQualifiersEnum to set
	 */
	public void setTypeQualifier(TypeQualifiersEnum typeQualifiersEnum) {
		this.typeQualifier = typeQualifiersEnum;
	}

	/**
	 * 
	 * @return the name of the type qualifier
	 */
	public String getTypeQualifierAsString() {
		return getTypeQualifier().getName();
	}

	/**
	 * 
	 * @return the name of the storage specifier
	 */
	public String getStorageSpecifierAsString() {
		return getStorageSpecifier().getName();
	}

}
