package com.gs.gapp.metamodel.c;

import java.util.ArrayList;
import java.util.List;


/**
 * Parent class of the CObjectLikeMacro and CFunctionLikeMacro
 * 
 * @author dsn
 *
 */
public abstract class CMacro extends CModelElement<CFile> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1013239349858732460L;

	/**
	 * replacement tokens are the values in macros
	 */
	protected List<String> replacementTokenList = new ArrayList<>();

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param replacementTokens
	 */
	public CMacro(String name, CFile owner ,String... replacementTokens) {
		super(name, owner);
		if(replacementTokens != null) {
			for(String replacementToken : replacementTokens) {
				replacementTokenList.add(replacementToken);
			}
		}
	}

	/**
	 * @param name
	 * @param owner
	 */
	public CMacro(String name, CFile owner) {
		super(name, owner);
	}

	/**
	 * @param name
	 */
	public CMacro(String name) {
		super(name);
	}

	/**
	 * get all the replacement tokens
	 * replacement tokens are the values in a macro definitions
	 * 
	 * @return the replacementTokenList
	 */
	public List<String> getReplacementTokenList() {
		return replacementTokenList;
	}

	/**
	 * @param replacementTokenList the replacementTokenList to set
	 */
	public void setReplacementTokenList(List<String> replacementTokenList) {
		this.replacementTokenList = replacementTokenList;
	}

	/**
	 * @param replacementTokenList the replacementTokenList to add
	 */
	public void addReplacementTokenList(String replacementToken) {
		this.replacementTokenList.add(replacementToken);
	}


}
