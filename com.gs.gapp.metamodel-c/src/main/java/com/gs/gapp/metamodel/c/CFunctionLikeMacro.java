package com.gs.gapp.metamodel.c;

/**
 *  
 * This is not fully implemented yet. We should discuss if it is necessary or not.
 * Still function like macros can be defined with manually written as replacement tokens and parameters.
 * e.g
 * #define x(a,b) x(a+1,b+1) + 4 is a functionlike macro.
 * a and b are parameters. the rest is replacement tokens.
 *  
 * @author dsn
 * 
 */
public class CFunctionLikeMacro extends CMacro {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6896097683626508175L;

	//TODO: for now function like macro support is weak so that we take parameters as a single string.

	/**
	 * parameters of the function which defined as macro
	 */
	private String parameters;
	/**
	 * @param name
	 * @param owner
	 * @param replacementTokens
	 */
	public CFunctionLikeMacro(String name, CFile owner, String parameters, String... replacementTokens) {
		super(name, owner, replacementTokens);
		this.parameters = parameters;
		owner.addFunctionLikeMacro(this);
	}

	/**
	 * @param name
	 * @param owner
	 */
	public CFunctionLikeMacro(String name, CFile owner) {
		super(name, owner);
		owner.addFunctionLikeMacro(this);
	}

	/**
	 * get all parameters (they are strings)
	 * 
	 * @return parameter
	 */
	public String getParameters() {
		return parameters;
	}

	/**
	 * 
	 * @param parameters
	 */
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

}
