package com.gs.gapp.metamodel.c;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * CEnumType is a similar to CDataTypes but doesn't contain the member definitions
 * Instead of member definitions it has CEnumConstants, thus they cannot have a nested structure.
 * 
 * @author dsn
 *
 */
public class CEnumType extends AbstractCDataTypes {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4895267129394209108L;

	/**
	 * constants of the enum
	 */
	private Set<CEnumConstant> constants = new LinkedHashSet<>();


	/**
	 * 
	 */
	public CEnumType() {
		super("enum");
	}

	/**
	 * 
	 * @param owner
	 */
	public CEnumType(ModelElementI owner) {
		super("enum", owner);
	}

	/**
	 * 
	 * @param identifierName
	 * @param dataTypeVariables
	 */
	public CEnumType(String identifierName, Set<String> dataTypeVariables) {
		super("enum", identifierName, dataTypeVariables);
	}

	/**
	 * 
	 * @param identifierName
	 * @param owner
	 */
	public CEnumType(String identifierName, ModelElementI owner) {
		super("enum", identifierName, owner);
	}


	/**
	 * 
	 * @param identifierName
	 * @param owner
	 * @param dataTypeVariables
	 */
	public CEnumType(String identifierName, ModelElementI owner, Set<String> dataTypeVariables) {
		super("enum", identifierName, owner, dataTypeVariables);
	}


	/**
	 * 
	 * @param identifierName
	 * @param owner
	 * @param dataTypeVariables
	 * @param enumConstants
	 */
	public CEnumType(String identifierName, ModelElementI owner, Set<String> dataTypeVariables, Set<CEnumConstant> enumConstants) {
		super("enum", identifierName, owner, dataTypeVariables);
		this.constants = enumConstants;
	}

	/**
	 * 
	 * @param identifierName
	 */
	public CEnumType(String identifierName) {
		super("enum", identifierName);
	}
	/**
	 * constants of the enum
	 * 
	 * @return constants
	 */
	public Set<CEnumConstant> getConstants() {
		return constants;
	}

	/**
	 * 
	 * @param constants
	 */
	public void setConstants(Set<CEnumConstant> constants) {
		this.constants = constants;
	}

	/**
	 * 
	 * @param constant
	 */
	public void addConstant(CEnumConstant constant) {
		this.constants.add(constant);
	}

	/**
	 * 
	 * @param constantString
	 */
	public void addConstantWithoutValue(String constantString) {
		this.constants.add(new CEnumConstant(constantString));
	}

	/**
	 * CEnumConstans are consists of a name and a value.
	 * 
	 * @author dsn
	 *
	 */
	public static class CEnumConstant {

		/**
		 * name of the constant
		 */
		private String name;

		/**
		 * value of the constant
		 * empty by default
		 */
		private String value = "";

		/**
		 * @param name
		 * @param value
		 */
		public CEnumConstant(String name, String value) {
			super();
			this.name = name;
			this.value = value;
		}

		/**
		 * 
		 * @param name
		 */
		public CEnumConstant(String name) {
			super();
			this.name = name;
		}


		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * 
		 * @return true if has a value
		 */
		public boolean hasValue() {
			return !value.equals("");
		}

		/**
		 * @param value the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}

	}


}
