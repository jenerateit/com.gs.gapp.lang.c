/**
 * 
 */
package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * @author dsn
 *
 * This class allows us to use integer types in stdint.h
 */
public final class CIntegerType extends AbstractCType<ModelElementI>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3005102878075751406L;

	/**
	 * 
	 * @param name
	 */
	public CIntegerType(String name) {
		super(name);
	}

	/**
	 * 
	 * @author dsn
	 *
	 */
	public static enum StdIntTypes {
		SIGNED_INT_8(new CIntegerType("int8_t")),
		SIGNED_INT_16(new CIntegerType("int16_t")),
		SIGNED_INT_32(new CIntegerType("int32_t")),
		SIGNED_INT_64(new CIntegerType("int64_t")),
		UNSIGNED_INT_8(new CIntegerType("uint8_t")),
		UNSIGNED_INT_16(new CIntegerType("uint16_t")),
		UNSIGNED_INT_32(new CIntegerType("uint32_t")),
		UNSIGNED_INT_64(new CIntegerType("uint64_t")),
		;

		/**
		 * 
		 */
		private final CIntegerType type;

		/**
		 * 
		 * @param type
		 */
		StdIntTypes(CIntegerType type) {
			this.type = type;
		}

		/**
		 * 
		 * @return type
		 */
		public CIntegerType getType() {
			return type;
		}
	}

}
