package com.gs.gapp.metamodel.c.enums;

import java.util.HashMap;
import java.util.Map;
//not necessary for now
public enum BasicTypeArithmeticEnum {
	NONE (""),
	INT ("int"),
	CHAR ("char"),
	DOUBLE ("double"),
	FLOAT ("float"),
	;
	
	private static Map<String, BasicTypeArithmeticEnum> nameToEnumMap = new HashMap<>();

	static {
		for (BasicTypeArithmeticEnum e : values()) {
			nameToEnumMap.put(e.getName(), e);
		}
	}

	private final String name;

	/**
	 * @param name
	 */
	private BasicTypeArithmeticEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static BasicTypeArithmeticEnum getFromName(String name) {
		return nameToEnumMap.get(name);
	}
}
