package com.gs.gapp.metamodel.c;

import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * CUnionType structure as follows
 * e.g 
 * union identifiername {
 *     data_type member1;
 *     data_type member2;
 *     ...
 * } structure_variable1, structure_variable2, structure_variable[3] ;
 *
 * @author dsn
 *
 */
public class CUnionType extends CDataTypes {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7170419505554298531L;


	/**
	 * @param identifierName
	 * @param owner
	 * @param memberDefinitions
	 * @param dataTypeVariables
	 * 
	 */
	public CUnionType(String identifierName, ModelElementI owner, Set<CDataMember> memberDefinitions, Set<String> dataTypeVariables) {
		super("union", identifierName, owner, memberDefinitions, dataTypeVariables);
	}


	/**
	 * 
	 * @param identifierName
	 * @param owner
	 */
	public CUnionType(String identifierName, ModelElementI owner) {
		super("union", identifierName, owner);
	}

	/**
	 *
	 * @param identifierName
	 */
	public CUnionType(String identifierName) {
		super("union", identifierName);
	}
	
	/**
	 *
	 * 
	 */
	public CUnionType() {
		super("union");
	}


}
