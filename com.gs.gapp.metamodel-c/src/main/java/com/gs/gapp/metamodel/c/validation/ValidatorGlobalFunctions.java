/**
 * 
 */
package com.gs.gapp.metamodel.c.validation;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.c.CFunction;
import com.gs.gapp.metamodel.c.CSourceFile;

/**
 * @author dsn
 *
 */
public class ValidatorGlobalFunctions implements ModelValidatorI {

	/**
	 * 
	 */
	public ValidatorGlobalFunctions() {	}

	/**
	 * 
	 */
	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(globalFunctionsHaveUniqueNames(modelElements));

		return result;
	}


	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> globalFunctionsHaveUniqueNames(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CSourceFile) {
				CSourceFile cSourceFile =  (CSourceFile) element;
				Set<CFunction> allGlobalFunctions = cSourceFile.getGlobalFunctions();
				Set<String> allLocalFunctionNames = new LinkedHashSet<>();
				for(CFunction localVar : allGlobalFunctions) {
					if (!allLocalFunctionNames.add(localVar.getName())) {
						StringBuilder sb = new StringBuilder("Function: ");
						sb.append(localVar.getName()).append(" in ").append(((CSourceFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is defined twice."
								+ " Each Global Function must have a unique name");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}

}
