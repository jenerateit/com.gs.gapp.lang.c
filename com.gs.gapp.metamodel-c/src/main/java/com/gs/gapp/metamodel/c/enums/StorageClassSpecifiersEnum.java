package com.gs.gapp.metamodel.c.enums;

import java.util.HashMap;
import java.util.Map;

public enum StorageClassSpecifiersEnum {

	NONE (""),
	AUTO ("auto"),
	REGISTER ("register"),
	STATIC ("static"),
	EXTERN ("extern"),
	;

	private static Map<String, StorageClassSpecifiersEnum> nameToEnumMap = new HashMap<>();

	static {
		for (StorageClassSpecifiersEnum e : values()) {
			nameToEnumMap.put(e.getName(), e);
		}
	}

	private final String name;

	/**
	 * @param name
	 */
	private StorageClassSpecifiersEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static StorageClassSpecifiersEnum getFromName(String name) {
		return nameToEnumMap.get(name);
	}
}
