/**
 * 
 */
package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum;

/**
 * 
 * CLocalVariable is the variable that is used as a local variable.
 * 
 * @author dsn
 *
 */
public class CModuleVariable extends CVariable {

	/**
	 * @param abstractCDataTypes
	 * @param owner
	 * @param value
	 */
	public CModuleVariable(AbstractCDataTypes abstractCDataTypes, ModelElementI owner, String value) {
		super(abstractCDataTypes, owner, value);
	}

	/**
	 * @param cTypedef
	 * @param owner
	 * @param value
	 */
	public CModuleVariable(CTypedef cTypedef, ModelElementI owner, String value) {
		super(cTypedef, owner, value);
	}

	/**
	 * @param name
	 * @param owner
	 * @param type
	 */
	public CModuleVariable(String name, ModelElementI owner, AbstractCType<ModelElementI> type) {
		super(name, owner, type);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 103689810033744487L;

	/**
	 * @param derivedType
	 * @param owner
	 * @param value
	 */
	public CModuleVariable(CDerivedType derivedType, ModelElementI owner, String value) {
		super(derivedType, owner, value);
	}

	/**
	 * @param derivedType
	 * @param value
	 */
	public CModuleVariable(CDerivedType derivedType, String value) {
		super(derivedType, derivedType.getOwner(), value);
	}

	/**
	 * @param derivedType
	 */
	public CModuleVariable(CDerivedType derivedType) {
		super(derivedType, derivedType.getOwner(), "");
	}
	/**
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 */
	public CModuleVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner, value, type, storageSpecifier, typeQualifiersEnum);
	}

	/**
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param storageSpecifier
	 */
	public CModuleVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type,
			StorageClassSpecifiersEnum storageSpecifier) {
		super(name, owner, value, type, storageSpecifier);
	}

	/**
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param typeQualifiersEnum
	 */
	public CModuleVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type,
			TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner, value, type, typeQualifiersEnum);
	}

	/**
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 */
	public CModuleVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type) {
		super(name, owner, value, type);
	}




}
