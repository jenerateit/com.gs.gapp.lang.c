/**
 *
 */
package com.gs.gapp.metamodel.c;

/**
 * @author dsn
 *	not sure if we need this class
 */
public class CMetaModelUtil {

	private CMetaModelUtil() {}

	/**
	 * @param cFile
	 * @return
	 */
	public static String getFilePath(CFile cFile) {
		return cFile.getName();
	}
}
