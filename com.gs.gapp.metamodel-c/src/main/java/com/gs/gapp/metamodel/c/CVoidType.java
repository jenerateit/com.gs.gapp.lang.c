package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * 
 * CVoid type is simply void
 * 
 * @author dsn
 *
 */
public final class CVoidType extends AbstractCType<ModelElementI> implements CTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1945868760083161515L;



	/**
	 * 
	 * @param owner
	 */
	public CVoidType(ModelElementI owner) {
		super("void", owner);
	}


	/**
	 * 
	 */
	public CVoidType() {
		super("void");
	}




}
