/**
 * 
 */
package com.gs.gapp.metamodel.c.validation;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.c.CSourceFile;
import com.gs.gapp.metamodel.c.CVariable;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;

/**
 * @author dsn
 *
 */
public class ValidatorModuleVariables implements ModelValidatorI {

	/**
	 * 
	 */
	public ValidatorModuleVariables() {	}

	/**
	 * 
	 */
	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(localVariablesAreStatic(modelElements));
		result.addAll(moduleVariablesHaveUniqueNames(modelElements));
		return result;
	}


	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> localVariablesAreStatic(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CSourceFile) {
				CSourceFile cSourceFile =  (CSourceFile) element;
				Set<CVariable> allLocalVariables = cSourceFile.getModuleVariables();
				for(CVariable cLocalVariable : allLocalVariables) {
					if (cLocalVariable.getStorageSpecifier() != StorageClassSpecifiersEnum.STATIC) {
						StringBuilder sb = new StringBuilder("Variable: ");
						sb.append(cLocalVariable.getName()).append(" in ").append(((CSourceFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is located in "
								+ "local variables section but its storage specifier is not static.");
						result.add(message);
					}
				}
			}
		}

		return result;
	}

	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> moduleVariablesHaveUniqueNames(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CSourceFile) {
				CSourceFile cSourceFile =  (CSourceFile) element;
				Set<CVariable> allLocalVariables = cSourceFile.getModuleVariables();
				Set<String> allLocalVariableNames = new LinkedHashSet<>();
				for(CVariable localVar : allLocalVariables) {
					if (!allLocalVariableNames.add(localVar.getName())) {
						StringBuilder sb = new StringBuilder("Variable: ");
						sb.append(localVar.getName()).append(" in ").append(((CSourceFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is defined twice."
								+ " Each Local Variable must have a unique name");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}



}
