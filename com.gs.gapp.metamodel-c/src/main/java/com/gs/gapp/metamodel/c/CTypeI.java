package com.gs.gapp.metamodel.c;

import java.io.Serializable;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementTraceabilityI;

/**
 * @author dsn
 *
 */
public interface CTypeI extends Serializable, ModelElementI, ModelElementTraceabilityI {

	
}
