/**
 * 
 */
package com.gs.gapp.metamodel.c.validation;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.c.CFile;
import com.gs.gapp.metamodel.c.CFunctionPrototype;
import com.gs.gapp.metamodel.c.CHeaderFile;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;

/**
 * @author dsn
 *
 */
public class ValidatorGlobalFunctionPrototypes implements ModelValidatorI {

	/**
	 * 
	 */
	public ValidatorGlobalFunctionPrototypes() {	}

	/**
	 * 
	 */
	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(globalFunctionPrototypesHaveUniqueNames(modelElements));
		result.addAll(globalFunctionPrototypesCannotBeAuto(modelElements));
		result.addAll(globalFunctionPrototypesCannotBeRegister(modelElements));

		return result;
	}



	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> globalFunctionPrototypesHaveUniqueNames(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CHeaderFile) {
				CHeaderFile cHeaderFile =  (CHeaderFile) element;
				Set<CFunctionPrototype> allLocalFunctions = cHeaderFile.getGlobalFunctionPrototypes();
				Set<String> allGlobalFunctionPrototypeNames = new LinkedHashSet<>();
				for(CFunctionPrototype localVar : allLocalFunctions) {
					if (!allGlobalFunctionPrototypeNames.add(localVar.getName())) {
						StringBuilder sb = new StringBuilder("Function Prototype: ");
						sb.append(localVar.getName()).append(" in ").append(((CHeaderFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is defined twice."
								+ " Each Global Function Prototype must have a unique name");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}
	
	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> globalFunctionPrototypesCannotBeAuto(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CHeaderFile) {
				CHeaderFile cHeaderFile =  (CHeaderFile) element;
				Set<CFunctionPrototype> allLocalFunctions = cHeaderFile.getGlobalFunctionPrototypes();
				for(CFunctionPrototype localVar : allLocalFunctions) {
					if (localVar.getAssociatedFunction().getStorageSpecifier() == StorageClassSpecifiersEnum.AUTO) {
						StringBuilder sb = new StringBuilder("Function Prototype: ");
						sb.append(localVar.getName()).append(" in ").append(((CFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") has storage specifier auto"
								+ "but it is not allowed as global function prototype in c. ");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}
	
	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> globalFunctionPrototypesCannotBeRegister(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CHeaderFile) {
				CHeaderFile cHeaderFile =  (CHeaderFile) element;
				Set<CFunctionPrototype> allLocalFunctions = cHeaderFile.getGlobalFunctionPrototypes();
				for(CFunctionPrototype localVar : allLocalFunctions) {
					if (localVar.getAssociatedFunction().getStorageSpecifier() == StorageClassSpecifiersEnum.REGISTER) {
						StringBuilder sb = new StringBuilder("Function Prototype: ");
						sb.append(localVar.getName()).append(" in ").append(((CFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") has storage specifier register"
								+ "but it is not allowed as global function prototype in c. ");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}
	
	

}
