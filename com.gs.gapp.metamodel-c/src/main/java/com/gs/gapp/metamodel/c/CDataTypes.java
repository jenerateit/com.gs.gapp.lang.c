/**
 * 
 */
package com.gs.gapp.metamodel.c;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * CDataTypes has the parent class of CStructType and CUnionType
 * it conains a set of memberdefinition and flag to control 
 * whether has nested datatype structure or not.
 * 
 * @author dsn
 *
 */
public abstract class CDataTypes extends AbstractCDataTypes {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4735015195964171628L;

	/**
	 * memberDefinitions are members of a C Data Types
	 * e.g 
	 * struct AStruct {
	 *     char a;
	 *     long double b;
	 * } structVar, AnotherStructVar;
	 * a and b are member definitions of struct AStruct
	 */
	private Set<CDataMember> memberDefinitions = new LinkedHashSet<>(); 

	/**
	 * This boolean is to check wheter a datatype contains the definition
	 * of another datatype inside.
	 * 
	 */
	private boolean hasNestedDataType;



	/**
	 * 
	 * @param name
	 * @param identifierName
	 * @param owner
	 * @param memberDefinitions
	 * @param structureVariables
	 */
	public CDataTypes(String name, String identifierName, ModelElementI owner, Set<CDataMember> memberDefinitions, Set<String> structureVariables) {
		super(name,owner);
		this.memberDefinitions = memberDefinitions;
		this.setDataTypeVariables(structureVariables);
		this.setIdentifierName(identifierName);
	}

	/**
	 * 
	 * @param name
	 * @param identifierName
	 * @param owner
	 */
	public CDataTypes(String name, String identifierName, ModelElementI owner) {
		super(name, owner);
		this.setIdentifierName(identifierName);
	}

	/**
	 * @param name
	 * @param identifierName
	 */
	public CDataTypes(String name, String identifierName) {
		super(name);
		this.setIdentifierName(identifierName);
	}
	/**
	 * @param name
	 * @param owner
	 */
	public CDataTypes(String name, ModelElementI owner) {
		super(name, owner);
	}

	/**
	 * @param name
	 */
	public CDataTypes(String name) {
		super(name);
	}

	/**
	 * memberDefinitions are members of a C Data Types
	 * e.g 
	 * struct AStruct {
	 *     char a;
	 *     long double b;
	 * } structVar, AnotherStructVar;
	 * a and b are member definitions of struct AStruct
	 * 
	 * @return the memberDefinitions
	 */
	public Set<CDataMember> getMemberDefinitions() {
		return memberDefinitions;
	}

	/**
	 * @param memberDefinitions the memberDefinitions to set
	 */
	public void setMemberDefinitions(Set<CDataMember> memberDefinitions) {
		this.memberDefinitions = memberDefinitions;
		for(CDataMember cDataMember : memberDefinitions) {
			if(cDataMember.getType() instanceof CDataTypes) {
				setHasNestedDataType(true);
			}
		}
	}

	/**
	 * @param add memberDef to memberDefinitions
	 */
	public void addMemberDefinition (CDataMember memberDef) {
		this.memberDefinitions.add(memberDef);
		if(memberDef.getType() instanceof CDataTypes) {
			setHasNestedDataType(true);
		}
	}

	/**
	 * This boolean is to check wheter a datatype contains the definition
	 * of another datatype inside.
	 * 
	 * @return the hasNestedDataType
	 */
	public boolean hasNestedDataType() {
		return hasNestedDataType;
	}

	/**
	 * @param hasNestedDataType the hasNestedDataType to set
	 */
	public void setHasNestedDataType(boolean hasNestedDataType) {
		this.hasNestedDataType = hasNestedDataType;
	}



}
