package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * @author dsn
 *
 */
public abstract class AbstractCType<T extends ModelElementI> extends CModelElement<T> implements CTypeI {

	private static final long serialVersionUID = 1L;

	/**
	 * @param name
	 */
	public AbstractCType(String name) {
		super(name);
	}



	public AbstractCType(String name, T owner) {
		super(name, owner);
	}





	

}
