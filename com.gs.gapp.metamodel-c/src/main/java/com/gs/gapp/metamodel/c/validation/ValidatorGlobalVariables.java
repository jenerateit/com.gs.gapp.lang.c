/**
 * 
 */
package com.gs.gapp.metamodel.c.validation;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.c.CFile;
import com.gs.gapp.metamodel.c.CVariable;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;

/**
 * @author dsn
 *
 */
public class ValidatorGlobalVariables implements ModelValidatorI {

	/**
	 * 
	 */
	public ValidatorGlobalVariables() {	}

	/**
	 * 
	 */
	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(globalVariablesHaveUniqueNames(modelElements));
		result.addAll(globalVariablesCannotBeAuto(modelElements));
		result.addAll(globalVariablesCannotBeRegister(modelElements));
		return result;
	}




	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> globalVariablesHaveUniqueNames(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CFile) {
				CFile cFile =  (CFile) element;
				Set<CVariable> allGlobalVariables = cFile.getGlobalVariables();
				Set<String> allGlobalVariableNames = new LinkedHashSet<>();
				for(CVariable globalVar : allGlobalVariables) {
					if (!allGlobalVariableNames.add(globalVar.getName())) {
						StringBuilder sb = new StringBuilder("Variable: ");
						sb.append(globalVar.getName()).append(" in ").append(((CFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") is defined twice."
								+ " Each Global Variable must have a unique name");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}
	
	
	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> globalVariablesCannotBeAuto(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CFile) {
				CFile cFile =  (CFile) element;
				Set<CVariable> allGlobalVariables = cFile.getGlobalVariables();
				for(CVariable globalVar : allGlobalVariables) {
					if (globalVar.getStorageSpecifier() == StorageClassSpecifiersEnum.AUTO) {
						StringBuilder sb = new StringBuilder("Variable: ");
						sb.append(globalVar.getName()).append(" in ").append(((CFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") has storage specifier auto"
								+ "but it is not allowed as global variable in c. ");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}
	
	/**
	 * 
	 * @param rawElements
	 * @return result
	 */
	private Collection<Message> globalVariablesCannotBeRegister(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();

		for (Object element : rawElements) {
			if (element instanceof CFile) {
				CFile cFile =  (CFile) element;
				Set<CVariable> allGlobalVariables = cFile.getGlobalVariables();
				for(CVariable globalVar : allGlobalVariables) {
					if (globalVar.getStorageSpecifier() == StorageClassSpecifiersEnum.REGISTER) {
						StringBuilder sb = new StringBuilder("Variable: ");
						sb.append(globalVar.getName()).append(" in ").append(((CFile) element).getName()).append(".");
						Message message = new Message(MessageStatus.ERROR, "(" + sb + ") has storage specifier register"
								+ "but it is not allowed as global variable in c. ");
						result.add(message);				       
					}
				}
			}
		}

		return result;
	}




}
