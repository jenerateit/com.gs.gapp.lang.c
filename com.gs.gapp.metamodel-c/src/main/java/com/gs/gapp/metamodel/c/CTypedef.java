package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * 
 * CTypedef is used to define typedefs. When an existing type or used defined data type
 * is typedefed, newIdentifierName becomes the type name. We still keep track of 
 * the typedefed type with existingCType.
 * 
 * @author dsn
 *
 */
public class CTypedef extends AbstractCType<ModelElementI> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7979179751720325421L;

	protected final String newIdentifierName;

	protected final AbstractCType<ModelElementI> existingCType;

	/**
	 * 
	 * @param newIdentifierName
	 * @param existingCType
	 * @param owner
	 */
	public CTypedef(String newIdentifierName, AbstractCType<ModelElementI> existingCType, ModelElementI owner) {
		super("typedef", owner);
		this.newIdentifierName = newIdentifierName;
		this.existingCType = existingCType;
	}

	/**
	 * 
	 * @param newIdentifierName
	 * @param existingCType
	 */
	public CTypedef(String newIdentifierName, AbstractCType<ModelElementI> existingCType) {
		super("typedef");
		this.newIdentifierName = newIdentifierName;
		this.existingCType = existingCType;
	}

	/**
	 * 
	 * @return newIdentifierName
	 */
	public String getNewIdentifierName() {
		return newIdentifierName;
	}

	/**
	 * 
	 * @return existingCType
	 */
	public AbstractCType<ModelElementI> getExistingCType() {
		return existingCType;
	}


}
