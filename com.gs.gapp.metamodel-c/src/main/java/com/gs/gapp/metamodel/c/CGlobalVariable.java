/**
 * 
 */
package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum;

/**
 * CGlobalVariable is the CVariable that is used as global variable.
 * 
 * @author dsn
 *
 */
public class CGlobalVariable extends CVariable {

	/**
	 * @param derivedType
	 * @param owner
	 * @param value
	 */
	public CGlobalVariable(CDerivedType derivedType, ModelElementI owner, String value) {
		super(derivedType, owner, value);
	}

	/**
	 * @param abstractCDataTypes
	 * @param owner
	 * @param value
	 */
	public CGlobalVariable(AbstractCDataTypes abstractCDataTypes, ModelElementI owner, String value) {
		super(abstractCDataTypes, owner, value);
	}

	/**
	 * @param cTypedef
	 * @param owner
	 * @param value
	 */
	public CGlobalVariable(CTypedef cTypedef, ModelElementI owner, String value) {
		super(cTypedef, owner, value);
	}

	/**
	 * @param derivedType
	 * @param value
	 */
	public CGlobalVariable(CDerivedType derivedType, String value) {
		super(derivedType, derivedType.getOwner(), value);
	}

	/**
	 * @param derivedType
	 */
	public CGlobalVariable(CDerivedType derivedType) {
		super(derivedType, derivedType.getOwner(), "");
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 2096072440810834621L;

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 */
	public CGlobalVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner, value, type, storageSpecifier, typeQualifiersEnum);
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param storageSpecifier
	 */
	public CGlobalVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type,
			StorageClassSpecifiersEnum storageSpecifier) {
		super(name, owner, value, type, storageSpecifier);
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 * @param typeQualifiersEnum
	 */
	public CGlobalVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type,
			TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner, value, type, typeQualifiersEnum);
	}

	/**
	 * @param name
	 * @param owner
	 * @param value
	 * @param type
	 */
	public CGlobalVariable(String name, ModelElementI owner, String value, AbstractCType<ModelElementI> type) {
		super(name, owner, value, type);
	}


	/**
	 * 
	 * @param name
	 * @param owner
	 * @param type
	 */
	public CGlobalVariable(String name, ModelElementI owner, AbstractCType<ModelElementI> type) {
		super(name, owner, "", type);
	}






}
