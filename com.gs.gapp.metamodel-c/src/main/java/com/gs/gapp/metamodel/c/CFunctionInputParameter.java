/**
 * 
 */
package com.gs.gapp.metamodel.c;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum;
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum;

/**
 * 
 * CFunctionInput parameter is also a CVariable, this class is used to 
 * set function input parameter.
 * 
 * @author dsn
 *
 */
public class CFunctionInputParameter extends CVariable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -9014228571840389537L;

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param abstractCDataTypes
	 */
	public CFunctionInputParameter(String name, ModelElementI owner, AbstractCDataTypes abstractCDataTypes ) {
		super(name, owner, "", abstractCDataTypes);
	}

	/**
	 * 
	 * @param abstractCDataTypes
	 * @param owner
	 */
	public CFunctionInputParameter(AbstractCDataTypes abstractCDataTypes, ModelElementI owner) {
		super(abstractCDataTypes, owner, "");
	}


	/**
	 * 
	 * @param derivedType
	 * @param owner
	 */
	public CFunctionInputParameter(CDerivedType derivedType, ModelElementI owner) {
		super(derivedType, owner, "");
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param type
	 * @param storageSpecifier
	 * @param typeQualifiersEnum
	 */
	public CFunctionInputParameter(String name, ModelElementI owner, AbstractCType<ModelElementI> type,
			StorageClassSpecifiersEnum storageSpecifier, TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner, "", type, storageSpecifier, typeQualifiersEnum);
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param type
	 * @param storageSpecifier
	 */
	public CFunctionInputParameter(String name, ModelElementI owner, AbstractCType<ModelElementI> type,
			StorageClassSpecifiersEnum storageSpecifier) {
		super(name, owner, "", type, storageSpecifier);
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param type
	 * @param typeQualifiersEnum
	 */
	public CFunctionInputParameter(String name, ModelElementI owner, AbstractCType<ModelElementI> type,
			TypeQualifiersEnum typeQualifiersEnum) {
		super(name, owner, "", type, typeQualifiersEnum);
	}

	/**
	 * 
	 * @param name
	 * @param owner
	 * @param type
	 */
	public CFunctionInputParameter(String name, ModelElementI owner, AbstractCType<ModelElementI> type) {
		super(name, owner, "", type);
	}


}
