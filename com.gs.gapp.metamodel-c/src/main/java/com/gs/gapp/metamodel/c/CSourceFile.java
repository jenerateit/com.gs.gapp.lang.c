package com.gs.gapp.metamodel.c;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 
 * CSourFile extends from CFile. Sections of a sourcefile consists of 
 * Includes
 * Macro Definitions
 * Typedefs
 * Global Variables
 * Local Variables
 * Global Composite Data Type Declarations
 * Local Function Prototypes
 * Global Functions
 * 
 * @author dsn
 *
 */
public class CSourceFile extends CFile{
	
	/**
	 * The Set that holds the local variables
	 */
	private Set<CVariable> moduleVariables = new LinkedHashSet<>();
	
	/**
	 * The set that holds the local function prototypes
	 */
	private Set<CFunctionPrototype> localFunctionPrototypes = new LinkedHashSet<>();
	
	/**
	 * global functions are held in that set
	 */
	private Set<CFunction> globalFunctions = new LinkedHashSet<>();

	/**
	 * local functions are held the local functions
	 */
	private Set<CFunction> localFunctions = new LinkedHashSet<>();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3678353605306484210L;
	
	/**
	 * 
	 * @param name
	 */
	public CSourceFile(String name) {
		super(name);
	}

	/**
	 * return all the local variables
	 * 
	 * @return localVariables
	 */
	public Set<CVariable> getModuleVariables() {
		return moduleVariables;
	}

	/**
	 * 
	 * @param localVariables
	 */
	public void setModuleVariables(Set<CVariable> localVariables) {
		this.moduleVariables = localVariables;
	}
	
	/**
	 * 
	 * @param localVariable
	 */
	public void addModuleVariable(CVariable localVariable) {
		this.moduleVariables.add(localVariable);
	}
	
	/**
	 * return all the local function prototypes
	 * 
	 * @return localFunctionPrototypes
	 */
	public Set<CFunctionPrototype> getLocalFunctionPrototypes() {
		return localFunctionPrototypes;
	}

	/**
	 * 
	 * @param localFunctionPrototypes
	 */
	public void setLocalFunctionPrototypes(Set<CFunctionPrototype> localFunctionPrototypes) {
		this.localFunctionPrototypes = localFunctionPrototypes;
	}
	
	/**
	 * 
	 * @param localFunctionPrototype
	 */
	public void addLocalFunctionPrototypes(CFunctionPrototype localFunctionPrototype) {
		this.localFunctionPrototypes.add(localFunctionPrototype);
	}
	
	/**
	 * 
	 * @param localFunctionPrototype
	 */
	public void addLocalFunctionPrototypes(CFunction cFunction) {
		addLocalFunctionPrototypes(new CFunctionPrototype(cFunction));
	}
	
	/**
	 * return all the functions which are located in the global functions section
	 * 
	 * @return globalFunctions
	 */
	public Set<CFunction> getGlobalFunctions() {
		return globalFunctions;
	}

	/**
	 * 
	 * @param globalFunctions
	 */
	public void setGlobalFunctions(Set<CFunction> globalFunctions) {
		this.globalFunctions = globalFunctions;
	}

	/**
	 * 
	 * @param globalFunction
	 * @return true if successfully added
	 */
	public boolean addGlobalFunction(CFunction globalFunction) {
		if (this.globalFunctions.add(globalFunction)) {
			globalFunction.setOwner(this);
			return true;
		}
		return false;
	}

	/**
	 * return all the functions which are located in the local functions section
	 * 
	 * @return localFunctions
	 */
	public Set<CFunction> getLocalFunctions() {
		return localFunctions;
	}

	/**
	 * 
	 * @param localFunctions
	 */
	public void setLocalFunctions(Set<CFunction> localFunctions) {
		this.localFunctions = localFunctions;
	}
	
	/**
	 * 
	 * @param localFunction
	 * @return true if the function is added correctly
	 */
	public boolean addLocalFunction(CFunction localFunction) {
		if (this.localFunctions.add(localFunction)) {
			localFunction.setOwner(this);
			CFunctionPrototype cFuncPrototype = new CFunctionPrototype(localFunction);
			if(localFunction.getBody()!=null) {
				cFuncPrototype.setBody(localFunction.getBody());
			}
			addLocalFunctionPrototypes(cFuncPrototype);
			return true;
		}
		return false;
	}
}
