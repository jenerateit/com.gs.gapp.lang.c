package com.gs.gapp.metamodel.c;

import java.util.EnumMap;

import com.gs.gapp.metamodel.basic.ModelElementI;
/**
 * 
 * BasicTypes consists of 2 components  
 * Basic Arithmetic types : int, char, double, float
 * Modifiers : signed, unsigned, short and long
 * 
 * 
 * @author dsn
 */
public final class CBasicType extends AbstractCType<ModelElementI> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7117132258195792129L;

	/**
	 * basicTypeModifiers can be none, signed, unsigned, short, long or combination
	 * of multiple of them
	 */
	private final String basicTypeModifier;
	
	/**
	 * basicArithmeticType can be none, int, char, double or float
	 * 
	 */
	private final String basicArithmeticType;
	
	/**
	 * 
	 * @param modifier
	 * @param name
	 */
	private CBasicType(String modifier, String name) {
		super(modifier.isEmpty()? name: modifier + " " + name);
		this.basicTypeModifier = modifier;	
		this.basicArithmeticType = name;
	}
	
	/**
	 * basicTypeModifiers can be none, signed, unsigned, short, long or combination
	 * of multiple of them
	 * 
	 * @return basicTypeModifier
	 */
    public String getBasicTypeModifier() {
		return basicTypeModifier;
	}
    
    /**
     * basicArithmeticType can be none, int, char, double or float
     * 
     * @return basicArithmeticType
     */
    public String getBasicArithmeticType() {
    	return basicArithmeticType;
    }

	private static EnumMap<BasicArithmeticTypeEnum, String> basicArithmeticTypeEnumMap = new EnumMap<>(BasicArithmeticTypeEnum.class);
    private static EnumMap<BasicTypeModifierEnum, String> basicTypeModifiersEnumMap = new EnumMap<>(BasicTypeModifierEnum.class);

    static {
    	basicTypeModifiersEnumMap.put(BasicTypeModifierEnum.NONE, "");
	    basicTypeModifiersEnumMap.put(BasicTypeModifierEnum.SIGNED, "signed");
	    basicTypeModifiersEnumMap.put(BasicTypeModifierEnum.UNSIGNED, "unsigned");
	    basicTypeModifiersEnumMap.put(BasicTypeModifierEnum.SHORT, "short");
	    basicTypeModifiersEnumMap.put(BasicTypeModifierEnum.LONG, "long");
	    
	    basicArithmeticTypeEnumMap.put(BasicArithmeticTypeEnum.NONE, "");
	    basicArithmeticTypeEnumMap.put(BasicArithmeticTypeEnum.INT, "int");
	    basicArithmeticTypeEnumMap.put(BasicArithmeticTypeEnum.CHAR, "char");
	    basicArithmeticTypeEnumMap.put(BasicArithmeticTypeEnum.DOUBLE, "double");
	    basicArithmeticTypeEnumMap.put(BasicArithmeticTypeEnum.FLOAT, "float");
    }
    
    /**
     * 
     * @author dsn
     *
     */
	public static enum BasicType {
		CHAR (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.NONE)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.CHAR))),
		SIGNED_CHAR (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.CHAR))),
		UNSIGEND_CHAR (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.UNSIGNED)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.CHAR))),
		SHORT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SHORT)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		SHORT_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SHORT)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		SIGNED_SHORT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SHORT)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		SIGNED_SHORT_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SHORT)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		UNSIGNED_SHORT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.UNSIGNED) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SHORT)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.NONE)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		SIGNED (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		SIGNED_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		UNSIGNED (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.UNSIGNED)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		UNSIGNED_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.UNSIGNED)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		LONG (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		LONG_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		SIGNED_LONG (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		SIGNED_LONG_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		UNSIGNED_LONG (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.UNSIGNED) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		UNSIGNED_LONG_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.UNSIGNED) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		LONG_LONG (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		LONG_LONG_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		SIGNED_LONG_LONG (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED)
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		SIGNED_LONG_LONG_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.SIGNED)
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		UNSIGNED_LONG_LONG (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.UNSIGNED)
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.NONE))),
		UNSIGNED_LONG_LONG_INT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.UNSIGNED)
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG) 
				+ " " + basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.INT))),
		FLOAT (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.NONE)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.FLOAT))),
		DOUBLE (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.NONE)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.DOUBLE))),
		LONG_DOUBLE (new CBasicType(basicTypeModifiersEnumMap.get(BasicTypeModifierEnum.LONG)
				, basicArithmeticTypeEnumMap.get(BasicArithmeticTypeEnum.DOUBLE))),
		;
		
		
		private final CBasicType type;
		
		/**
		 * 
		 * @param type
		 */
		BasicType(CBasicType type) {
			this.type = type;
		}
		
		/**
		 * 
		 * @return type
		 */
		public CBasicType getType() {
			return type;
		}
		
	}
	
	
	/**
	 * 
	 * @author dsn
	 *
	 */
	protected enum BasicArithmeticTypeEnum {
		NONE ,
		INT ,
		CHAR,
		DOUBLE,
		FLOAT	
	}
	
	/**
	 * 
	 * @author dsn
	 *
	 */
	protected enum BasicTypeModifierEnum {
		NONE,
		SIGNED,
		UNSIGNED,
		SHORT,
		LONG
	}


	
}
