package com.gs.vd.c.test

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CHeaderFile
import com.gs.gapp.metamodel.c.CModuleVariable
import com.gs.gapp.metamodel.c.CSourceFile
import com.gs.gapp.metamodel.c.CTypedef
import com.gs.gapp.metamodel.c.CVariable
import com.gs.gapp.metamodel.c.CBasicType.BasicType
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum
/**
 *
 * @author dsn
 *
 *	This class is to generate tests for global variables for source files.
 */
class TestGlobalVariablesSectionForSource extends AbstractTestModel {

	/**
	 *
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CHeaderFile cHeaderFile = new CHeaderFile("TestGlobalVariablesSectionForHeader");
		
		int i = 0;
		for(BasicType basicTypeQualifier : CBasicType.BasicType.values()) {
			for(StorageClassSpecifiersEnum storageClassSpecifier : StorageClassSpecifiersEnum.values()) {
				if(storageClassSpecifier != StorageClassSpecifiersEnum.AUTO && storageClassSpecifier != StorageClassSpecifiersEnum.REGISTER) {
					for(TypeQualifiersEnum typeQualifier : TypeQualifiersEnum.values()) {
						if(storageClassSpecifier != StorageClassSpecifiersEnum.STATIC) {
							CVariable cVariable001 = new CModuleVariable("var"+i+"tgvh",cHeaderFile, i.toString(), basicTypeQualifier.getType()/*getName()*/, storageClassSpecifier, typeQualifier);
							cHeaderFile.addGlobalVariable(cVariable001);
							i++;
						}
					}
				}
			}
		}
		
		result.add(cHeaderFile);
		return result;
	}

}
