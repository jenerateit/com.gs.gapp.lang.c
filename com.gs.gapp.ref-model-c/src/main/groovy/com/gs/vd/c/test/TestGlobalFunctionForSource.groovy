package com.gs.vd.c.test

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CArrayType
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CDataMember
import com.gs.gapp.metamodel.c.CEnumType
import com.gs.gapp.metamodel.c.CFunction
import com.gs.gapp.metamodel.c.CFunctionInputParameter
import com.gs.gapp.metamodel.c.CModuleVariable
import com.gs.gapp.metamodel.c.CPointerType
import com.gs.gapp.metamodel.c.CSourceFile
import com.gs.gapp.metamodel.c.CStructType
import com.gs.gapp.metamodel.c.CTypedef
import com.gs.gapp.metamodel.c.CUnionType
import com.gs.gapp.metamodel.c.CVariable
import com.gs.gapp.metamodel.c.CVoidType
import com.gs.gapp.metamodel.c.CEnumType.CEnumConstant
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum
/**
 * 
 * This class intend to test generation of global function with different 
 * input-output types for source files (.c).  
 */
public class TestGlobalFunctionForSource extends AbstractTestModel {

	/**
	 *
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("TestGlobalFunctionForSource");
		
		CStructType struct1 = new CStructType("AStructsgg",cSourceFile);
		struct1.addDataTypeVariable("structVarsgg");
		struct1.addDataTypeVariable("AnotherStructVarsgg");

		CDataMember cDataMember1 = new CDataMember("agg",struct1, CBasicType.BasicType.CHAR.getType());
		struct1.addMemberDefinition(cDataMember1);

		CDataMember cDataMember2 = new CDataMember("bgg",struct1, CBasicType.BasicType.LONG_DOUBLE.getType());
		struct1.addMemberDefinition(cDataMember2);

		CArrayType aNewArray = new CArrayType("anArraygg", 2, cSourceFile, CBasicType.BasicType.INT.getType());
		aNewArray.addDimensionSize(3);
		aNewArray.addDimensionSize(6);  //even if arraysize is 5 and if we have only 3 dimension size we generate only 3 dimensional array with sizes.
		aNewArray.addDimensionSize(99); //array size increases automatically as we add dimensionsize
		
		CFunction localFunction = new CFunction("myFirstLocalFunctionsgg", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		cSourceFile.addLocalFunction(localFunction);

		CUnionType union1 = new CUnionType("AUniontdhgg",cSourceFile);
		union1.addDataTypeVariable("unionVartdhgg");
		union1.addDataTypeVariable("AnotherUnionVarhgg");
		CDataMember cDataMember3 = new CDataMember("ctdhgg",union1, CBasicType.BasicType.CHAR.getType());
		union1.addMemberDefinition(cDataMember3);

		CDataMember cDataMember4 = new CDataMember("dtdhgg",union1, CBasicType.BasicType.LONG_DOUBLE.getType());
		union1.addMemberDefinition(cDataMember4);

		CDataMember structAsAMember = new CDataMember("xtdhgg", union1, struct1);
		union1.addMemberDefinition(structAsAMember);

		CFunction mySecondLocalFunction = new CFunction("mySecondLocalFunctionsgg", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("aParamsgg", mySecondLocalFunction,CBasicType.BasicType.INT.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("bParamsgg", mySecondLocalFunction, CBasicType.BasicType.CHAR.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("cParamggs", mySecondLocalFunction,CBasicType.BasicType.FLOAT.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("structasaparametersgg",mySecondLocalFunction, struct1));
		

		cSourceFile.addLocalFunction(mySecondLocalFunction);

		CPointerType aPointertoAIntxx = new CPointerType("apointertoCharsgg", null, CBasicType.BasicType.CHAR.getType());
		CPointerType aPointerToAPointerxx = new CPointerType("apointertoCharPointersgg", null, aPointertoAIntxx);
		CPointerType aPointerToNotTypedefCDataxx = new CPointerType("myFourthPointersgg", null, union1);
		CPointerType aPointerToNotTypedefCData2xx = new CPointerType("myFifthPointersgg", null, aPointerToNotTypedefCDataxx);

		CFunction myThirdLocalFunction = new CFunction("myThirdLocalFunctionsgg", cSourceFile, aPointerToAPointerxx, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("aParamsgg", myThirdLocalFunction,CBasicType.BasicType.INT.getType()));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("aPointerToACharsgg",myThirdLocalFunction, aPointerToNotTypedefCDataxx));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("cParamsgg", myThirdLocalFunction,aPointerToNotTypedefCData2xx));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("structasaparametersgg",myThirdLocalFunction, struct1));

		cSourceFile.addGlobalDataTypeDeclaration(struct1);
		cSourceFile.addGlobalDataTypeDeclaration(union1);

		
		CFunction myFirstGlobalFunction =  new CFunction("myFirstGlobalFunctiongg", cSourceFile, CBasicType.BasicType.INT.getType());
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("xgg", myFirstGlobalFunction,CBasicType.BasicType.INT.getType()));
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("ygg", myFirstGlobalFunction, CBasicType.BasicType.CHAR.getType()));
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("zgg", myFirstGlobalFunction,CBasicType.BasicType.FLOAT.getType()));
		
		
		CPointerType mySecondPointer = new CPointerType("mySecondPointergg", mySecondLocalFunction, CBasicType.BasicType.INT.getType());
		CVariable cSecondPointerVariable= new CModuleVariable(mySecondPointer);
		myFirstGlobalFunction.addLocalVariable(cSecondPointerVariable);
		CModuleVariable intLocalVar = new CModuleVariable("anotherVargg", myFirstGlobalFunction, "3", CBasicType.BasicType.INT.getType())
		myFirstGlobalFunction.addLocalVariable(intLocalVar);
		myFirstGlobalFunction.setReturnVariable(intLocalVar);
		
		CFunction mySecondGlobalFunction =  new CFunction("mySecondGlobalFunctiongg", cSourceFile, new CVoidType());
		
		
		cSourceFile.addGlobalFunction(myFirstGlobalFunction);
		cSourceFile.addGlobalFunction(mySecondGlobalFunction);
		
		
		CEnumType enumDeclaration = new CEnumType("colorgg", cSourceFile);
		enumDeclaration.addConstantWithoutValue("RED");
		enumDeclaration.addConstantWithoutValue("GREEN");
		enumDeclaration.addConstantWithoutValue("BLUE");
		enumDeclaration.addConstant(new CEnumConstant("BLACK","1"));
		enumDeclaration.addConstantWithoutValue("WHITE");
		enumDeclaration.addDataTypeVariable("somecolors")

		cSourceFile.addGlobalDataTypeDeclaration(enumDeclaration);
		
		
		CFunction myThirdGlobalFunction =  new CFunction("myThirdGlobalFunctiongg", cSourceFile, struct1);
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myThirdGlobalFunction, union1));
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myThirdGlobalFunction, struct1));
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myThirdGlobalFunction, enumDeclaration));
		cSourceFile.addGlobalFunction(myThirdGlobalFunction);
		
		CFunction myFourthGlobalFunction =  new CFunction("myFourthGlobalFunctiongg", cSourceFile, union1);
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myFourthGlobalFunction, union1));
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myFourthGlobalFunction, struct1));
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myFourthGlobalFunction, enumDeclaration));
		cSourceFile.addGlobalFunction(myFourthGlobalFunction);
		
		CFunction myFifthGlobalFunction =  new CFunction("myFifthGlobalFunctiongg", cSourceFile, enumDeclaration);
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myFifthGlobalFunction, union1));
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myFifthGlobalFunction, struct1));
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myFifthGlobalFunction, enumDeclaration));
		cSourceFile.addGlobalFunction(myFifthGlobalFunction);
		
		CPointerType aPointerAsReturnType = new CPointerType("mySecondPointergg", null, CBasicType.BasicType.INT.getType());
		
		CFunction mySixthGlobalFunction =  new CFunction("mySixthGlobalFunctiongg", cSourceFile, aPointerAsReturnType);
		cSourceFile.addGlobalFunction(mySixthGlobalFunction);
		
		CPointerType aPointerToStructAsReturnType = new CPointerType("myThirddPointergg", null, struct1);
		
		CFunction mySeventhGlobalFunction =  new CFunction("mySeventhGlobalFunctiongg", cSourceFile, aPointerToStructAsReturnType);
		cSourceFile.addGlobalFunction(mySeventhGlobalFunction);
		
		CTypedef aTypeDef = new CTypedef("SIGNEDLONGINTgg",CBasicType.BasicType.SIGNED_LONG_INT.getType(), cSourceFile);
		cSourceFile.addTypeDefs(aTypeDef);
		
		CPointerType aPointerToATypedefedEnum = new CPointerType("myFourthPointergg", null, enumDeclaration);
		CPointerType aTypeToAPrimitiveType = new CPointerType("aTypeDefPointergg", null , aTypeDef);
		
		
		CFunction myEigthGlobalFunction =  new CFunction("myEigthGlobalFunctiongg", cSourceFile, aPointerToATypedefedEnum);
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToATypedefedEnum",myEigthGlobalFunction, aPointerToATypedefedEnum));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToStructAsReturnType",myEigthGlobalFunction, aPointerToStructAsReturnType));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerAsReturnType",myEigthGlobalFunction, aPointerAsReturnType));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("y", myFirstGlobalFunction, CBasicType.BasicType.CHAR.getType()));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aTypeToAPrimitiveType",myEigthGlobalFunction, aTypeToAPrimitiveType));
		
		CFunction myNintheGlobalFunction =  new CFunction("myNintheGlobalFunctiongg", cSourceFile, aTypeToAPrimitiveType);
		
		
		CPointerType aPointerToNotTypedefCData = new CPointerType("myFourthPointergg", null, union1);
		CPointerType aPointerToNotTypedefCData2 = new CPointerType("myFifthPointergg", null, aPointerToNotTypedefCData);
		
		CPointerType aPointertoAInt = new CPointerType("apointertoChargg", null, CBasicType.BasicType.CHAR.getType());
		CPointerType aPointerToAPointer = new CPointerType("apointertoCharPointergg", null, aPointertoAInt);
		CPointerType aPointerToATypedef = new CPointerType("apointerToATypeDefgg", null, struct1);
		CPointerType aPointerToATypedef2 = new CPointerType("apointerToATypeDefxxgg", null, aPointerToATypedef);
		
		CFunction myTenthGlobalFunction =  new CFunction("myTenthGlobalFunctiongg", cSourceFile, aPointerToAPointer);
		myTenthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToATypedefedEnumgg",myTenthGlobalFunction, aPointerToNotTypedefCData2));
		myTenthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToAChargg",myTenthGlobalFunction, aPointertoAInt));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointertoAInt));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToAPointer));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToNotTypedefCData));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToNotTypedefCData2));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToATypedef));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToATypedef2));
		myTenthGlobalFunction.setReturnVariable(new CModuleVariable(aPointerToAPointer));
		
		CPointerType aPointerToAUnion = new CPointerType("aPointerToAUniongg", null, union1);
		CFunction myTwelvethGlobalFunction =  new CFunction("myTwelvethGlobalFunctiongg", cSourceFile, aPointerToAUnion);
		CFunctionInputParameter aPointerToAPointerInputParameter = new CFunctionInputParameter("aPointerToAPointergg",myTwelvethGlobalFunction, aPointerToAPointer);
		aPointerToAPointerInputParameter.setTypeQualifier(TypeQualifiersEnum.CONST);
		myTwelvethGlobalFunction.addInputParameter(aPointerToAPointerInputParameter);
		myTwelvethGlobalFunction.addInputParameter(new CFunctionInputParameter("aMultiDimArraygg",myTwelvethGlobalFunction, aNewArray));
		CPointerType aPointerToArray = new CPointerType("aPointerToAnArraygg", null, aNewArray);
		CFunctionInputParameter aPointerToArrayParam = new CFunctionInputParameter("aPointerToAnArrayParametergg",myTwelvethGlobalFunction, aPointerToArray);
		myTwelvethGlobalFunction.addInputParameter(aPointerToArrayParam);
		CArrayType aNewArray3 = new CArrayType("arrayWithNoDimgg", 1, cSourceFile, CBasicType.BasicType.INT.getType());
		myTwelvethGlobalFunction.addInputParameter(new CFunctionInputParameter("arraywithNoDimParametergg",myTwelvethGlobalFunction,aNewArray3));
		
		
		
		

		cSourceFile.addGlobalFunction(myEigthGlobalFunction);
		cSourceFile.addGlobalFunction(myNintheGlobalFunction);
		cSourceFile.addGlobalFunction(myTenthGlobalFunction);
		cSourceFile.addGlobalFunction(myTwelvethGlobalFunction);


		result.add(cSourceFile);
		return result;
	}

}
