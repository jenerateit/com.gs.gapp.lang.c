/**
 * 
 */
package com.gs.vd.c.test

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CDataMember
import com.gs.gapp.metamodel.c.CEnumType
import com.gs.gapp.metamodel.c.CHeaderFile
import com.gs.gapp.metamodel.c.CStructType
import com.gs.gapp.metamodel.c.CTypedef
import com.gs.gapp.metamodel.c.CUnionType
import com.gs.gapp.metamodel.c.CBasicType.BasicType
import com.gs.gapp.metamodel.c.CEnumType.CEnumConstant

/**
 * @author dsn
 *
 *	Tests the functionality of typedefs in header files. In order to create more complicated
 *	scenarios, user defined data types were used and typedefed.
 */
class TestTypedefsSectionOnlyCDataTypesTypesForHeader extends AbstractTestModel {

	/**
	 * 
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CHeaderFile cHeaderFile = new CHeaderFile("TestTypedefsSectionOnlyCDataTypesTypesForHeader");
		
		CStructType struct1 = new CStructType("AStructtdh",cHeaderFile);
		struct1.addDataTypeVariable("structVartdh");
		struct1.addDataTypeVariable("AnotherStructVartdh");

		CDataMember cDataMember1 = new CDataMember("atdh",struct1, CBasicType.BasicType.CHAR.getType());
		struct1.addMemberDefinition(cDataMember1);

		CDataMember cDataMember2 = new CDataMember("btdh",struct1, CBasicType.BasicType.LONG_DOUBLE.getType());
		struct1.addMemberDefinition(cDataMember2);

		CUnionType union1 = new CUnionType("AUniontdh",cHeaderFile);
		union1.addDataTypeVariable("unionVartdh");
		union1.addDataTypeVariable("AnotherUnionVarh");
		CDataMember cDataMember3 = new CDataMember("ctdh",union1, CBasicType.BasicType.CHAR.getType());
		union1.addMemberDefinition(cDataMember3);

		CDataMember cDataMember4 = new CDataMember("dtdh",union1, CBasicType.BasicType.LONG_DOUBLE.getType());
		union1.addMemberDefinition(cDataMember4);

		CDataMember structAsAMember = new CDataMember("xtdh", union1, struct1);
		union1.addMemberDefinition(structAsAMember);


		CStructType struct2 = new CStructType("BStructtdh",union1);
		struct2.addDataTypeVariable("singleStructVartdh");

		CDataMember cDataMember5 = new CDataMember("aatdh",struct1, CBasicType.BasicType.SHORT_INT.getType());
		struct2.addMemberDefinition(cDataMember5);

		CDataMember cDataMember6 = new CDataMember("bbtdh",struct1, CBasicType.BasicType.FLOAT.getType());
		struct2.addMemberDefinition(cDataMember6);

		CDataMember structAsAMember2 = new CDataMember("ytdh", union1, struct2);

		union1.addMemberDefinition(structAsAMember2);


		CStructType struct3 = new CStructType("CStructtdh",struct2);

		CDataMember cDataMember7 = new CDataMember("aaatdh",struct3, CBasicType.BasicType.UNSIGNED_LONG_LONG_INT.getType());
		struct3.addMemberDefinition(cDataMember7);

		CDataMember cDataMember8 = new CDataMember("bbbtdh",struct3, CBasicType.BasicType.SIGNED_LONG_LONG_INT.getType());
		struct3.addMemberDefinition(cDataMember8);

		CDataMember structAsAMember3 = new CDataMember("ztdh", struct2, struct3);


		struct2.addMemberDefinition(structAsAMember3);


		cHeaderFile.addGlobalDataTypeDeclaration(struct1);
		cHeaderFile.addGlobalDataTypeDeclaration(union1);



		CEnumType enumDeclaration = new CEnumType("colorh", cHeaderFile);
		enumDeclaration.addConstantWithoutValue("RED");
		enumDeclaration.addConstantWithoutValue("GREEN");
		enumDeclaration.addConstantWithoutValue("BLUE");
		enumDeclaration.addConstant(new CEnumConstant("BLACK","1"));
		enumDeclaration.addConstantWithoutValue("WHITE");
		enumDeclaration.addDataTypeVariable("somecolors")

		cHeaderFile.addGlobalDataTypeDeclaration(enumDeclaration);


		CDataMember enumDeclarationAsMember = new CDataMember("enumAsVariableh",struct2 ,enumDeclaration);
		struct2.addMemberDefinition(enumDeclarationAsMember);


		CEnumType enumDeclarationInsideStruct = new CEnumType("dayz", struct2);
		enumDeclarationInsideStruct.addConstantWithoutValue("sun");
		enumDeclarationInsideStruct.addConstantWithoutValue("mon");
		enumDeclarationInsideStruct.addConstantWithoutValue("tue");
		enumDeclarationInsideStruct.addConstant(new CEnumConstant("wed","1"));
		enumDeclarationInsideStruct.addConstantWithoutValue("thurs");
		enumDeclarationInsideStruct.addDataTypeVariable("fri");

		CDataMember enumDefinitionInMemberField = new CDataMember("t",struct2 ,enumDeclarationInsideStruct);

		struct2.addMemberDefinition(enumDefinitionInMemberField);
		
		CTypedef aTypeDef = new CTypedef("STRUCTEINSh",struct1, cHeaderFile);
		cHeaderFile.addTypeDefs(aTypeDef);
		
		CTypedef bTypeDef = new CTypedef("enumtypedefh",enumDeclaration, cHeaderFile);
		cHeaderFile.addTypeDefs(bTypeDef);
		
		CTypedef cTypeDef = new CTypedef("STRUCTEZWEIh",struct2, cHeaderFile);
		cHeaderFile.addTypeDefs(cTypeDef);
		
		CTypedef dTypeDef = new CTypedef("enumtypedefh",enumDeclaration, struct3);
		cHeaderFile.addTypeDefs(dTypeDef);
		
		CTypedef eTypedef = new CTypedef("typedefofBigUnionh", union1, cHeaderFile);
		cHeaderFile.addTypeDefs(eTypedef);
		
		
		result.add(cHeaderFile);
		return result;
	}

}
