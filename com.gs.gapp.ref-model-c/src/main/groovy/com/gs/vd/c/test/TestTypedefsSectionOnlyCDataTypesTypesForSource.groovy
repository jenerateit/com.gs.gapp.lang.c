/**
 * 
 */
package com.gs.vd.c.test

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CDataMember
import com.gs.gapp.metamodel.c.CEnumType
import com.gs.gapp.metamodel.c.CSourceFile
import com.gs.gapp.metamodel.c.CStructType
import com.gs.gapp.metamodel.c.CTypedef
import com.gs.gapp.metamodel.c.CUnionType
import com.gs.gapp.metamodel.c.CBasicType.BasicType
import com.gs.gapp.metamodel.c.CEnumType.CEnumConstant

/**
 * @author dsn
 *
 *	Tests the functionality of typedefs in source files. In order to create more complicated
 *	scenarios, user defined data types were used and typedefed.
 */
class TestTypedefsSectionOnlyCDataTypesTypesForSource extends AbstractTestModel {

	/**
	 * 
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("TestTypedefsSectionOnlyCDataTypesTypesForSource");
		
		CStructType struct1 = new CStructType("AStructtds",cSourceFile);
		struct1.addDataTypeVariable("structVartds");
		struct1.addDataTypeVariable("AnotherStructVartds");

		CDataMember cDataMember1 = new CDataMember("atds",struct1, CBasicType.BasicType.CHAR.getType());
		struct1.addMemberDefinition(cDataMember1);

		CDataMember cDataMember2 = new CDataMember("btds",struct1, CBasicType.BasicType.LONG_DOUBLE.getType());
		struct1.addMemberDefinition(cDataMember2);

		CUnionType union1 = new CUnionType("AUniontds",cSourceFile);
		union1.addDataTypeVariable("unionVartds");
		union1.addDataTypeVariable("AnotherUnionVar");
		CDataMember cDataMember3 = new CDataMember("ctds",union1, CBasicType.BasicType.CHAR.getType());
		union1.addMemberDefinition(cDataMember3);

		CDataMember cDataMember4 = new CDataMember("dtds",union1, CBasicType.BasicType.LONG_DOUBLE.getType());
		union1.addMemberDefinition(cDataMember4);

		CDataMember structAsAMember = new CDataMember("xtds", union1, struct1);
		union1.addMemberDefinition(structAsAMember);


		CStructType struct2 = new CStructType("BStructtds",union1);
		struct2.addDataTypeVariable("singleStructVartds");

		CDataMember cDataMember5 = new CDataMember("aatds",struct1, CBasicType.BasicType.SHORT_INT.getType());
		struct2.addMemberDefinition(cDataMember5);

		CDataMember cDataMember6 = new CDataMember("bbtds",struct1, CBasicType.BasicType.FLOAT.getType());
		struct2.addMemberDefinition(cDataMember6);

		CDataMember structAsAMember2 = new CDataMember("ytds", union1, struct2);

		union1.addMemberDefinition(structAsAMember2);


		CStructType struct3 = new CStructType("CStructtds",struct2);

		CDataMember cDataMember7 = new CDataMember("aaatds",struct3, CBasicType.BasicType.UNSIGNED_LONG_LONG_INT.getType());
		struct3.addMemberDefinition(cDataMember7);

		CDataMember cDataMember8 = new CDataMember("bbbtds",struct3, CBasicType.BasicType.SIGNED_LONG_LONG_INT.getType());
		struct3.addMemberDefinition(cDataMember8);

		CDataMember structAsAMember3 = new CDataMember("ztds", struct2, struct3);


		struct2.addMemberDefinition(structAsAMember3);


		cSourceFile.addGlobalDataTypeDeclaration(struct1);
		cSourceFile.addGlobalDataTypeDeclaration(union1);



		CEnumType enumDeclaration = new CEnumType("color", cSourceFile);
		enumDeclaration.addConstantWithoutValue("RED");
		enumDeclaration.addConstantWithoutValue("GREEN");
		enumDeclaration.addConstantWithoutValue("BLUE");
		enumDeclaration.addConstant(new CEnumConstant("BLACK","1"));
		enumDeclaration.addConstantWithoutValue("WHITE");
		enumDeclaration.addDataTypeVariable("somecolors")

		cSourceFile.addGlobalDataTypeDeclaration(enumDeclaration);


		CDataMember enumDeclarationAsMember = new CDataMember("enumAsVariable",struct2 ,enumDeclaration);
		struct2.addMemberDefinition(enumDeclarationAsMember);


		CEnumType enumDeclarationInsideStruct = new CEnumType("dayz", struct2);
		enumDeclarationInsideStruct.addConstantWithoutValue("sun");
		enumDeclarationInsideStruct.addConstantWithoutValue("mon");
		enumDeclarationInsideStruct.addConstantWithoutValue("tue");
		enumDeclarationInsideStruct.addConstant(new CEnumConstant("wed","1"));
		enumDeclarationInsideStruct.addConstantWithoutValue("thurs");
		enumDeclarationInsideStruct.addDataTypeVariable("fri");

		CDataMember enumDefinitionInMemberField = new CDataMember("t",struct2 ,enumDeclarationInsideStruct);

		struct2.addMemberDefinition(enumDefinitionInMemberField);
		
		CTypedef aTypeDef = new CTypedef("STRUCTEINS",struct1, cSourceFile);
		cSourceFile.addTypeDefs(aTypeDef);
		
		CTypedef bTypeDef = new CTypedef("enumtypedef",enumDeclaration, cSourceFile);
		cSourceFile.addTypeDefs(bTypeDef);
		
		CTypedef cTypeDef = new CTypedef("STRUCTEZWEI",struct2, cSourceFile);
		cSourceFile.addTypeDefs(cTypeDef);
		
		CTypedef dTypeDef = new CTypedef("enumtypedef",enumDeclaration, struct3);
		cSourceFile.addTypeDefs(dTypeDef);
		
		CTypedef eTypedef = new CTypedef("typedefofBigUnion", union1, cSourceFile);
		cSourceFile.addTypeDefs(eTypedef);
		
		
		result.add(cSourceFile);
		return result;
	}

}
