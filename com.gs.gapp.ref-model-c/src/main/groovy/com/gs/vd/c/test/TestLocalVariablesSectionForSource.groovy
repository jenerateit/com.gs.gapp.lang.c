package com.gs.vd.c.test

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CModuleVariable
import com.gs.gapp.metamodel.c.CSourceFile
import com.gs.gapp.metamodel.c.CTypedef
import com.gs.gapp.metamodel.c.CVariable
import com.gs.gapp.metamodel.c.CBasicType.BasicType
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum
/**
 * 
 * @author dsn
 *
 *	Tests the local variables section with all possible basic types all possible
 *	specifiers and qualifiers. (Source files)
 */
class TestLocalVariablesSectionForSource extends AbstractTestModel {

	/**
	 *
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("TestLocalVariablesSectionForSource");
		
		int i = 0;
		for(BasicType basicTypeQualifier : CBasicType.BasicType.values()) {
			for(StorageClassSpecifiersEnum storageClassSpecifier : StorageClassSpecifiersEnum.values()) {
				if(storageClassSpecifier != StorageClassSpecifiersEnum.AUTO && storageClassSpecifier != StorageClassSpecifiersEnum.REGISTER) {
					for(TypeQualifiersEnum typeQualifier : TypeQualifiersEnum.values()) {
						if(storageClassSpecifier == StorageClassSpecifiersEnum.STATIC) {
							CVariable cVariable001 = new CModuleVariable("var"+i+"locals",cSourceFile, i.toString(), basicTypeQualifier.getType()/*getName()*/, storageClassSpecifier, typeQualifier);
							cSourceFile.addModuleVariable(cVariable001);
							i++;
						}
					}
				}
			}
		}
		
		result.add(cSourceFile);
		return result;
	}

}
