/**
 * 
 */
package com.gs.vd.c.test

import java.util.Set

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CFunctionLikeMacro
import com.gs.gapp.metamodel.c.CHeaderFile
import com.gs.gapp.metamodel.c.CMacro
import com.gs.gapp.metamodel.c.CObjectLikeMacro
import com.gs.gapp.metamodel.c.CSourceFile

/**
 * @author dsn
 *
 *	Tests the marcro definitions for both source and header files.
 */
class TestMacroDefinitionSectionForSourceAndHeader extends AbstractTestModel {

	/**
	 * 
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("TestSourceFileForMacroDef");
		CHeaderFile cHeaderFile = new CHeaderFile("TestHeaderFileForMacroDef");
		
		CMacro aDefinition = new CObjectLikeMacro("FIRSTMACROsh", cSourceFile, "3");
		CMacro bDefinition = new CObjectLikeMacro("SECONDMACROsh", cSourceFile);
		CMacro cDefinition = new CObjectLikeMacro("THIRDMACROsh", cSourceFile, "3" , "5");

		CMacro functionLikeMacro = new CFunctionLikeMacro("RADTODEGsh", cSourceFile, "x", "((x) * 57.29578)");
		
		CMacro aDefinitionH = new CObjectLikeMacro("FIRSTMACROHsh", cSourceFile, "3");
		CMacro bDefinitionH = new CObjectLikeMacro("SECONDMACROHsh", cSourceFile);
		CMacro cDefinitionH = new CObjectLikeMacro("THIRDMACROHsh", cSourceFile, "3" , "5");

		CMacro functionLikeMacroH = new CFunctionLikeMacro("RADTODEGsh", cSourceFile, "x", "((x) * 57.29578)")

		cSourceFile.addObjectLikeMacro(aDefinition);
		cSourceFile.addObjectLikeMacro(bDefinition);
		cSourceFile.addObjectLikeMacro(cDefinition);
		cSourceFile.addFunctionLikeMacro(functionLikeMacro);
		
		cHeaderFile.addObjectLikeMacro(aDefinitionH);
		cHeaderFile.addObjectLikeMacro(bDefinitionH);
		cHeaderFile.addObjectLikeMacro(cDefinitionH);
		cHeaderFile.addFunctionLikeMacro(functionLikeMacroH);
		
		result.add(cSourceFile);
		result.add(cHeaderFile);
		return result;
	}
}
