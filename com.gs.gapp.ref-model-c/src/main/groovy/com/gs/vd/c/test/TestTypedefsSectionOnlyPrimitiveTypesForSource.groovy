package com.gs.vd.c.test

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CGlobalVariable
import com.gs.gapp.metamodel.c.CSourceFile
import com.gs.gapp.metamodel.c.CTypedef
import com.gs.gapp.metamodel.c.CVariable
import com.gs.gapp.metamodel.c.CVoidType
import com.gs.gapp.metamodel.c.CBasicType.BasicType
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum

/**
 * @author dsn
 *
 *	All primitive types were typedefed in source.
 */
class TestTypedefsSectionOnlyPrimitiveTypesForSource extends AbstractTestModel {

	/**
	 * 
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("TestSourceForTypedefsSectoinOnlyPrimitiveTypesForSource");
		CVariable tempVar = new CGlobalVariable("dummy", cSourceFile, "", new CVoidType());
		int i = 0;
		CTypedef aTypeDef;
		
		for(BasicType basicTypeQualifier : CBasicType.BasicType.values()) {
			for(StorageClassSpecifiersEnum storageClassSpecifier : StorageClassSpecifiersEnum.values()) {
				if(storageClassSpecifier != StorageClassSpecifiersEnum.AUTO && storageClassSpecifier != StorageClassSpecifiersEnum.REGISTER) {
					for(TypeQualifiersEnum typeQualifier : TypeQualifiersEnum.values()) {
						if(storageClassSpecifier != StorageClassSpecifiersEnum.STATIC) {
							CVariable cVariable001 = new CGlobalVariable("var"+i+"tdis",cSourceFile, i.toString(), basicTypeQualifier.getType()/*getName()*/, storageClassSpecifier, typeQualifier);
							if(cVariable001.getType() != tempVar.getType()) {
								aTypeDef = new CTypedef("var"+i+"typedefedInSource",cVariable001.getType(), cSourceFile);
								cSourceFile.addTypeDefs(aTypeDef);
								tempVar = cVariable001;

							}
							if(aTypeDef != null) {
								cSourceFile.addGlobalVariable(new CGlobalVariable("var"+i+"tdis", cSourceFile, i.toString(), aTypeDef, storageClassSpecifier, typeQualifier));
							}
							i++;
						}
					}
				}
			}
		}

		result.add(cSourceFile);
		return result;
	}

}
