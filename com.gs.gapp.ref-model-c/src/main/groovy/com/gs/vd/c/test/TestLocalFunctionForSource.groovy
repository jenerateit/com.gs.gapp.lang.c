package com.gs.vd.c.test

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CArrayType
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CDataMember
import com.gs.gapp.metamodel.c.CEnumType
import com.gs.gapp.metamodel.c.CFunction
import com.gs.gapp.metamodel.c.CFunctionInputParameter
import com.gs.gapp.metamodel.c.CModuleVariable
import com.gs.gapp.metamodel.c.CPointerType
import com.gs.gapp.metamodel.c.CSourceFile
import com.gs.gapp.metamodel.c.CStructType
import com.gs.gapp.metamodel.c.CTypedef
import com.gs.gapp.metamodel.c.CUnionType
import com.gs.gapp.metamodel.c.CVariable
import com.gs.gapp.metamodel.c.CVoidType
import com.gs.gapp.metamodel.c.CEnumType.CEnumConstant
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum
/**
 * 
 * This class intend to test generation of global function with different 
 * input-output types for source files (.c).  
 */
public class TestLocalFunctionForSource extends AbstractTestModel {

	/**
	 *
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("TestLocalFunctionForSource");
		
		CStructType struct1 = new CStructType("AStructsll",cSourceFile);
		struct1.addDataTypeVariable("structVarsll");
		struct1.addDataTypeVariable("AnotherStructVarsll");

		CDataMember cDataMember1 = new CDataMember("all",struct1, CBasicType.BasicType.CHAR.getType());
		struct1.addMemberDefinition(cDataMember1);

		CDataMember cDataMember2 = new CDataMember("bll",struct1, CBasicType.BasicType.LONG_DOUBLE.getType());
		struct1.addMemberDefinition(cDataMember2);

		CArrayType aNewArray = new CArrayType("anArrayll", 2, cSourceFile, CBasicType.BasicType.INT.getType());
		aNewArray.addDimensionSize(3);
		aNewArray.addDimensionSize(6);  //even if arraysize is 5 and if we have only 3 dimension size we generate only 3 dimensional array with sizes.
		aNewArray.addDimensionSize(99); //array size increases automatically as we add dimensionsize
		
		CFunction localFunction = new CFunction("myFirstLocalFunctionsll", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);

		CUnionType union1 = new CUnionType("AUniontdhll",cSourceFile);
		union1.addDataTypeVariable("unionVartdhll");
		union1.addDataTypeVariable("AnotherUnionVarhll");
		CDataMember cDataMember3 = new CDataMember("ctdhll",union1, CBasicType.BasicType.CHAR.getType());
		union1.addMemberDefinition(cDataMember3);

		CDataMember cDataMember4 = new CDataMember("dtdhll",union1, CBasicType.BasicType.LONG_DOUBLE.getType());
		union1.addMemberDefinition(cDataMember4);

		CDataMember structAsAMember = new CDataMember("xtdhll", union1, struct1);
		union1.addMemberDefinition(structAsAMember);

		CFunction mySecondLocalFunction = new CFunction("mySecondLocalFunctionsll", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("aParamsll", mySecondLocalFunction,CBasicType.BasicType.INT.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("bParamsll", mySecondLocalFunction, CBasicType.BasicType.CHAR.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("cParamll", mySecondLocalFunction,CBasicType.BasicType.FLOAT.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("structasaparametersll",mySecondLocalFunction, struct1));
		


		CPointerType aPointertoAIntxx = new CPointerType("apointertoCharsll", null, CBasicType.BasicType.CHAR.getType());
		CPointerType aPointerToAPointerxx = new CPointerType("apointertoCharPointersll", null, aPointertoAIntxx);
		CPointerType aPointerToNotTypedefCDataxx = new CPointerType("myFourthPointersll", null, union1);
		CPointerType aPointerToNotTypedefCData2xx = new CPointerType("myFifthPointersll", null, aPointerToNotTypedefCDataxx);

		CFunction myThirdLocalFunction = new CFunction("myThirdLocalFunctionsll", cSourceFile, aPointerToAPointerxx, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("aParamsll", myThirdLocalFunction,CBasicType.BasicType.INT.getType()));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("aPointerToACharsll",myThirdLocalFunction, aPointerToNotTypedefCDataxx));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("cParamsll", myThirdLocalFunction,aPointerToNotTypedefCData2xx));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("structasaparametersll",myThirdLocalFunction, struct1));

		cSourceFile.addGlobalDataTypeDeclaration(struct1);
		cSourceFile.addGlobalDataTypeDeclaration(union1);

		
		CFunction myFirstGlobalFunction =  new CFunction("myFirstGlobalFunctionll", cSourceFile, CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("xll", myFirstGlobalFunction,CBasicType.BasicType.INT.getType()));
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("yll", myFirstGlobalFunction, CBasicType.BasicType.CHAR.getType()));
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("zll", myFirstGlobalFunction,CBasicType.BasicType.FLOAT.getType()));
		
		
		CPointerType mySecondPointer = new CPointerType("mySecondPointerll", mySecondLocalFunction, CBasicType.BasicType.INT.getType());
		CVariable cSecondPointerVariable= new CModuleVariable(mySecondPointer);
		myFirstGlobalFunction.addLocalVariable(cSecondPointerVariable);
		CModuleVariable intLocalVar = new CModuleVariable("anotherVarll", myFirstGlobalFunction, "3", CBasicType.BasicType.INT.getType())
		myFirstGlobalFunction.addLocalVariable(intLocalVar);
		myFirstGlobalFunction.setReturnVariable(intLocalVar);
		
		CFunction mySecondGlobalFunction =  new CFunction("mySecondGlobalFunctionll", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		
		

		
		
		CEnumType enumDeclaration = new CEnumType("colorll", cSourceFile);
		enumDeclaration.addConstantWithoutValue("RED");
		enumDeclaration.addConstantWithoutValue("GREEN");
		enumDeclaration.addConstantWithoutValue("BLUE");
		enumDeclaration.addConstant(new CEnumConstant("BLACK","1"));
		enumDeclaration.addConstantWithoutValue("WHITE");
		enumDeclaration.addDataTypeVariable("somecolors")

		cSourceFile.addGlobalDataTypeDeclaration(enumDeclaration);
		
		
		CFunction myThirdGlobalFunction =  new CFunction("myThirdGlobalFunctionll", cSourceFile, struct1, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myThirdGlobalFunction, union1));
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myThirdGlobalFunction, struct1));
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myThirdGlobalFunction, enumDeclaration));
		
		CFunction myFourthGlobalFunction =  new CFunction("myFourthGlobalFunctionll", cSourceFile, union1, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myFourthGlobalFunction, union1));
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myFourthGlobalFunction, struct1));
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myFourthGlobalFunction, enumDeclaration));

		
		CFunction myFifthGlobalFunction =  new CFunction("myFifthGlobalFunctionll", cSourceFile, enumDeclaration, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myFifthGlobalFunction, union1));
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myFifthGlobalFunction, struct1));
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myFifthGlobalFunction, enumDeclaration));
		
		CPointerType aPointerAsReturnType = new CPointerType("mySecondPointerll", null, CBasicType.BasicType.INT.getType());
		
		CFunction mySixthGlobalFunction =  new CFunction("mySixthGlobalFunctionll", cSourceFile, aPointerAsReturnType, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);

		
		CPointerType aPointerToStructAsReturnType = new CPointerType("myThirddPointerll", null, struct1);
		
		CFunction mySeventhGlobalFunction =  new CFunction("mySeventhGlobalFunctionll", cSourceFile, aPointerToStructAsReturnType, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);

		CTypedef aTypeDef = new CTypedef("SIGNEDLONGINTll",CBasicType.BasicType.SIGNED_LONG_INT.getType(), cSourceFile);
		cSourceFile.addTypeDefs(aTypeDef);
		
		CPointerType aPointerToATypedefedEnum = new CPointerType("myFourthPointerll", null, enumDeclaration);
		CPointerType aTypeToAPrimitiveType = new CPointerType("aTypeDefPointerll", null , aTypeDef);
		
		
		CFunction myEigthGlobalFunction =  new CFunction("myEigthGlobalFunctionll", cSourceFile, aPointerToATypedefedEnum, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToATypedefedEnum",myEigthGlobalFunction, aPointerToATypedefedEnum));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToStructAsReturnType",myEigthGlobalFunction, aPointerToStructAsReturnType));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerAsReturnType",myEigthGlobalFunction, aPointerAsReturnType));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("y", myFirstGlobalFunction, CBasicType.BasicType.CHAR.getType()));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aTypeToAPrimitiveType",myEigthGlobalFunction, aTypeToAPrimitiveType));
		
		CFunction myNintheGlobalFunction =  new CFunction("myNintheGlobalFunctionll", cSourceFile, aTypeToAPrimitiveType, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		
		
		CPointerType aPointerToNotTypedefCData = new CPointerType("myFourthPointerll", null, union1);
		CPointerType aPointerToNotTypedefCData2 = new CPointerType("myFifthPointerll", null, aPointerToNotTypedefCData);
		
		CPointerType aPointertoAInt = new CPointerType("apointertoCharll", null, CBasicType.BasicType.CHAR.getType());
		CPointerType aPointerToAPointer = new CPointerType("apointertoCharPointerll", null, aPointertoAInt);
		CPointerType aPointerToATypedef = new CPointerType("apointerToATypeDefll", null, struct1);
		CPointerType aPointerToATypedef2 = new CPointerType("apointerToATypeDefxxll", null, aPointerToATypedef);
		
		CFunction myTenthGlobalFunction =  new CFunction("myTenthGlobalFunctionll", cSourceFile, aPointerToAPointer, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myTenthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToATypedefedEnumll",myTenthGlobalFunction, aPointerToNotTypedefCData2));
		myTenthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToACharll",myTenthGlobalFunction, aPointertoAInt));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointertoAInt));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToAPointer));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToNotTypedefCData));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToNotTypedefCData2));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToATypedef));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToATypedef2));
		myTenthGlobalFunction.setReturnVariable(new CModuleVariable(aPointerToAPointer));
		
		CPointerType aPointerToAUnion = new CPointerType("aPointerToAUnionll", null, union1);
		CFunction myTwelvethGlobalFunction =  new CFunction("myTwelvethGlobalFunctionll", cSourceFile, aPointerToAUnion, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		CFunctionInputParameter aPointerToAPointerInputParameter = new CFunctionInputParameter("aPointerToAPointerll",myTwelvethGlobalFunction, aPointerToAPointer);
		aPointerToAPointerInputParameter.setTypeQualifier(TypeQualifiersEnum.CONST);
		myTwelvethGlobalFunction.addInputParameter(aPointerToAPointerInputParameter);
		myTwelvethGlobalFunction.addInputParameter(new CFunctionInputParameter("aMultiDimArrayll",myTwelvethGlobalFunction, aNewArray));
		CPointerType aPointerToArray = new CPointerType("aPointerToAnArrayll", null, aNewArray);
		CFunctionInputParameter aPointerToArrayParam = new CFunctionInputParameter("aPointerToAnArrayParameterll",myTwelvethGlobalFunction, aPointerToArray);
		myTwelvethGlobalFunction.addInputParameter(aPointerToArrayParam);
		CArrayType aNewArray3 = new CArrayType("arrayWithNoDimll", 1, cSourceFile, CBasicType.BasicType.INT.getType());
		myTwelvethGlobalFunction.addInputParameter(new CFunctionInputParameter("arraywithNoDimParameterll",myTwelvethGlobalFunction,aNewArray3));
		
		
		
		cSourceFile.addLocalFunction(myFirstGlobalFunction);
		cSourceFile.addLocalFunction(mySecondGlobalFunction);
		cSourceFile.addLocalFunction(mySeventhGlobalFunction);
		cSourceFile.addLocalFunction(mySixthGlobalFunction);
		cSourceFile.addLocalFunction(myFifthGlobalFunction);
		cSourceFile.addLocalFunction(localFunction);
		cSourceFile.addLocalFunction(mySecondLocalFunction);
		cSourceFile.addLocalFunction(myEigthGlobalFunction);
		cSourceFile.addLocalFunction(myNintheGlobalFunction);
		cSourceFile.addLocalFunction(myTenthGlobalFunction);
		cSourceFile.addLocalFunction(myTwelvethGlobalFunction);
		cSourceFile.addLocalFunction(myFourthGlobalFunction);
		cSourceFile.addLocalFunction(myThirdGlobalFunction);


		result.add(cSourceFile);
		return result;
	}

}
