package com.gs.vd.c.test

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CDataMember
import com.gs.gapp.metamodel.c.CFunction
import com.gs.gapp.metamodel.c.CFunctionInputParameter
import com.gs.gapp.metamodel.c.CPointerType
import com.gs.gapp.metamodel.c.CSourceFile
import com.gs.gapp.metamodel.c.CStructType
import com.gs.gapp.metamodel.c.CUnionType
import com.gs.gapp.metamodel.c.CVoidType
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum
/**
 * @author dsn
 * Tests the local function prototypes with different input-output types for source files.
 */
class TestLocalFunctionPrototypesForSource extends AbstractTestModel {

	/**
	 *
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("TestLocalFunctionPrototypesForSource");
		
		CStructType struct1 = new CStructType("AStructs",cSourceFile);
		struct1.addDataTypeVariable("structVars");
		struct1.addDataTypeVariable("AnotherStructVars");

		CDataMember cDataMember1 = new CDataMember("a",struct1, CBasicType.BasicType.CHAR.getType());
		struct1.addMemberDefinition(cDataMember1);

		CDataMember cDataMember2 = new CDataMember("b",struct1, CBasicType.BasicType.LONG_DOUBLE.getType());
		struct1.addMemberDefinition(cDataMember2);

		
		CFunction localFunction = new CFunction("myFirstLocalFunctions", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		cSourceFile.addLocalFunctionPrototypes(localFunction);

		CUnionType union1 = new CUnionType("AUniontdh",cSourceFile);
		union1.addDataTypeVariable("unionVartdh");
		union1.addDataTypeVariable("AnotherUnionVarh");
		CDataMember cDataMember3 = new CDataMember("ctdh",union1, CBasicType.BasicType.CHAR.getType());
		union1.addMemberDefinition(cDataMember3);

		CDataMember cDataMember4 = new CDataMember("dtdh",union1, CBasicType.BasicType.LONG_DOUBLE.getType());
		union1.addMemberDefinition(cDataMember4);

		CDataMember structAsAMember = new CDataMember("xtdh", union1, struct1);
		union1.addMemberDefinition(structAsAMember);

		CFunction mySecondLocalFunction = new CFunction("mySecondLocalFunctions", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("aParams", mySecondLocalFunction,CBasicType.BasicType.INT.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("bParams", mySecondLocalFunction, CBasicType.BasicType.CHAR.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("cParams", mySecondLocalFunction,CBasicType.BasicType.FLOAT.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("structasaparameters",mySecondLocalFunction, struct1));
		

		cSourceFile.addLocalFunctionPrototypes(mySecondLocalFunction);

		CPointerType aPointertoAIntxx = new CPointerType("apointertoChars", null, CBasicType.BasicType.CHAR.getType());
		CPointerType aPointerToAPointerxx = new CPointerType("apointertoCharPointers", null, aPointertoAIntxx);
		CPointerType aPointerToNotTypedefCDataxx = new CPointerType("myFourthPointers", null, union1);
		CPointerType aPointerToNotTypedefCData2xx = new CPointerType("myFifthPointers", null, aPointerToNotTypedefCDataxx);

		CFunction myThirdLocalFunction = new CFunction("myThirdLocalFunctions", cSourceFile, aPointerToAPointerxx, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("aParams", myThirdLocalFunction,CBasicType.BasicType.INT.getType()));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("aPointerToAChars",myThirdLocalFunction, aPointerToNotTypedefCDataxx));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("cParams", myThirdLocalFunction,aPointerToNotTypedefCData2xx));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("structasaparameters",myThirdLocalFunction, struct1));

		cSourceFile.addGlobalDataTypeDeclaration(struct1);
		cSourceFile.addGlobalDataTypeDeclaration(union1);

		
		
		
				
		cSourceFile.addLocalFunctionPrototypes(myThirdLocalFunction);

		result.add(cSourceFile);
		return result;
	}

}
