/**
 * 
 */
package com.gs.vd.c.test

import java.util.Set

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CHeaderFile
import com.gs.gapp.metamodel.c.CSourceFile

/**
 * @author dsn
 * Tests the includes section of header files.
 */
public class TestIncludesSectionForSourceAndHeader extends AbstractTestModel {

	/**
	 * 
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("TestSourceFileForIncludesSection");
		CSourceFile cSourceFile2 = new CSourceFile("TestSourceFileForIncludesSectionZwei");
		
		CHeaderFile cHeaderFile = new CHeaderFile("TestHeaderFileForIncludesSection");
		CHeaderFile cHeaderFile2 = new CHeaderFile("TestHeaderFileForIncludesSectionZwei");
		
		
		cSourceFile.addInclude(cSourceFile2);
		cSourceFile.addInclude(cHeaderFile);
		cSourceFile.addInclude(cHeaderFile2);
		
		cSourceFile.addIncludesSystemHeader("stdio.h");
		cHeaderFile.addIncludesSystemHeader("stdlib.h");
		
		cSourceFile2.addInclude(cHeaderFile);
		cSourceFile2.addInclude(cHeaderFile2);
		
		cSourceFile.addIncludesSystemHeader("stdio.h");
		cHeaderFile.addIncludesSystemHeader("stdlib.h");
		
		
		
		result.add(cSourceFile);
		result.add(cHeaderFile);
		result.add(cSourceFile2);
		result.add(cHeaderFile2);
		return result;
	}

}
