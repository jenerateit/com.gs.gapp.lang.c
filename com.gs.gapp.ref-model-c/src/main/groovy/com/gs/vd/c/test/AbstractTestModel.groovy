package com.gs.vd.c.test;

import org.jenerateit.modelconverter.ModelConverterException
import org.jenerateit.modelconverter.ModelConverterI
import org.jenerateit.modelconverter.ModelConverterOptions

import com.gs.gapp.metamodel.basic.ModelElementI

public class AbstractTestModel implements ModelConverterI {

	private final static java.lang.String NS_AS_STRING = "com.gs.vd.c.test";

	public AbstractTestModel() {}

	/**
	 * @return
	 */
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = new LinkedHashSet<ModelElementI>();
		return result;
	}

    /* (non-Javadoc)
     * @see org.jenerateit.modelconverter.ModelConverterI#convert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
     */
    @Override
	public Set<Object> convert(Collection<?> elements, ModelConverterOptions options)
			throws ModelConverterException {
        Set<Object> result = new LinkedHashSet<Object>(onConvert());
		return result;
	}

	@Override
	public java.util.List<java.lang.String> getErrors() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public java.util.List<java.lang.String> getInfos() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public java.util.List<java.lang.String> getWarnings() {
        return Collections.EMPTY_LIST;
	}
}
