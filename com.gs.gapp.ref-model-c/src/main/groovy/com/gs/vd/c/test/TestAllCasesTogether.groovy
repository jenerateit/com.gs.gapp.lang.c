package com.gs.vd.c.test;

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.c.CArrayType
import com.gs.gapp.metamodel.c.CBasicType
import com.gs.gapp.metamodel.c.CDataMember
import com.gs.gapp.metamodel.c.CEnumType
import com.gs.gapp.metamodel.c.CFunctionLikeMacro
import com.gs.gapp.metamodel.c.CGlobalVariable
import com.gs.gapp.metamodel.c.CHeaderFile
import com.gs.gapp.metamodel.c.CIntegerType
import com.gs.gapp.metamodel.c.CModuleVariable
import com.gs.gapp.metamodel.c.CMacro
import com.gs.gapp.metamodel.c.CObjectLikeMacro
import com.gs.gapp.metamodel.c.CPointerType
import com.gs.gapp.metamodel.c.CSourceFile
import com.gs.gapp.metamodel.c.CStructType
import com.gs.gapp.metamodel.c.CTypedef
import com.gs.gapp.metamodel.c.CUnionType
import com.gs.gapp.metamodel.c.CVariable
import com.gs.gapp.metamodel.c.CVoidType
import com.gs.gapp.metamodel.c.CBasicType.BasicType
import com.gs.gapp.metamodel.c.CEnumType.CEnumConstant
import com.gs.gapp.metamodel.c.CIntegerType.StdIntTypes
import com.gs.gapp.metamodel.c.CFunction
import com.gs.gapp.metamodel.c.CFunctionInputParameter
import com.gs.gapp.metamodel.c.enums.StorageClassSpecifiersEnum
import com.gs.gapp.metamodel.c.enums.TypeQualifiersEnum

/**
 * This class provided model elements that are based on types
 * of the C metamodel.
 *
 * @author dsn
 *
 */
public class TestAllCasesTogether extends AbstractTestModel {

	/**
	 * @return
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		CSourceFile cSourceFile = new CSourceFile("AllCases");
		cSourceFile.addInclude("AllCases.h");
		cSourceFile.addIncludesSystemHeader("stdio.h");
		cSourceFile.addIncludesSystemHeader("stdint.h");


		CMacro aDefinition = new CObjectLikeMacro("FIRSTMACRO", cSourceFile, "3");
		aDefinition.setBody("FIRSTMACRO");
		CMacro bDefinition = new CObjectLikeMacro("SECONDMACRO", cSourceFile);
		CMacro cDefinition = new CObjectLikeMacro("THIRDMACRO", cSourceFile, "3" , "5");

		CMacro functionLikeMacro = new CFunctionLikeMacro("RADTODEG", cSourceFile, "x", "((x) * 57.29578)");
		functionLikeMacro.setBody("RADTOREG JAVADOC");

		cSourceFile.addObjectLikeMacro(aDefinition);
		cSourceFile.addObjectLikeMacro(bDefinition);
		cSourceFile.addObjectLikeMacro(cDefinition);

		cSourceFile.addFunctionLikeMacro(functionLikeMacro);



		//		BasicTypeQualifiersEnum[] possibleBasicTypeQualifiers = BasicTypeQualifiersEnum.getEnumConstants();
		//		Object[] possibleStorageClassSpecifiers = StorageClassSpecifiersEnum.getEnumConstants();
		//		Object[] possibleTypeQualifiers = StorageClassSpecifiersEnum.getEnumConstants();

		int i = 0;
		for(BasicType basicTypeQualifier : CBasicType.BasicType.values()) {
			for(StorageClassSpecifiersEnum storageClassSpecifier : StorageClassSpecifiersEnum.values()) {
				if(storageClassSpecifier != StorageClassSpecifiersEnum.AUTO && storageClassSpecifier != StorageClassSpecifiersEnum.REGISTER) {
					for(TypeQualifiersEnum typeQualifier : TypeQualifiersEnum.values()) {
						if(storageClassSpecifier == StorageClassSpecifiersEnum.STATIC) {
							CVariable cVariable001 = new CModuleVariable("var"+i,cSourceFile, i.toString(), basicTypeQualifier.getType()/*getName()*/, storageClassSpecifier, typeQualifier);
							cVariable001.setBody("JAVADOC: "+ cVariable001.getName());
							cSourceFile.addModuleVariable(cVariable001);
						} else {
							CVariable cVariable0 = new CGlobalVariable("var"+i,cSourceFile, i.toString(), basicTypeQualifier.getType()/*getName()*/, storageClassSpecifier, typeQualifier);
							cVariable0.setBody("JAVADOC: "+ cVariable0.getName());

							cSourceFile.addGlobalVariable(cVariable0);
						}
						i++;
					}
				}
			}

		}


		int j = 0;
		for(StdIntTypes stdTypes : CIntegerType.StdIntTypes.values()) {
			for(StorageClassSpecifiersEnum storageClassSpecifier : StorageClassSpecifiersEnum.values()) {
				if(storageClassSpecifier != StorageClassSpecifiersEnum.AUTO && storageClassSpecifier != StorageClassSpecifiersEnum.REGISTER) {
					for(TypeQualifiersEnum typeQualifier : TypeQualifiersEnum.values()) {
						if(storageClassSpecifier == StorageClassSpecifiersEnum.STATIC) {
							CVariable cVariable001 = new CModuleVariable("varCIntTypes"+j,cSourceFile, j.toString(), stdTypes.getType()/*getName()*/, storageClassSpecifier, typeQualifier);
							cSourceFile.addModuleVariable(cVariable001);
							cVariable001.setBody("Javadoc: "+cVariable001.getName());
						} else {
							CVariable cVariable0 = new CGlobalVariable("varCIntTypes"+j,cSourceFile, j.toString(), stdTypes.getType()/*getName()*/, storageClassSpecifier, typeQualifier);
							cSourceFile.addGlobalVariable(cVariable0);
							cVariable0.setBody("Javadoc: "+cVariable0.getName());

						}
						j++;
					}
				}
			}

		}

		//		struct AStruct {
		//			int a;
		//			int b;
		//		} myStructVar1;




		CStructType struct1 = new CStructType("AStruct",cSourceFile);
		struct1.addDataTypeVariable("structVar");
		struct1.addDataTypeVariable("AnotherStructVar");

		CDataMember cDataMember1 = new CDataMember("a",struct1, CBasicType.BasicType.CHAR.getType());
		struct1.addMemberDefinition(cDataMember1);

		CDataMember cDataMember2 = new CDataMember("b",struct1, CBasicType.BasicType.LONG_DOUBLE.getType());
		struct1.addMemberDefinition(cDataMember2);








		CUnionType union1 = new CUnionType("AUnion",cSourceFile);
		union1.setBody("JAVADOC: "+ union1.getName());
		union1.addDataTypeVariable("unionVar");
		union1.addDataTypeVariable("AnotherUnionVar");
		CDataMember cDataMember3 = new CDataMember("c",union1, CBasicType.BasicType.CHAR.getType());
		union1.addMemberDefinition(cDataMember3);
		cDataMember3.setBody("JAVADOC: asd");
		CDataMember cDataMember4 = new CDataMember("d",union1, CBasicType.BasicType.LONG_DOUBLE.getType());
		union1.addMemberDefinition(cDataMember4);


		CDataMember structAsAMember = new CDataMember("x", union1, struct1);
		union1.addMemberDefinition(structAsAMember);



		CStructType struct2 = new CStructType("BStruct",union1);
		struct2.addDataTypeVariable("singleStructVar");

		CDataMember cDataMember5 = new CDataMember("aa",struct1, CBasicType.BasicType.SHORT_INT.getType());
		struct2.addMemberDefinition(cDataMember5);

		CDataMember cDataMember6 = new CDataMember("bb",struct1, CBasicType.BasicType.FLOAT.getType());
		struct2.addMemberDefinition(cDataMember6);

		CDataMember structAsAMember2 = new CDataMember("y", union1, struct2);


		union1.addMemberDefinition(structAsAMember2);



		CStructType struct3 = new CStructType("CStruct",struct2);

		CDataMember cDataMember7 = new CDataMember("aaa",struct3, CBasicType.BasicType.UNSIGNED_LONG_LONG_INT.getType());
		struct3.addMemberDefinition(cDataMember7);

		CDataMember cDataMember8 = new CDataMember("bbb",struct3, CBasicType.BasicType.SIGNED_LONG_LONG_INT.getType());
		struct3.addMemberDefinition(cDataMember8);

		CDataMember structAsAMember3 = new CDataMember("z", struct2, struct3);


		struct2.addMemberDefinition(structAsAMember3);




		cSourceFile.addGlobalDataTypeDeclaration(struct1);
		cSourceFile.addGlobalDataTypeDeclaration(union1);



		CEnumType enumDeclaration = new CEnumType("color", cSourceFile);
		enumDeclaration.addConstantWithoutValue("RED");
		enumDeclaration.addConstantWithoutValue("GREEN");
		enumDeclaration.addConstantWithoutValue("BLUE");
		enumDeclaration.addConstant(new CEnumConstant("BLACK","1"));
		enumDeclaration.addConstantWithoutValue("WHITE");
		enumDeclaration.addDataTypeVariable("somecolors")

		cSourceFile.addGlobalDataTypeDeclaration(enumDeclaration);


		CDataMember enumDeclarationAsMember = new CDataMember("enumAsVariable",struct2 ,enumDeclaration);
		struct2.addMemberDefinition(enumDeclarationAsMember);



		CEnumType enumDeclarationInsideStruct = new CEnumType("dayz", struct2);
		enumDeclarationInsideStruct.addConstantWithoutValue("sun");
		enumDeclarationInsideStruct.addConstantWithoutValue("mon");
		enumDeclarationInsideStruct.addConstantWithoutValue("tue");
		enumDeclarationInsideStruct.addConstant(new CEnumConstant("wed","1"));
		enumDeclarationInsideStruct.addConstantWithoutValue("thurs");
		enumDeclarationInsideStruct.addDataTypeVariable("fri");

		CDataMember enumDefinitionInMemberField = new CDataMember("t",struct2 ,enumDeclarationInsideStruct);

		struct2.addMemberDefinition(enumDefinitionInMemberField);



		CArrayType aNewArray = new CArrayType("anArray", 2, cSourceFile, CBasicType.BasicType.INT.getType());
		aNewArray.addDimensionSize(3);
		aNewArray.addDimensionSize(6);  //even if arraysize is 5 and if we have only 3 dimension size we generate only 3 dimensional array with sizes.
		aNewArray.addDimensionSize(99); //array size increases automatically as we add dimensionsize


		CVariable cArrayVariable= new CGlobalVariable(aNewArray);
		cSourceFile.addGlobalVariable(cArrayVariable);


		CArrayType aNewArray2 = new CArrayType("anotherArray", 1, cSourceFile, CBasicType.BasicType.INT.getType());
		aNewArray2.addDimensionSize(9);
		CVariable cArrayVariable2= new CGlobalVariable(aNewArray2);
		cSourceFile.addGlobalVariable(cArrayVariable2);

		CPointerType myFirstPointer = new CPointerType("firstPointer", cSourceFile, CBasicType.BasicType.INT.getType());
		CVariable cPointerVariable= new CGlobalVariable(myFirstPointer);
		cSourceFile.addGlobalVariable(cPointerVariable);
		//CVariable cPointerVariable2= new CGlobalVariable(cPointerVariable); // Pointer to a pointer is an error as well.
		//cSourceFile.addGlobalVariable(cPointerVariable2);
		/*------------------------------------------LOCAL FUNCTIONS -------------------------------*/
		CFunction localFunction = new CFunction("myFirstLocalFunction", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		cSourceFile.addLocalFunction(localFunction);


		CFunction mySecondLocalFunction = new CFunction("mySecondLocalFunction", cSourceFile, new CVoidType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("aParam", mySecondLocalFunction,CBasicType.BasicType.INT.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("bParam", mySecondLocalFunction, CBasicType.BasicType.CHAR.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("cParam", mySecondLocalFunction,CBasicType.BasicType.FLOAT.getType()));
		mySecondLocalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",mySecondLocalFunction, struct1));
		mySecondLocalFunction.setBody("mySecondLocalFunction");


		cSourceFile.addLocalFunction(mySecondLocalFunction);

		CPointerType aPointertoAIntxx = new CPointerType("apointertoChar", null, CBasicType.BasicType.CHAR.getType());
		CPointerType aPointerToAPointerxx = new CPointerType("apointertoCharPointer", null, aPointertoAIntxx);
		CPointerType aPointerToNotTypedefCDataxx = new CPointerType("myFourthPointer", null, union1);
		CPointerType aPointerToNotTypedefCData2xx = new CPointerType("myFifthPointer", null, aPointerToNotTypedefCDataxx);

		CFunction myThirdLocalFunction = new CFunction("myThirdLocalFunction", cSourceFile, aPointerToAPointerxx, StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("aParam", myThirdLocalFunction,CBasicType.BasicType.INT.getType()));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("aPointerToAChar",myThirdLocalFunction, aPointerToNotTypedefCDataxx));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("cParam", myThirdLocalFunction,aPointerToNotTypedefCData2xx));
		myThirdLocalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myThirdLocalFunction, struct1));


		cSourceFile.addLocalFunction(myThirdLocalFunction);





		/*------------------------------------------TYPEDEFS -------------------------------*/



		CTypedef aTypeDef = new CTypedef("SIGNEDLONGINT",CBasicType.BasicType.SIGNED_LONG_INT.getType(), cSourceFile);
		aTypeDef.setBody("JAVADOC FOR TYPEDEFED");
		cSourceFile.addTypeDefs(aTypeDef);

		CTypedef bTypeDef = new CTypedef("ULALA",CBasicType.BasicType.UNSIGNED_LONG_LONG_INT.getType(), cSourceFile);
		cSourceFile.addTypeDefs(bTypeDef);

		CTypedef cTypeDef = new CTypedef("STRUCTEINS",struct1, cSourceFile);
		cSourceFile.addTypeDefs(cTypeDef);

		CTypedef dTypeDef = new CTypedef("enumtypedef",enumDeclaration, cSourceFile);
		cSourceFile.addTypeDefs(dTypeDef);

		//		CPointerType returnAsPointer = new CPointerType("returnAsPointer", CBasicType.BasicType.INT.getType());
		//
		//		CFunction myThirdGlobalFunction =  new CFunction("myThirdGlobalFunction", cSourceFile, returnAsPointer);
		//		cSourceFile.addGlobalFunction(myThirdGlobalFunction);


		CEnumType enumDeclarationZwei = new CEnumType("alphabet");
		enumDeclarationZwei.setBody("JavaDoc for alphabet");
		enumDeclarationZwei.addConstantWithoutValue("a");
		enumDeclarationZwei.addConstantWithoutValue("b");
		enumDeclarationZwei.addConstantWithoutValue("c");
		enumDeclarationZwei.addConstant(new CEnumConstant("d","1"));
		enumDeclarationZwei.addConstantWithoutValue("e");
		enumDeclarationZwei.addDataTypeVariable("f");

		CTypedef fTypeDef = new CTypedef("TYPEDEFFORENUM",enumDeclarationZwei, cSourceFile);
		fTypeDef.setBody("TYPEDEFFORENUM")
		enumDeclarationZwei.setOwner(fTypeDef);
		cSourceFile.addTypeDefs(fTypeDef);


		CStructType structWithTypeDef = new CStructType("structWithTypeDef");
		structWithTypeDef.addDataTypeVariable("structVarOne");
		structWithTypeDef.addDataTypeVariable("AnotherStructVarTwo");

		CDataMember cDataMemberstructWithTypeDef1 = new CDataMember("a",structWithTypeDef, CBasicType.BasicType.CHAR.getType());
		cDataMemberstructWithTypeDef1.setBody("CHECK THIS")
		structWithTypeDef.addMemberDefinition(cDataMemberstructWithTypeDef1);

		CDataMember cDataMemberstructWithTypeDef2 = new CDataMember("b",structWithTypeDef, CBasicType.BasicType.LONG_DOUBLE.getType());
		structWithTypeDef.addMemberDefinition(cDataMemberstructWithTypeDef2);

		CTypedef gTypeDef = new CTypedef("TYPEDEFFORSTRUCT",structWithTypeDef, cSourceFile);
		structWithTypeDef.setOwner(gTypeDef);
		cSourceFile.addTypeDefs(gTypeDef);

		/***************************************************FUNCTIONS WITH IN AND OUT***************************************************************/
		CFunction mainFunction =  new CFunction("main", cSourceFile, CBasicType.BasicType.INT.getType());
		cSourceFile.addGlobalFunction(mainFunction);
		mainFunction.addFunctionBody("return 0;");



		CFunction myFirstGlobalFunction =  new CFunction("myFirstGlobalFunction", cSourceFile, new CVoidType());
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("x", myFirstGlobalFunction,CBasicType.BasicType.INT.getType()));
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("y", myFirstGlobalFunction, CBasicType.BasicType.CHAR.getType()));
		myFirstGlobalFunction.addInputParameter(new CFunctionInputParameter("z", myFirstGlobalFunction,CBasicType.BasicType.FLOAT.getType()));


		CPointerType mySecondPointer = new CPointerType("mySecondPointer", mySecondLocalFunction, CBasicType.BasicType.INT.getType());
		CVariable cSecondPointerVariable= new CModuleVariable(mySecondPointer);
		myFirstGlobalFunction.addLocalVariable(cSecondPointerVariable);
		myFirstGlobalFunction.addLocalVariable(new CModuleVariable("anotherVar", myFirstGlobalFunction, "3", CBasicType.BasicType.INT.getType()));

		CFunction mySecondGlobalFunction =  new CFunction("mySecondGlobalFunction", cSourceFile, new CVoidType());
		mySecondGlobalFunction.setBody("ROCKROCKYOURBODY");
		CVariable usedVar = cSourceFile.getModuleVariables().getAt(0);
		mySecondGlobalFunction.addUsedVariableAndSetAValue(usedVar, "23");
		mySecondGlobalFunction.addUsedVariableAndSetAValue(cSourceFile.getGlobalVariables().getAt(0), "33");

		cSourceFile.addGlobalFunction(myFirstGlobalFunction);
		cSourceFile.addGlobalFunction(mySecondGlobalFunction);

		CFunction myThirdGlobalFunction =  new CFunction("myThirdGlobalFunction", cSourceFile, struct1);
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myThirdGlobalFunction, union1));
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myThirdGlobalFunction, struct1));
		myThirdGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myThirdGlobalFunction, enumDeclaration));
		cSourceFile.addGlobalFunction(myThirdGlobalFunction);

		CFunction myFourthGlobalFunction =  new CFunction("myFourthGlobalFunction", cSourceFile, union1);
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myFourthGlobalFunction, union1));
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myFourthGlobalFunction, struct1));
		myFourthGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myFourthGlobalFunction, enumDeclaration));
		cSourceFile.addGlobalFunction(myFourthGlobalFunction);

		CFunction myFifthGlobalFunction =  new CFunction("myFifthGlobalFunction", cSourceFile, enumDeclaration);
		myFifthGlobalFunction.setBody("GLOBAL FUNCTION JAVA DOC");
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("unionasaparemeter",myFifthGlobalFunction, union1));
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("structasaparameter",myFifthGlobalFunction, struct1));
		myFifthGlobalFunction.addInputParameter(new CFunctionInputParameter("enumasaparameter",myFifthGlobalFunction, enumDeclaration));
		cSourceFile.addGlobalFunction(myFifthGlobalFunction);

		CPointerType aPointerAsReturnType = new CPointerType("mySecondPointer", null, CBasicType.BasicType.INT.getType());

		CFunction mySixthGlobalFunction =  new CFunction("mySixthGlobalFunction", cSourceFile, aPointerAsReturnType);
		cSourceFile.addGlobalFunction(mySixthGlobalFunction);

		CPointerType aPointerToStructAsReturnType = new CPointerType("myThirddPointer", null, struct1);

		CFunction mySeventhGlobalFunction =  new CFunction("mySeventhGlobalFunction", cSourceFile, aPointerToStructAsReturnType);
		cSourceFile.addGlobalFunction(mySeventhGlobalFunction);

		CPointerType aPointerToATypedefedEnum = new CPointerType("myFourthPointer", null, enumDeclaration);
		CPointerType aTypeToAPrimitiveType = new CPointerType("aTypeDefPointer", null , aTypeDef);

		CFunction myEigthGlobalFunction =  new CFunction("myEigthGlobalFunction", cSourceFile, aPointerToATypedefedEnum);
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToATypedefedEnum",myEigthGlobalFunction, aPointerToATypedefedEnum));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToStructAsReturnType",myEigthGlobalFunction, aPointerToStructAsReturnType));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerAsReturnType",myEigthGlobalFunction, aPointerAsReturnType));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("y", myFirstGlobalFunction, CBasicType.BasicType.CHAR.getType()));
		myEigthGlobalFunction.addInputParameter(new CFunctionInputParameter("aTypeToAPrimitiveType",myEigthGlobalFunction, aTypeToAPrimitiveType));

		CFunction myNintheGlobalFunction =  new CFunction("myNintheGlobalFunction", cSourceFile, aTypeToAPrimitiveType);


		CPointerType aPointerToNotTypedefCData = new CPointerType("myFourthPointer", null, union1);
		CPointerType aPointerToNotTypedefCData2 = new CPointerType("myFifthPointer", null, aPointerToNotTypedefCData);

		CPointerType aPointertoAInt = new CPointerType("apointertoChar", null, CBasicType.BasicType.CHAR.getType());
		CPointerType aPointerToAPointer = new CPointerType("apointertoCharPointer", null, aPointertoAInt);
		CPointerType aPointerToATypedef = new CPointerType("apointerToATypeDef", null, struct1);
		CPointerType aPointerToATypedef2 = new CPointerType("apointerToATypeDefxx", null, aPointerToATypedef);

		CFunction myTenthGlobalFunction =  new CFunction("myTenthGlobalFunction", cSourceFile, aPointerToAPointer);
		myTenthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToATypedefedEnum",myTenthGlobalFunction, aPointerToNotTypedefCData2));
		myTenthGlobalFunction.addInputParameter(new CFunctionInputParameter("aPointerToAChar",myTenthGlobalFunction, aPointertoAInt));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointertoAInt));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToAPointer));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToNotTypedefCData));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToNotTypedefCData2));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToATypedef));
		myTenthGlobalFunction.addLocalVariable(new CModuleVariable(aPointerToATypedef2));

		CPointerType aPointerToAUnion = new CPointerType("aPointerToAUnion", null, union1);
		CFunction myTwelvethGlobalFunction =  new CFunction("myTwelvethGlobalFunction", cSourceFile, aPointerToAUnion);
		CFunctionInputParameter aPointerToAPointerInputParameter = new CFunctionInputParameter("aPointerToAPointer",myTwelvethGlobalFunction, aPointerToAPointer);
		aPointerToAPointerInputParameter.setTypeQualifier(TypeQualifiersEnum.CONST);
		myTwelvethGlobalFunction.addInputParameter(aPointerToAPointerInputParameter);
		myTwelvethGlobalFunction.addInputParameter(new CFunctionInputParameter("aMultiDimArray",myTwelvethGlobalFunction, aNewArray));
		CPointerType aPointerToArray = new CPointerType("aPointerToAnArray", null, aNewArray);
		CFunctionInputParameter aPointerToArrayParam = new CFunctionInputParameter("aPointerToAnArrayParameter",myTwelvethGlobalFunction, aPointerToArray);
		myTwelvethGlobalFunction.addInputParameter(aPointerToArrayParam);
		CArrayType aNewArray3 = new CArrayType("arrayWithNoDim", 1, cSourceFile, CBasicType.BasicType.INT.getType());
		myTwelvethGlobalFunction.addInputParameter(new CFunctionInputParameter("arraywithNoDimParameter",myTwelvethGlobalFunction,aNewArray3));





		cSourceFile.addGlobalFunction(myEigthGlobalFunction);
		cSourceFile.addGlobalFunction(myNintheGlobalFunction);
		cSourceFile.addGlobalFunction(myTenthGlobalFunction);
		cSourceFile.addGlobalFunction(myTwelvethGlobalFunction);
		/************************************************LOCAL FUNCTIONS *******************************************************/


		CFunction myLocalFunction = new CFunction("firslocalFunction", cSourceFile, CBasicType.BasicType.CHAR.getType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		cSourceFile.addLocalFunction(myLocalFunction);

		CFunction myLocalFunction2 = new CFunction("secondLocalFunction", cSourceFile, CBasicType.BasicType.LONG_DOUBLE.getType(), StorageClassSpecifiersEnum.STATIC, TypeQualifiersEnum.NONE);
		cSourceFile.addLocalFunction(myLocalFunction2);


		/**********************************************SOME TYPEDEFS********************************************************************/

		CEnumType enumDeclarationDrei = new CEnumType();
		enumDeclarationDrei.addConstantWithoutValue("POWER_MODE_RUNNING");
		enumDeclarationDrei.addConstantWithoutValue("POWER_MODE_SLEEP");
		enumDeclarationDrei.addConstantWithoutValue("POWER_MODE_DEEPSLEEP");
		enumDeclarationDrei.addConstantWithoutValue("POWER_MODE_HIBERNATE");

		CTypedef hTypeDef = new CTypedef("EN_POWER_MODE_T",enumDeclarationDrei, cSourceFile);
		enumDeclarationDrei.setOwner(hTypeDef);
		cSourceFile.addTypeDefs(hTypeDef);


		CStructType structNoOwnerNoName = new CStructType();
		CDataMember cDataMemberxx = new CDataMember("a",structNoOwnerNoName, CBasicType.BasicType.CHAR.getType());
		structNoOwnerNoName.addMemberDefinition(cDataMember1);
		CDataMember cDataMemberxxy = new CDataMember("b",structNoOwnerNoName, CBasicType.BasicType.LONG_DOUBLE.getType());
		structNoOwnerNoName.addMemberDefinition(cDataMember2);
		CTypedef iTypeDef = new CTypedef("ANAME",structNoOwnerNoName, cSourceFile);
		structNoOwnerNoName.setOwner(iTypeDef);
		cSourceFile.addTypeDefs(iTypeDef);

		struct1.addMemberDefinition(new CDataMember("thisMustBeAVar",structNoOwnerNoName, iTypeDef));
		/******************************************************HEADER FILE ***************************************************************************************/		

		CHeaderFile cHeaderFile = new CHeaderFile("AllCases");
		cHeaderFile.addIncludesSystemHeader("stdlib.h");

		CMacro aRandomMacro = new CObjectLikeMacro("RANDOMMACRO", cHeaderFile, "3");
		aRandomMacro.setBody("JAVADOC: RANDOMMACRO")
		cHeaderFile.addObjectLikeMacro(aRandomMacro);

		CFunction globalFunctionPrototype = new CFunction("AglobalFunctionPrototype", cHeaderFile, new CVoidType(), StorageClassSpecifiersEnum.NONE, TypeQualifiersEnum.NONE);
		globalFunctionPrototype.setBody("JAVADOC");
		cHeaderFile.addGlobalFunctionPrototypes(globalFunctionPrototype);




		CTypedef aTypeDefh = new CTypedef("INTEGERh",CBasicType.BasicType.LONG_INT.getType(), cHeaderFile);
		cHeaderFile.addTypeDefs(aTypeDefh);
		aTypeDefh.setBody("ANOTHERJAVADOC2");

		CTypedef bTypeDefh = new CTypedef("ULALAh",CBasicType.BasicType.UNSIGNED_LONG_LONG_INT.getType(), cHeaderFile);
		cHeaderFile.addTypeDefs(bTypeDefh);

		CTypedef cTypeDefh = new CTypedef("STRUCTEINSh",struct1, cHeaderFile);
		cHeaderFile.addTypeDefs(cTypeDefh);

		CTypedef dTypeDefh = new CTypedef("enumtypedefh",enumDeclaration, cHeaderFile);
		cHeaderFile.addTypeDefs(dTypeDefh);


		CVariable cVariablexyc = new CGlobalVariable("var",cHeaderFile,"",CBasicType.BasicType.INT.getType(), StorageClassSpecifiersEnum.EXTERN, TypeQualifiersEnum.NONE);
		cVariablexyc.setBody("cVariablexyc");
		cHeaderFile.addGlobalVariable(cVariablexyc);


		CStructType struct12 = new CStructType("AStructXX",cHeaderFile);

		CDataMember cDataMembercxc = new CDataMember("a",struct1, CBasicType.BasicType.CHAR.getType());
		struct12.addMemberDefinition(cDataMembercxc);

		CDataMember cDataMembeyxy = new CDataMember("b",struct1, CBasicType.BasicType.LONG_DOUBLE.getType());
		cDataMembeyxy.setBody("LONG_DOUBLE");
		struct12.addMemberDefinition(cDataMembeyxy);

		cHeaderFile.addGlobalDataTypeDeclaration(struct12)


		result.add(cSourceFile);
		result.add(cHeaderFile);



		return result;
	}
}